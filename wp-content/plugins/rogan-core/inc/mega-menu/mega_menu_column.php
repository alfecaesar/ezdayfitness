<?php
add_shortcode('roganmm_menu_column', function($atts, $content) {
    ob_start();
    $atts = shortcode_atts(array(
        'title'   => '',
        'column'  => '3',
    ), $atts);
    ?>
    <div class="col-lg-<?php echo esc_attr($atts['column']) ?> mt-md-3 mb-md-3 mt-lg-3 mb-lg-3">
        <?php if ( !empty($atts['title']) ) : ?>
            <h6 class="mega-menu-title"> <?php echo esc_html($atts['title']) ?> </h6>
        <?php endif; ?>
        <ul class="list-unstyled mega-dropdown-list">
            <?php echo do_shortcode($content) ?>
        </ul>
    </div>

    <?php
    $html = ob_get_clean();
    return $html;
});