<?php
add_shortcode('roganmm_single_item', function($atts, $content) {
    ob_start();
    $atts = shortcode_atts( array (
        'icon_image_url'    => '',
        'url'               => '',
    ), $atts );
    ?>

    <li class="mm_nav_item">
        <a href="<?php echo esc_url($atts['url']) ?>" class="mm_nav_link">
            <?php if(!empty($atts['icon_image_url'])) : ?>
                <img src="<?php echo esc_url($atts['icon_image_url']) ?>" alt="<?php echo esc_attr($content) ?>" class="svg mm_nav_icon">
            <?php endif; ?>
            <?php echo esc_html($content) ?>
        </a>
    </li>

    <?php
    $html = ob_get_clean();
    return $html;
});