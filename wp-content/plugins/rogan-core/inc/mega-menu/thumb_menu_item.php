<?php
add_shortcode('roganmm_thumb_menu_item', function($atts, $content) {
    ob_start();
    $atts = shortcode_atts(array(
        'image_url'   => '',
        'url'         => '',
        'column'      => '3',
    ), $atts);
    ?>

    <div class="col-lg-<?php echo esc_attr($atts['column']) ?> col-md-4 col-sm-6 mt-3 mb-3 mt-lg-3 mb-lg-3">
        <a href="<?php echo esc_url($atts['url']) ?>" class="img-box">
            <?php if(!empty($atts['image_url'])) : ?>
                <span class="img"> <img src="<?php echo esc_url($atts['image_url']) ?>" alt="<?php echo esc_attr($content) ?>"> </span>
            <?php endif; ?>
            <?php if(!empty($content)) : ?>
                <span class="text"> <?php echo esc_html($content) ?> </span>
            <?php endif; ?>
        </a>
    </div>

    <?php
    $html = ob_get_clean();
    return $html;
});