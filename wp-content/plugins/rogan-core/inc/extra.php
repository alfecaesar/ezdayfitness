<?php
// Add image sizes
add_action( 'after_setup_theme', function() {
    add_image_size('rogan_570x750', 570, 750, true);
    add_image_size('rogan_370x225', 370, 225, true);
    add_image_size('rogan_570x700', 570, 700, true);
    add_image_size('rogan_550x700', 550, 700, true);
    add_image_size('rogan_50x50', 50, 50, true);
    add_image_size('rogan_380x420', 380, 420, true);
    add_image_size('rogan_635x490', 635, 490, true);
    add_image_size('rogan_570x770', 570, 770, true);
    add_image_size('rogan_510x550', 510, 550, true);
    add_image_size('rogan_370x474', 370, 474, true);
    add_image_size('rogan_355x390', 355, 390, true);
    add_image_size('rogan_415x360', 415, 360, true);
    add_image_size('rogan_1000x500', 1000, 500, true);
});


// Elementor is anchor external target
function rogan_is_external($settings_key) {
    if(isset($settings_key['is_external'])) {
        echo $settings_key['is_external'] == true ? 'target="_blank"' : '';
    }
}

// Elementor is anchor nofollow
function rogan_is_nofollow($settings_key) {
    if(isset($settings_key['nofollow'])) {
        echo $settings_key['nofollow'] == true ? 'rel="nofollow"' : '';
    }
}

// Check if the url is external or nofollow
function rogan_is_exno($settings_key) {
    echo $settings_key['is_external'] == true ? 'target="_blank"' : '';
    echo $settings_key['nofollow'] == true ? 'rel="nofollow"' : '';
}


function rogan_icon_array($k, $replace = 'icon', $separator = '-') {
    $v = array();
    foreach ($k as $kv) {
        $kv = str_replace($separator, ' ', $kv);
        $kv = str_replace($replace, '', $kv);
        $v[] = array_push($v, ucwords($kv));
    }
    foreach($v as $key => $value) if($key&1) unset($v[$key]);
    return array_combine($k, $v);
}


// Social Share
function rogan_social_share() { ?>
    <ul class="share-icon">
        <li> <?php esc_html_e('Share: ', 'rogan-core') ?> </li>
        <li><a href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="https://twitter.com/intent/tweet?text=<?php the_permalink(); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="https://www.pinterest.com/pin/create/button/?url=<?php the_permalink() ?>"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink() ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
    </ul>
    <?php
}

add_filter('gettext','rogan_enter_title');
function rogan_enter_title( $input ) {
    global $post_type;
    if( is_admin() && 'Enter title here' == $input && 'team' == $post_type )
        return 'Enter here the team member name';
    return $input;
}


// Get the first category name
function rogan_first_category($term = 'category') {
    $cats = get_the_terms(get_the_ID(), $term);
    $cat  = is_array($cats) ? $cats[0]->name : '';
    echo esc_html($cat);
}


// Get the first category link
function rogan_first_category_link($term='category') {
    $cats = get_the_terms(get_the_ID(), $term);
    $cat  = is_array($cats) ? get_category_link($cats[0]->term_id) : '';
    echo esc_url($cat);
}


// Post title array
function rogan_get_postTitleArray($postType = 'post') {
    $post_type_query  = new WP_Query(
        array (
            'post_type'      => $postType,
            'posts_per_page' => -1
        )
    );
    // we need the array of posts
    $posts_array      = $post_type_query->posts;
    // create a list with needed information
    // the key equals the ID, the value is the post_title
    $post_title_array = wp_list_pluck( $posts_array, 'post_title', 'ID' );

    return array_flip($post_title_array);
}


// Support SVG file format. Allow .svg file to upload
function karpartz_add_svg_to_upload_mimes( $upload_mimes )
{
    $upload_mimes['svg'] = 'image/svg+xml';
    $upload_mimes['svgz'] = 'image/svg+xml';
    return $upload_mimes;
}
add_filter( 'upload_mimes', 'karpartz_add_svg_to_upload_mimes', 10, 1 );

// Add Rogan Portfolio Layout in the post column
function rogan_add_acf_columns ( $columns ) {
    return array_merge ( $columns, array (
        'layout' => __ ( 'Layout' ),
    ) );
}
add_filter ( 'manage_portfolio_posts_columns', 'rogan_add_acf_columns' );

function rogan_portfolio_custom_column ( $column, $post_id ) {
    switch ( $column ) {
        case 'layout':
            echo ucwords( get_post_meta ( $post_id, 'layout', true ) );
            break;
    }
}
add_action ( 'manage_portfolio_posts_custom_column', 'rogan_portfolio_custom_column', 10, 2 );


// Portfolio ul>li Pagination
function rogan_pagination() {
    global $wp_query;
    if ( $wp_query->max_num_pages <= 1 ) return;
    $big = 999999999; // need an unlikely integer
    $pages = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'type'  => 'array',
        'prev_text' => '<i class="flaticon-back"></i>',
        'next_text' => '<i class="flaticon-next-1"></i>'
    ) );
    if( is_array( $pages ) ) {
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<ul>';
        foreach ( $pages as $page ) {
            echo "<li>$page</li>";
        }
        echo '</ul>';
    }
}
