<?php

// Require widget files
require plugin_dir_path(__FILE__) . '/Rogan_recent_posts.php';
require plugin_dir_path(__FILE__) . 'Rogan_about_us.php';
require plugin_dir_path(__FILE__) . 'Rogan_contact_info_widget.php';

// Register Widgets
add_action('widgets_init', function() {
    register_widget('Rogan_recent_posts');
    register_widget('Rogan_contact_info_widget');
});
