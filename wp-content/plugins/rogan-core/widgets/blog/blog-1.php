<div class="agn-home-blog our-blog-one">
    <?php if (!empty($settings['shape1']['url'])) : ?>
        <img src="<?php echo esc_url($settings['shape1']['url']) ?>" alt="<?php echo esc_attr($settings['alt']) ?>" class="shape-one">
    <?php endif; ?>
    <?php if (!empty($settings['shape2']['url'])) : ?>
        <img src="<?php echo esc_url($settings['shape2']['url']) ?>" alt="<?php echo esc_attr($settings['alt']) ?>" class="shape-two">
    <?php endif; ?>
    <?php if (!empty($settings['shape3']['url'])) : ?>
        <img src="<?php echo esc_url($settings['shape3']['url']) ?>" alt="<?php echo esc_attr($settings['alt']) ?>" class="shape-three">
    <?php endif; ?>
    <?php if (!empty($settings['shape4']['url'])) : ?>
        <img src="<?php echo esc_url($settings['shape4']['url']) ?>" alt="<?php echo esc_attr($settings['alt']) ?>" class="shape-four">
    <?php endif; ?>
    <div class="container">
        <div class="theme-title-one text-center">
            <?php if (!empty($settings['upper_title'])) : ?>
                <div class="upper-title"><?php echo esc_html($settings['upper_title']) ?></div>
            <?php endif; ?>
            <?php if (!empty($settings['main_title'])) : ?>
                <h2 class="main-title"><?php echo esc_html($settings['main_title']) ?></h2>
            <?php endif; ?>
        </div> <!-- /.theme-title-one -->

        <div class="blog-one-slider">

            <?php
            while ($blog_posts->have_posts()) : $blog_posts->the_post();
                ?>
                <div class="item">
                    <div class="single-blog-post">
                        <div class="flip-box-front">
                            <div class="clearfix">
                                <?php echo get_avatar(get_the_author_meta('ID'),'', '', 60, array('class' => 'author-img' )); ?>

                                <div class="author-info">
                                    <h6 class="name"><?php the_author() ?></h6>
                                    <div class="date"><?php echo get_the_date() ?></div>
                                </div>
                            </div>
                            <a href="<?php the_permalink() ?>" class="title" title="<?php the_title() ?>">
                                <?php echo wp_trim_words(get_the_title(), 10, '') ?>
                            </a>
                            <?php echo wp_trim_words(get_the_content(), 20) ?>
                        </div> <!-- /.flip-box-front -->
                        <div class="flip-box-back">
                            <div class="author-info">
                                <h6 class="name"><?php the_author() ?></h6>
                                <div class="date"><?php echo get_the_date() ?></div>
                            </div>
                            <a href="<?php the_permalink() ?>" class="title" title="<?php the_title() ?>">
                                <?php echo wp_trim_words(get_the_title(), 10, '') ?>
                            </a>
                            <a href="<?php the_permalink() ?>" class="more"><i class='flaticon-next-1'></i></a>
                        </div> <!-- /.flip-box-back -->
                    </div> <!-- /.single-blog-post -->
                </div>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
    </div>
</div>