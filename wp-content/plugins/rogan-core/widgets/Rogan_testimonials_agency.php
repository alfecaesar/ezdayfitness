<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_testimonials_agency extends Widget_Base {

    public function get_name() {
        return 'rogan_testimonials_agency';
    }

    public function get_title() {
        return esc_html__( 'Testimonials Agency', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-testimonial-carousel';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return ['owl-carousel'];
    }

    public function get_style_depends() {
        return ['owl-carousel', 'owl-theme', 'animate'];
    }

    protected function _register_controls() {

        //***********************************  Testimonials Title ******************************************//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Testimonials Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'upper_title',
            [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Testimonials'
            ]
        );

        $this->add_control(
            'main_title',
            [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Check what’s our client say about us!'
            ]
        );

        $this->end_controls_section();


        //********************************* Testimonials List **********************************//
        $this->start_controls_section(
            'testimonials_sec',
            [
                'label' => esc_html__( 'Testimonials Slider', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'author_name',
            [
                'label' => esc_html__( 'Author Name', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Eh Jewel'
            ]
        );

        $repeater->add_control(
            'designation',
            [
                'label' => esc_html__( 'Designation', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Developer'
            ]
        );

        $repeater->add_control(
            'author_img',
            [
                'label' => esc_html__( 'Author Image', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
            ]
        );

        $repeater->add_control(
            'contents',
            [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' =>Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'testimonials_sliders',
            [
                'label' => esc_html__( 'Sliders Section', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ author_name }}}',
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tabs
         */
        /********************************* Section Title ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_upper_title',
            [
                'label' => esc_html__( 'Upper Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .upper-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'color_main_title',
            [
                'label' => esc_html__( 'Main Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_upper_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'label' => esc_html__( 'Upper Title Typography', 'rogan-core' ),
                'selector' => '
                    {{WRAPPER}} .theme-title-one .upper-title',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_main_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'label' => esc_html__( 'Main Title Typography', 'rogan-core' ),
                'selector' => '
                    {{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();

        //********************************* Background Shape Image ********************************//
        $this->start_controls_section(
            'bg_shapes',
            [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE
            ]
        );

        $this->add_control(
            'shape_img1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/4.png', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape_img2',
            [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-6.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape_img3',
            [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-66.svg', __FILE__)
                ],
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .agn-testimonial' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();


    }

    protected function render() {
        $settings = $this->get_settings();
        $testimonials = $settings['testimonials_sliders'];
        ?>

        <div class="agn-testimonial">
            <div class="shape-box">
                <?php if (!empty($settings['shape_img1']['url'])) : ?>
                    <img src="<?php echo esc_url($settings['shape_img1']['url']) ?>" alt="<?php echo esc_attr($settings['upper_title']) ?>">
                <?php endif; ?>
                <?php
                foreach ($testimonials as $testimonial) {
                    if(!empty($testimonial['author_img']['url'])) {
                        ?>
                        <img src="<?php echo esc_url($testimonial['author_img']['url']) ?>"
                             alt="<?php echo esc_attr($testimonial['author_name']) ?>"
                             class="people elementor-repeater-item-<?php echo $testimonial['_id'] ?>">
                        <?php
                    }
                }
                ?>
                <?php if (!empty($settings['shape_img2']['url'])) : ?>
                    <img src="<?php echo esc_url($settings['shape_img2']['url']) ?>" alt="<?php echo esc_attr($settings['upper_title']) ?>" class="shape-one">
                <?php endif; ?>
            </div>
            <?php if (!empty($settings['shape_img3']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape_img3']['url']) ?>" alt="<?php echo esc_attr($settings['upper_title']) ?>" class="shape-two" data-aos="fade-left">
            <?php endif; ?>
            <div class="container clearfix">
                <div class="main-content">
                    <div class="theme-title-one">
                        <?php if (!empty($settings['upper_title'])) : ?>
                            <div class="upper-title"><?php echo esc_html($settings['upper_title']) ?></div>
                        <?php endif; ?>
                        <?php if (!empty($settings['main_title'])) : ?>
                            <h2 class="main-title"><?php echo esc_html($settings['main_title']) ?></h2>
                        <?php endif; ?>
                    </div> <!-- /.theme-title-one -->

                    <div class="agn-testimonial-slider">

                        <?php
                        if (!empty($testimonials)) {
                        foreach ($testimonials as $testimonial) {
                            $image = isset($testimonial['author_img']['id']) ? $testimonial['author_img']['id'] : '';
                            $image = wp_get_attachment_image_src($image, 'full');
                            ?>
                            <div class="item elementor-repeater-item-<?php echo $testimonial['_id'] ?>">
                                <?php if (!empty($testimonial['contents'])) : ?>
                                    <?php echo wp_kses_post(wpautop($testimonial['contents'])) ?>
                                <?php endif; ?>
                                <div class="author-info clearfix">
                                    <img src="<?php echo ($image[0]) ?>" alt="<?php echo esc_attr($testimonial['author_name']) ?>" class="author-img">
                                    <div class="name-info">
                                        <?php if (!empty($testimonial['author_name'])) : ?>
                                            <h6 class="name"><?php echo esc_html($testimonial['author_name']) ?></h6>
                                        <?php endif; ?>
                                        <?php if (!empty($testimonial['designation'])) : ?>
                                            <span><?php echo esc_html($testimonial['designation']) ?></span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }}
                        ?>
                    </div> <!-- /.agn-testimonial-slider -->
                </div> <!-- /.main-content -->
            </div> <!-- /.container -->
        </div>


        <script>
            (function ( $ ) {
                "use strict";
                $(document).on ('ready', function (){
                    var agnTslider = $ (".agn-testimonial-slider");
                    if(agnTslider.length) {
                        agnTslider.owlCarousel({
                            loop:true,
                            nav:true,
                            navText: ["<i class='flaticon-back'></i>" , "<i class='flaticon-next'></i>"],
                            dots:false,
                            autoplay:true,
                            autoplayTimeout:4000,
                            smartSpeed:1200,
                            autoplayHoverPause:true,
                            lazyLoad:true,
                            items:1
                        });
                    }
                });
            }( jQuery ));
        </script>
        <?php
    }
}