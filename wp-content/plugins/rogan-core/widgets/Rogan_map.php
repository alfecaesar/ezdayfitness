<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_map extends Widget_Base {

    public function get_name() {
        return 'rogan_map';
    }

    public function get_title() {
        return __( 'Map', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-google-maps';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ------------------------------ Map Settings ------------------------------
        $this->start_controls_section(
            'map_sec', [
                'label' => __( 'Map Settings', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'important_note',
            [
                'label' => esc_html__( 'Important Note', 'rogan-core' ),
                'type' => \Elementor\Controls_Manager::RAW_HTML,
                'raw' => esc_html__( 'Place this widget in a full width container of Elementor in the Elementor Full Width page template. ', 'rogan-core' ),
                'content_classes' => 'your-class',
            ]
        );

        $this->add_control(
            'map_api', [
                'label' => esc_html__( 'Map API Key', 'rogan-core' ),
                'description' => __( 'Get the Map API Key <a href="https://is.gd/qEYdtJ" target="_blank">here</a>', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'lat', [
                'label' => esc_html__( 'Latitude', 'rogan-core' ),
                'description' => __( 'Get the Latitude <a href="https://is.gd/GDjonL" target="_blank">here</a>', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'lng', [
                'label' => esc_html__( 'Longitude', 'rogan-core' ),
                'description' => __( 'Get the Longitude <a href="https://is.gd/GDjonL" target="_blank">here</a>', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'content', [
                'label' => esc_html__( 'Content', 'rogan-core' ),
                'description' => esc_html__( 'You can keep here the address with your logo.', 'rogan-core' ),
                'type' => Controls_Manager::WYSIWYG,
                'label_block' => true,
            ]
        );

        $this->end_controls_section(); // End title section

    }

    protected function render() {
        $settings = $this->get_settings();
        $map_content = !empty($settings['content']) ? wp_kses_post($settings['content']) : '';;
        ?>
        <div id="google-map-two"><div class="map-canvas"></div></div>

        <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo esc_attr($settings['map_api']) ?>" type="text/javascript"></script>
        <script src="<?php echo plugins_url( 'js/sanzzy-map/dist/snazzy-info-window.min.js', __FILE__ ) ?>"></script>

        <script>
            (function ( $ ) {
                "use strict";

                    $(function() {
                        var mapStyle = [{featureType:"water",elementType:"geometry",stylers:[{color:"#e9e9e9"},{lightness:17}]},{featureType:"landscape",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:20}]},{featureType:"road.highway",elementType:"geometry.fill",stylers:[{color:"#ffffff"},{lightness:17}]},{featureType:"road.highway",elementType:"geometry.stroke",stylers:[{color:"#ffffff"},{lightness:29},{weight:.2}]},{featureType:"road.arterial",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:18}]},{featureType:"road.local",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:16}]},{featureType:"poi",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:21}]},{featureType:"poi.park",elementType:"geometry",stylers:[{color:"#dedede"},{lightness:21}]},{elementType:"labels.text.stroke",stylers:[{visibility:"on"},{color:"#ffffff"},{lightness:16}]},{elementType:"labels.text.fill",stylers:[{saturation:36},{color:"#333333"},{lightness:40}]},{elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"transit",elementType:"geometry",stylers:[{color:"#f2f2f2"},{lightness:19}]},{featureType:"administrative",elementType:"geometry.fill",stylers:[{color:"#fefefe"},{lightness:20}]},{featureType:"administrative",elementType:"geometry.stroke",stylers:[{color:"#fefefe"},{lightness:17},{weight:1.2}]}];

                        // Create the map
                        var map = new google.maps.Map($('.map-canvas')[0], {
                            zoom: 14,
                            styles: mapStyle,
                            scrollwheel: false,
                            center: new google.maps.LatLng(<?php echo esc_js($settings['lat']) ?>, <?php echo esc_js($settings['lng']) ?>)
                        });

                        // Add a marker
                        var marker = new google.maps.Marker({
                            map: map,
                            position: new google.maps.LatLng(<?php echo esc_js($settings['lat']) ?>, <?php echo esc_js($settings['lng']) ?>)
                        });

                        // Add a Snazzy Info Window to the marker
                        var info = new SnazzyInfoWindow({
                            marker: marker,
                            content: '<?php echo $map_content ?>',
                            closeOnMapClick: false
                        });
                        info.open();
                    });

            }( jQuery ));

        </script>
        <?php
    }

}