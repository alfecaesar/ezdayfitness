<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

/**
 * Elementor alert widget.
 *
 * Elementor widget that displays a collapsible display of content in an toggle
 * style, allowing the user to open multiple items.
 *
 * @since 1.0.0
 */
class Rogan_alerts_box extends Widget_Base {

    /**
     * Get widget name.
     *
     * Retrieve alert widget name.
     *
     * @since 1.0.0
     * @access public
     *
     * @return string Widget name.
     */
    public function get_name() {
        return 'rogan_alerts_box';
    }

    /**
     * Get widget title.
     *
     * Retrieve alert widget title.
     *
     * @since 1.0.0
     * @access public
     *
     * @return string Widget title.
     */
    public function get_title() {
        return __( 'Rogan Alerts', 'elementor' );
    }

    /**
     * Get widget icon.
     *
     * Retrieve alert widget icon.
     *
     * @since 1.0.0
     * @access public
     *
     * @return string Widget icon.
     */
    public function get_icon() {
        return 'eicon-alert';
    }

    /**
     * Get widget keywords.
     *
     * Retrieve the list of keywords the widget belongs to.
     *
     * @since 2.1.0
     * @access public
     *
     * @return array Widget keywords.
     */
    public function get_keywords() {
        return [ 'alert', 'notice', 'message' ];
    }

    /**
     * Register alert widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function _register_controls() {


        $this->start_controls_section(
            'section_alert',
            [
                'label' => __( 'Alert', 'elementor' ),
            ]
        );

        // Icon 01
        $this->add_control(
            'box_size',
            [
                'label' => __( 'Alert Box Size', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'small_box' => esc_html__('Small Box', 'rogan-core'),
                    'medium_box' => esc_html__('Medium Box', 'rogan-core'),
                    'big_box' => esc_html__('Big Box', 'rogan-core'),
                ],
                'default' => 'small_box',
            ]
        );

        $this->add_control(
            'alert_type',
            [
                'label' => __( 'Type', 'elementor' ),
                'type' => Controls_Manager::SELECT,
                'default' => 'info',
                'options' => [
                    'notice' => __( 'Notice', 'elementor' ),
                    'error' => __( 'Error', 'elementor' ),
                    'warning' => __( 'Warning', 'elementor' ),
                    'info' => __( 'Info', 'elementor' ),
                    'success' => __( 'Success', 'elementor' ),
                    'message' => __( 'Message', 'elementor' ),
                    'danger' => __( 'Danger', 'elementor' ),
                ],
                'style_transfer' => true,
            ]
        );

        $this->add_control(
            'alert_title',
            [
                'label' => __( 'Title', 'elementor' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Notice Message! Your message here'
            ]
        );

        $this->add_control(
            'alert_description',
            [
                'label' => __( 'Subtitle', 'elementor' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'condition' => [
                    'box_size' =>  'big_box'
                ]
            ]
        );

        $this->add_control(
            'show_dismiss',
            [
                'label' => __( 'Dismiss Button', 'elementor' ),
                'type' => Controls_Manager::SELECT,
                'default' => 'show',
                'options' => [
                    'show' => __( 'Show', 'elementor' ),
                    'hide' => __( 'Hide', 'elementor' ),
                ],
            ]
        );

        $this->end_controls_section();


        $this->start_controls_section(
            'section_icon',
            [
                'label' => __( 'Icon', 'rogan-core' ),
            ]
        );

        // Icon 01
        $this->add_control(
            'icon_type',
            [
                'label' => __( 'Icon Type 01', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'fontawesome' => esc_html__('Font-Awesome', 'rogan-core'),
                    'eicon' => esc_html__('Elegant Icon', 'rogan-core'),
                    'ticon' => esc_html__('Themify Icon', 'rogan-core'),
                    'slicon' => esc_html__('Simple Line Icon', 'rogan-core'),
                    'flaticon' => esc_html__('Flaticon', 'rogan-core'),
                ],
                'default' => 'slicon',
            ]
        );

        $this->add_control(
            'fontawesome',
            [
                'label' => __( 'Font-Awesome', 'rogan-core' ),
                'type' => Controls_Manager::ICON,
                'condition' => [
                    'icon_type' => 'fontawesome'
                ]
            ]
        );

        $this->add_control(
            'eicon',
            [
                'label' => __( 'Elegant Icon', 'rogan-core' ),
                'type' => Controls_Manager::ICON,
                'options' => rogan_elegant_icons(),
                'include' => rogan_include_elegant_icons(),
                'default' => 'arrow_right',
                'condition' => [
                    'icon_type' => 'eicon'
                ]
            ]
        );

        $this->add_control(
            'ticon',
            [
                'label' => __( 'Themify Icon', 'rogan-core' ),
                'type' => Controls_Manager::ICON,
                'options' => rogan_themify_icons(),
                'include' => rogan_include_themify_icons(),
                'default' => 'ti-panel',
                'condition' => [
                    'icon_type' => 'ticon'
                ]
            ]
        );

        $this->add_control(
            'slicon',
            [
                'label'     => __( 'Simple Line Icon', 'rogan-core' ),
                'type'      => Controls_Manager::ICON,
                'options'   => rogan_simple_line_icons(),
                'include'   => rogan_include_simple_line_icons(),
                'default'   => 'icon-volume-2',
                'condition' => [
                    'icon_type' => 'slicon'
                ]
            ]
        );

        $this->add_control(
            'flaticon',
            [
                'label'      => __( 'Flaticon', 'rogan-core' ),
                'type'       => Controls_Manager::ICON,
                'options'    => rogan_flat_icons(),
                'include'    => rogan_flat_icons(),
                'default'    => 'flaticon-mortarboard',
                'condition'  => [
                    'icon_type' => 'flaticon'
                ]
            ]
        );

        $this->end_controls_section();


        /**
         * Tab: Style
         */
        $this->start_controls_section(
            'section_type',
            [
                'label' => __( 'Alert', 'elementor' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'text_color',
            [
                'label' => __( 'Text Color', 'elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .alert' => 'color: {{VALUE}};',
                    '{{WRAPPER}} .alert.big_alert' => 'color: {{VALUE}};',
                    '{{WRAPPER}} .box_alert' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'background',
            [
                'label' => __( 'Background Color', 'elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .alert' => 'background-color: {{VALUE}};',
                    '{{WRAPPER}} .alert.big_alert' => 'background-color: {{VALUE}};',
                    '{{WRAPPER}} .box_alert' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'border_color',
            [
                'label' => __( 'Border Color', 'elementor' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .alert' => 'background-color: {{VALUE}};',
                    '{{WRAPPER}} .alert.big_alert' => 'background-color: {{VALUE}};',
                    '{{WRAPPER}} .box_alert' => 'border-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'border_left-width',
            [
                'label' => __( 'Left Border Width', 'elementor' ),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .alert' => 'border-left-width: {{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .alert.big_alert' => 'border-left-width: {{{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .box_alert' => 'border-left-width: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();

    }

    /**
     * Render alert widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function render() {
        $settings = $this->get_settings();

        switch ( $settings['icon_type'] ) {
            case 'fontawesome':
                $icon = !empty($settings['fontawesome']) ? $settings['fontawesome'] : '';
                break;
            case 'eicon':
                wp_enqueue_style( 'elegent-icons' );
                $icon = !empty($settings['eicon']) ? $settings['eicon'] : '';
                break;
            case 'ticon':
                wp_enqueue_style( 'themify-icons' );
                $icon = !empty($settings['ticon']) ? $settings['ticon'] : '';
                break;
            case 'slicon':
                wp_enqueue_style('simple-line-icon');
                $icon = !empty($settings['slicon']) ? $settings['slicon'] : '';
                break;
            case 'flaticon':
                $icon = !empty($settings['flaticon']) ? $settings['flaticon'] : '';
                break;
        }
        $icon_class = (!empty($icon)) ? "class='$icon'" : '';


        $icon2_class = (!empty($icon2)) ? "class='$icon2'" : '';

        if($settings['box_size'] == 'small_box') {
            ?>
            <div class="alert <?php echo esc_attr($settings['alert_type']) ?>">
                <div class="alert_body">
                    <i <?php echo $icon_class; ?>></i>
                    <?php if (!empty($settings['alert_title'])) : ?>
                        <?php echo esc_html($settings['alert_title']) ?>
                    <?php endif; ?>
                </div>
                <?php if ( 'show' === $settings['show_dismiss'] ) : ?>
                    <div class="alert_close">
                        <i <?php echo $icon2_class; ?>></i>
                    </div>
                <?php endif; ?>
            </div>
            <?php
        }
        elseif($settings['box_size'] == 'medium_box') {
            ?>
            <div class="alert <?php echo esc_attr($settings['alert_type']) ?> big_alert">
                <div class="alert_body">
                    <i <?php echo $icon_class; ?>></i>
                    <?php if (!empty($settings['alert_title'])) : ?>
                        <?php echo esc_html($settings['alert_title']) ?>
                    <?php endif; ?>
                </div>
                <?php if ( 'show' === $settings['show_dismiss'] ) : ?>
                    <div class="alert_close">
                        <i <?php echo $icon2_class; ?>></i>
                    </div>
                <?php endif; ?>
            </div>
            <?php
        }
        elseif($settings['box_size'] == 'big_box') {
            ?>
            <div class="box_alert media box_<?php echo esc_attr($settings['alert_type']) ?>">
                <?php if ('show' === $settings['show_dismiss']) : ?>
                    <div class="alert_close">
                        <i <?php echo $icon2_class; ?>></i>
                    </div>
                <?php endif; ?>
                <div class="icon">
                    <i <?php echo $icon_class; ?>></i>
                </div>
                <div class="media-body">
                    <?php if (!empty($settings['alert_title'])) : ?>
                        <h5><?php echo esc_html($settings['alert_title']) ?></h5>
                    <?php endif; ?>
                    <?php if (!empty($settings['alert_description'])) : ?>
                        <?php echo wpautop($settings['alert_description']) ?>
                    <?php endif; ?>
                </div>
            </div>
            <script>
                ;(function($){
                    "use strict";
                    $(document).ready(function () {
                        $('.alert_close').on('click', function(e){
                            $(this).parent().hide();
                        });
                    })
                })(jQuery)

            </script>
            <?php
        }
    }
}
