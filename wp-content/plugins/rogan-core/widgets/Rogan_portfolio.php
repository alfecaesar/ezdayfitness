<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use WP_Query;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_portfolio extends Widget_Base {

    public function get_name() {
        return 'rogan_portfolio';
    }

    public function get_title() {
        return __( 'Portfolio', 'rogan-hero' );
    }

    public function get_icon() {
        return ' eicon-gallery-grid';
    }

    public function get_keywords() {
        return [ 'Projects' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_style_depends() {
        return [ 'fancybox' ];
    }

    public function get_script_depends() {
        return [ 'fancybox', 'isotope' ];
    }

    protected function _register_controls() {

        // -------------------------------------------- Filtering
        $this->start_controls_section(
            'portfolio_filter', [
                'label' => __( 'Portfolio Settings', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'all_label', [
                'label' => esc_html__( 'All filter label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'All'
            ]
        );

        $this->add_control(
            'title_word_limit', [
                'label' => esc_html__( 'Title Word Limit', 'rogan-core' ),
                'description' => esc_html__( 'Portfolio Title Word Limit', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'label_block' => true,
                'default' => 3
            ]
        );

        $this->add_control(
            'show_count', [
                'label' => esc_html__( 'Show count', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'label_block' => true,
                'default' => 6
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__( 'Order', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->add_control(
            'exclude', [
                'label' => esc_html__( 'Exclude Portfolio', 'rogan-core' ),
                'description' => esc_html__( 'Enter the portfolio post ID to hide. Input the multiple ID with comma separated', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->end_controls_section();


        // -------------------------------------------- Filtering
        $this->start_controls_section(
            'portfolio_layout', [
                'label' => __( 'Layout', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'is_full_width', [
                'label' => __( 'Full Width', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __( 'Yes', 'rogan-core' ),
                'label_off' => __( 'No', 'rogan-core' ),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'portfolio_style',
            [
                'label' => __( 'Portfolio Style', 'plugin-domain' ),
                'type' => \Elementor\Controls_Manager::CHOOSE,
                'options' => [
                    'grid' => [
                        'title' => __( 'Fixed Grid', 'plugin-domain' ),
                        'icon' => 'eicon-gallery-grid',
                    ],
                    'masonry' => [
                        'title' => __( 'Masonry Grid', 'plugin-domain' ),
                        'icon' => 'eicon-posts-masonry',
                    ],
                ],
                'default' => 'grid',
                'toggle' => false,
            ]
        );

        $this->add_control(
            'show_features', [
                'label' => esc_html__( 'Key Features', 'rogan-core' ),
                'description' => esc_html__( 'Limit the Key Features to display on hover.', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 1,
                'condition' => [
                    'portfolio_style' => 'masonry'
                ]
            ]
        );

        /*$this->add_control(
            'column', [
                'label' => __( 'Column', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '6' => __( 'Two column', 'rogan-core' ),
                    '4' => __( 'Three column', 'rogan-core' ),
                    '3' => __( 'Four column', 'rogan-core' ),
                ],
                'default' => '3'
            ]
        );*/

        $this->end_controls_section();


        $this->start_controls_section(
            'portfolio_color', [
                'label' => __( 'Colors', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'active_filter_color', [
                'label' => __( 'Active Filter Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-project .isotop-menu-wrapper li.is-checked' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'filter_color', [
                'label' => __( 'Filter Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-project .isotop-menu-wrapper li' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {

        $settings = $this->get_settings();

        $portfolios = new WP_Query(array(
            'post_type'     => 'portfolio',
            'posts_per_page'=> $settings['show_count'],
            'order' => $settings['order'],
            'post__not_in' => !empty($settings['exclude']) ? explode(',', $settings['exclude']) : ''
        ));

        $portfolio_cats = get_terms(array(
            'taxonomy' => 'portfolio_cat',
            'hide_empty' => true
        ));

        $boxed_container_start = $settings['is_full_width'] != 'yes' ? '<div class="container">' : '';
        $boxed_container_end = $settings['is_full_width'] != 'yes' ? '</div>' : '';
        $wrapper_class = $settings['is_full_width'] != 'yes' ? 'project-standard' : '';

        $ms_boxed_container_start = $settings['is_full_width'] != 'yes' ? '<div class="inner-container">' : '';
        $ms_boxed_container_end = $settings['is_full_width'] != 'yes' ? '</div>' : '';
        $ms_wrapper_class = $settings['is_full_width'] != 'yes' ? 'project-ms-grid' : 'project-ms-full-width';
        ?>

        <?php
        /**
         * Fixed Grid Portfolio Layout
         * @ Full Width
         * @ Boxed
         */
        if( $settings['portfolio_style'] == 'grid' ) : ?>
            <div class="our-project <?php echo $wrapper_class; ?>">
            <?php echo $boxed_container_start; ?>
            <ul class="isotop-menu-wrapper pb-100">
                <?php if(!empty($settings['all_label'])) : ?>
                    <li class="is-checked" data-filter="*">
                        <?php echo esc_html($settings['all_label']); ?>
                    </li>
                <?php endif; ?>
                <?php
                foreach ($portfolio_cats as $portfolio_cat) :
                    ?>
                    <li data-filter=".<?php echo $portfolio_cat->slug ?>">
                        <?php echo $portfolio_cat->name ?>
                    </li>
                    <?php
                endforeach;
                ?>
            </ul>

            <!-- Project Image -->
            <div id="isotop-gallery-wrapper">
                <div class="grid-sizer"></div>
                <?php
                while ($portfolios->have_posts()) : $portfolios->the_post();
                    $cats = get_the_terms(get_the_ID(), 'portfolio_cat');
                    $cat_slug = '';
                    if(is_array($cats)) {
                        foreach ($cats as $cat) {
                            $cat_slug .= $cat->slug.' ';
                        }
                    }
                    $title = wp_trim_words(get_the_title(), $settings['title_word_limit'], '');
                    $title_first_part = preg_replace('/\W\w+\s*(\W*)$/', '$1', $title);
                    $title_pieces = explode(' ', $title);
                    $title_last_word = array_pop($title_pieces);
                    $main_title = $title_first_part . " <span>$title_last_word</span>";
                    ?>
                    <div class="isotop-item <?php echo esc_attr($cat_slug) ?>">
                        <div class="project-item">
                            <div class="img-box">
                                <?php the_post_thumbnail( 'rogan_635x490' ) ?>
                            </div>
                            <div class="hover-jojo">
                                <div>
                                    <h4 class="title" title="<?php the_title() ?>">
                                        <a href="<?php the_permalink() ?>"> <?php echo wp_kses_post($main_title); ?> </a>
                                    </h4>
                                    <?php
                                    if ( have_rows('key_features') ) : ?>
                                    <div>
                                        <?php
                                        while ( have_rows('key_features') ) : the_row();
                                            echo wpautop( get_sub_field('feature_title') );
                                        endwhile;
                                        ?>
                                    </div>
                                    <?php endif; ?>
                                    <ul>
                                        <li><a href="<?php the_permalink() ?>"><span>+</span></a></li>
                                        <li><a href="<?php the_post_thumbnail_url() ?>" class="zoom fancybox" data-fancybox="gallery"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div> <!-- /.hover-jojo -->
                        </div> <!-- /.project-item -->
                    </div> <!-- /.isotop-item -->
                    <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </div> <!-- /#isotop-gallery-wrapper -->
            <?php echo $boxed_container_end; ?>
        </div>
        <?php endif; ?>

        <?php
        /**
         * Masonry Layout
         * @ Full Width
         * @ Boxed
         */
        if( $settings['portfolio_style'] == 'masonry' ) :
            ?>
            <div class="our-project <?php echo esc_attr($ms_wrapper_class) ?> pb-100">
                <?php echo $ms_boxed_container_start; ?>
                <ul class="isotop-menu-wrapper pb-100">
                    <?php if(!empty($settings['all_label'])) : ?>
                        <li class="is-checked" data-filter="*">
                            <?php echo esc_html($settings['all_label']); ?>
                        </li>
                    <?php endif; ?>
                    <?php
                    foreach ($portfolio_cats as $portfolio_cat) :
                        ?>
                        <li data-filter=".<?php echo $portfolio_cat->slug ?>">
                            <?php echo $portfolio_cat->name ?>
                        </li>
                        <?php
                    endforeach;
                    ?>
                </ul>
                <div id="isotop-gallery-wrapper">
                    <div class="grid-sizer"></div>
                    <?php
                    while ($portfolios->have_posts()) : $portfolios->the_post();
                        $cats = get_the_terms(get_the_ID(), 'portfolio_cat');
                        $cat_slug = '';
                        if(is_array($cats)) {
                            foreach ($cats as $cat) {
                                $cat_slug .= $cat->slug.' ';
                            }
                        }
                        $title = wp_trim_words(get_the_title(), $settings['title_word_limit'], '');
                        $title_first_part = preg_replace('/\W\w+\s*(\W*)$/', '$1', $title);
                        $title_pieces = explode(' ', $title);
                        $title_last_word = array_pop($title_pieces);
                        $main_title = $title_first_part . " <span>$title_last_word</span>";
                        ?>
                        <div class="isotop-item <?php echo esc_attr($cat_slug) ?>">
                            <div class="project-item">
                                <div class="img-box">
                                    <?php the_post_thumbnail() ?>
                                </div>
                                <div class="hover-valina">
                                    <div>
                                        <h4 class="title" title="<?php the_title() ?>">
                                            <a href="<?php the_permalink() ?>"> <?php echo wp_kses_post($main_title); ?> </a>
                                        </h4>
                                        <?php
                                        $i = 0;
                                        while ( have_rows('key_features') ) : the_row();
                                            if ( !empty($settings['show_features']) ) {
                                                if ($i == $settings['show_features']) {
                                                    break;
                                                }
                                            }
                                            echo wpautop( get_sub_field('feature_title') );
                                            ++$i;
                                        endwhile;
                                        ?>
                                        <a href="<?php the_post_thumbnail_url() ?>" class="zoom fancybox" data-fancybox="gallery">
                                            <img src="<?php echo plugins_url('images/icon/zoom-in.svg', __FILE__) ?>" alt="<?php the_title() ?>" class="svg">
                                        </a>
                                    </div>
                                </div> <!-- /.hover-valina -->
                            </div> <!-- /.project-item -->
                        </div> <!-- /.isotop-item -->
                    <?php
                endwhile;
                wp_reset_postdata();
                ?>
                </div> <!-- /#isotop-gallery-wrapper -->
                <?php echo $ms_boxed_container_end; ?>
            </div>
            <?php 
        endif;
        ?>

        <script>
            (function ( $ ) {
                "use strict";
                $(window).load(function () {
                    if ($("#isotop-gallery-wrapper").length) {
                        var $grid = $('#isotop-gallery-wrapper').isotope({
                            // options
                            itemSelector: '.isotop-item',
                            percentPosition: true,
                            masonry: {
                                // use element for option
                                columnWidth: '.grid-sizer'
                            }

                        });

                        // filter items on button click
                        $('.isotop-menu-wrapper').on( 'click', 'li', function() {
                            var filterValue = $(this).attr('data-filter');
                            $grid.isotope({ filter: filterValue });
                        });

                        // change is-checked class on buttons
                        $('.isotop-menu-wrapper').each( function( i, buttonGroup ) {
                            var $buttonGroup = $( buttonGroup );
                            $buttonGroup.on( 'click', 'li', function() {
                                $buttonGroup.find('.is-checked').removeClass('is-checked');
                                $( this ).addClass('is-checked');
                            });
                        });
                    }
                });
            }( jQuery ));
        </script>
        <?php
    }

}