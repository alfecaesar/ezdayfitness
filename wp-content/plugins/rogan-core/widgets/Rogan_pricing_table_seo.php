<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_pricing_table_seo extends Widget_Base {

    public function get_name() {
        return 'rogan_pricing_table_seo-pricing-table';
    }

    public function get_title() {
        return esc_html__( 'Pricing Table SEO', 'rogan-hero' );
    }

    public function get_icon() {
        return ' eicon-price-table';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {


        //*****************************  Title Section  *********************************//
        $this->start_controls_section(
            'title_sec', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'upper_title', [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Pricing Plan',
            ]
        );

        $this->add_control(
            'main_title', [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => 'No Hidden Charges! Choose <br> your Plan.',
            ]
        );

        $this->end_controls_section();


        // ********************************* Pricing Repeater Table tabs ***********************************//
        $this->start_controls_section(
            'pricing_table_sec',
            [
                'label' => esc_html__( 'Pricing Table', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'title', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Subway',
            ]
        );

        $repeater->add_control(
            'price', [
                'label' => esc_html__( 'Price', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '$39.<sup>99</sup>',
            ]
        );

        $repeater->add_control(
            'package', [
                'label' => esc_html__( 'Package', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Weekly',
            ]
        );

        $repeater->add_control(
            'icon', [
                'label' => esc_html__( 'Icon Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'description' => esc_html__( 'Recommended to upload a SVG to PNG image.', 'rogan-core' ),
                'default' => [
                    'url' => plugins_url('images/icon/icon15.svg', __FILE__)
                ]
            ]
        );

        $repeater->add_control(
            'table_header_color',
            [
                'label' => esc_html__( 'Border Top Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}} .single-pr-table.subway .pr-header' => 'border-color: {{VALUE}}',
                ],
            ]
        );

        $repeater->add_control(
            'slogan', [
                'label' => esc_html__( 'Table Features', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Quick job, Small Work',
            ]
        );

        $repeater->add_control(
            'contents', [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' =>Controls_Manager::WYSIWYG,
            ]
        );

        $repeater->add_control(
            'hr_btn',
            [
                'type' => \Elementor\Controls_Manager::DIVIDER,
            ]
        );

        $repeater->add_control(
            'btn_label',
            [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Choose Plan',
            ]
        );

        $repeater->add_control(
            'btn_url',
            [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url'   => '#'
                ]
            ]
        );

        $repeater->add_control(
            'trial_btn_label',
            [
                'label' => esc_html__( 'Trial Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Get your 30 day free trial',
            ]
        );

        $repeater->add_control(
            'trial_btn_url',
            [
                'label' => esc_html__( 'Trial Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url'   => '#'
                ]
            ]
        );

        $this->add_control(
            'pricing_tables',
            [
                'label' => esc_html__( 'Pricing Tables', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{title}}}'
            ]
        );

        $this->end_controls_section();


        // *********************************** Section Upper Title Color ******************************//
        $this->start_controls_section(
            'style_upper_title_color_sec', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'upper_title_color',
            [
                'label' => esc_html__( 'Upper Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .upper-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'upper_title_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one .upper-title',
            ]
        );

        $this->end_controls_section(); //End Upper Title


        // *********************************** Section Main Title Color ******************************//
        $this->start_controls_section(
            'style_main_title_color_sec', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'main_title_color',
            [
                'label' => esc_html__( 'Main Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'main_title_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section(); // End Main Title

        //------------------------------ Item Button Style  ------------------------------ //
        $this->start_controls_section(
            'item_btn_style', [
                'label' => esc_html__( 'Item Button', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        //---------------------------- Normal and Hover ---------------------------//
        $this->start_controls_tabs(
            'btn_style_tabs'
        );

        // Normal Color
        $this->start_controls_tab(
            'normal_btn_style',
            [
                'label' => __( 'Normal', 'saasland-core' ),
            ]
        );

        $this->add_control(
            'normal_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .plan-button' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'normal_bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .plan-button' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'normal_border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .plan-button' => 'border-color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();


        // Hover Color
        $this->start_controls_tab(
            'hover_btn_style',
            [
                'label' => __( 'Hover', 'saasland-core' ),
            ]
        );

        $this->add_control(
            'hover_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .plan-button:hover' => 'color: {{VALUE}}',
                ]
            ]
        );

        $this->add_control(
            'hover_bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .plan-button:hover' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'hover_border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .plan-button:hover' => 'border-color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'sec_bg_color',
            [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-our-pricing.section-bg' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .seo-our-pricing.section-bg' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->add_control(
            'sec_bg_img',
            [
                'label' => esc_html__( 'Background Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {

        $settings = $this->get_settings();
        $tables = $settings['pricing_tables'];
        ?>
        <div class="seo-our-pricing section-bg" style="background: url (<?php echo esc_url( $settings['sec_bg_img']['url'] ) ?>) ">
            <div class="container">
                <div class="theme-title-one title-underline text-center">
                    <?php if (!empty($settings['upper_title'])) : ?>
                        <div class="upper-title"><?php echo esc_html($settings['upper_title']) ?></div>
                    <?php endif; ?>
                    <?php if (!empty($settings['main_title'])) : ?>
                        <h2 class="main-title"><?php echo wp_kses_post(nl2br($settings['main_title'])) ?></h2>
                    <?php endif; ?>
                </div> <!-- /.theme-title-one -->
                <div class="row">
                    <?php
                    if (!empty($tables)) {
                    $i = 0;
                    foreach ($tables as $table) {
                        ?>
                        <div class="col-lg-4 elementor-repeater-item-<?php echo $table['_id'] ?>" data-aos="fade-up"
                            <?php if($i != 0) : ?> data-aos-delay="<?php echo $i; ?>" <?php endif; ?>>
                            <div class="single-pr-table subway">
                                <div class="pr-header">
                                    <?php if (!empty($table['icon']['url'])) : ?>
                                        <img src="<?php echo esc_url($table['icon']['url']) ?>" alt="<?php echo esc_attr($table['title']) ?>" class="icon" data-aos="fade-right">
                                    <?php endif; ?>
                                    <?php if (!empty($table['title'])) : ?>
                                        <h4 class="title"><?php echo esc_html($table['title']) ?></h4>
                                    <?php endif; ?>
                                    <?php if (!empty($table['price'])) : ?>
                                        <div class="price"><?php echo wp_kses_post($table['price']) ?></div>
                                    <?php endif; ?>
                                    <?php if (!empty($table['package'])) : ?>
                                        <div class="package"><?php echo esc_html($table['package']) ?></div>
                                    <?php endif; ?>
                                </div> <!-- /.pr-header -->
                                <div class="pr-body">
                                    <?php if (!empty($table['slogan'])) : ?>
                                        <h3 class="slogan"><?php echo esc_html($table['slogan']) ?></h3>
                                    <?php endif; ?>
                                    <?php if (!empty($table['contents'])) : ?>
                                        <?php echo wp_kses_post($table['contents']) ?>
                                    <?php endif; ?>
                                </div> <!-- /.pr-body -->
                                <div class="pr-footer">
                                    <?php if (!empty($table['btn_label'])) : ?>
                                        <a href="<?php echo esc_url($table['btn_url']['url']) ?>" class="plan-button line-button-two"
                                            <?php rogan_is_external($table['btn_url']); rogan_is_nofollow($table['btn_url']) ?>>
                                            <?php echo esc_html($table['btn_label']) ?>
                                        </a>
                                    <?php endif; ?>
                                    <?php if (!empty($table['trial_btn_label'])) : ?>
                                        <a href="<?php echo esc_url($table['trial_btn_url']['url']) ?>" class="trial-button"
                                            <?php rogan_is_external($table['trial_btn_url']); rogan_is_nofollow($table['trial_btn_url']) ?>>
                                            <?php echo esc_html($table['trial_btn_label']) ?>
                                        </a>
                                    <?php endif; ?>
                                </div> <!-- /.pr-footer -->
                            </div> <!-- /.single-pr-table -->
                        </div>
                        <?php
                        $i = $i + 200;
                    }}
                    ?>
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div>
        <?php
    }
}