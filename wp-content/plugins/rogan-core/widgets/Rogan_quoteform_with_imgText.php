<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_quoteform_with_imgText extends Widget_Base {

    public function get_name() {
        return 'rogan_cf7_with_bg_img';
    }

    public function get_title() {
        return __( 'Quote Form <br> with Image & Texts', 'rogan-core' );
    }

    public function get_icon() {
        return ' eicon-form-horizontal';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        //******************************* Title Section***************************************//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__('Section Title', 'rogan-core'),
            ]
        );

        $this->add_control(
            'upper_title', [
                'label' => esc_html__('Upper Title', 'rogan-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Contact Us',
            ]
        );

        $this->add_control(
            'main_title', [
                'label' => esc_html__('Main Title', 'rogan-core'),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'Send us messages for any info.',
            ]
        );

        $this->end_controls_section();


        //******************************* Sub title  Section***************************************//
        $this->start_controls_section(
            'subtitle_sec',
            [
                'label' => esc_html__('Section Subtitle', 'rogan-core'),
            ]
        );

        $this->add_control(
            'subtitle', [
                'label' => esc_html__('Subtitle', 'rogan-core'),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'tel_number', [
                'label' => esc_html__('Tel Number', 'rogan-core'),
                'type' => Controls_Manager::WYSIWYG,
            ]
        );

        $this->end_controls_section();


        //----------------------------- Contact Form Shortcode --------------------------------------//
        $this->start_controls_section(
            'contact_form_sec',
            [
                'label' => esc_html__( 'Contact Form', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'cf7_shortcode',
            [
                'label' => esc_html__( 'Shortcode', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tab
         * Upper Title
         */
        $this->start_controls_section(
            'upper_title_color_sec',
            [
                'label' => esc_html__( 'Upper Title', 'rogan-core'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'upper_title_color',
            [
                'label' => esc_html__( 'Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one.arch-title .upper-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'upper_title_typography',
                'label' => esc_html__('Typography', 'rogan-core'),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one.arch-title .upper-title',
            ]
        );

        $this->end_controls_section();


        //--------------------------------------- Section Title --------------------------------------------//
        $this->start_controls_section(
            'section_main_title_color',
            [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'main_title_color',
            [
                'label' => esc_html__( 'Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .main-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'main_title_typography',
                'label' => esc_html__('Typography', 'rogan-core'),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .main-title',
            ]
        );

        $this->end_controls_section();


        //------------------------------------- Background Section -----------------------------------//
        $this->start_controls_section(
            'bg_style_sec',
            [
                'label' => esc_html__('Background', 'rogan-core'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'bg_img',
            [
                'label' => esc_html__( 'Background Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_margin', [
                'label' => esc_html__( 'Section Margin', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .arch-contact' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings();
        ?>
        <div class="arch-contact" style="background: url(<?php echo esc_url( $settings['bg_img']['url']) ?>);
                background-size: cover;">
            <div class="form-wrapper">
                <div class="container">
                    <div class="inner-wrapper">
                        <div class="row">
                            <div class="col-lg-6 order-lg-last">
                                <div class="contact-text">
                                    <div class="theme-title-one arch-title">
                                        <?php if ( !empty( $settings['upper_title'] ) ) : ?>
                                            <div class="upper-title"><?php echo esc_html( $settings['upper_title']) ?></div>
                                        <?php endif; ?>
                                        <?php if ( !empty( $settings['main_title'] ) ) : ?>
                                            <h2 class="main-title"><?php echo esc_html( $settings['main_title']) ?></h2>
                                        <?php endif; ?>
                                    </div>
                                    <?php if ( !empty( $settings['subtitle'] ) ) : ?>
                                        <p class="font-lato"><?php echo esc_html( $settings['subtitle']) ?></p>
                                    <?php endif; ?>
                                    <?php if ( !empty( $settings['tel_number'] ) ) : ?>
                                        <a href="#" class="call-us font-lato">
                                            <?php echo wp_kses_post( $settings['tel_number']) ?>
                                        </a>
                                    <?php endif; ?>
                                </div> <!-- /.contact-text -->
                            </div> <!-- /.col- -->
                            <?php if ( !empty( $settings['cf7_shortcode'] ) ) : ?>
                                <div class="col-lg-6 order-lg-first">
                                    <?php echo do_shortcode( $settings['cf7_shortcode']) ?>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div> <!-- /.inner-wrapper -->
                </div> <!-- /.container -->
            </div> <!-- /.form-wrapper -->
        </div>
        <?php
    }
}