<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_op_image_slider extends Widget_Base {

    public function get_name() {
        return 'rogan_op_hero_area';
    }

    public function get_title() {
        return esc_html__( 'Slider with Vertical Nav', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-carousel';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_style_depends() {
        return ['owl-carousel', 'owl-theme', 'animate'];
    }

    public function get_script_depends() {
        return ['owl-carousel'];
    }

    protected function _register_controls() {

        // ------------------------------ one page hero slider ------------------------------
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__( 'Hero Slides', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'slider_img', [
                'label' => esc_html__( 'Hero Slide BG Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $repeater->add_control(
            'slide_text', [
                'label' => esc_html__( 'Slide Heading Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Agency'
            ]
        );

        $this->add_control(
            'hero_slides', [
                'label' => esc_html__( 'Hero Slider', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ slide_text }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section();

        /**
         * Style Tab
         * ------------------------------ Style Title ------------------------------
         */
        $this->start_controls_section(
            'style_title', [
                'label' => esc_html__( 'Style Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_title', [
                'label' => esc_html__( 'Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #op-hero-one .carousel-inner h2' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'heading_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} #op-hero-one .carousel-inner h2',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings();

        ?>
        <div class="op-hero-area-one">
            <div class="main-carousel-wrapper">
                <div id="op-hero-one" class="carousel slide" data-ride="carousel" data-interval="5000">
                    <div class="carousel-inner">
                        <?php
                        $counter = 0;
                        if (!empty($settings['hero_slides'])) {
                        foreach ($settings['hero_slides'] as $slider) {
                            $hero_slider = isset($slider['slider_img']['id']) ? $slider['slider_img']['id'] : '';
                            $hero_slider = wp_get_attachment_image_src($hero_slider, 'full');
                            $counter++;
                            ?>
                            <?php if (!empty($hero_slider[0])) : ?>

                                <div class="carousel-item <?=($counter == 1) ? 'active' : ''?>" style="background-image: url('<?php echo $hero_slider[0] ?>');">
                                    <div class="inner-container">
                                    <?php if(!empty($slider['slide_text'])) : ?>
                                        <h2 data-animation="animated fadeInUp"><?php echo wp_kses_post( $slider['slide_text'] ); ?></h2>
                                    <?php endif; ?>
                                    </div> <!-- /.inner-container -->
                                </div> <!-- /.carousel-item -->
                            <?php endif; ?>
                            <?php
                        }}
                        ?>
                    </div> <!-- /.carousel-inner -->
                    <div class="carousel-nav">
                        <div>
                            <a class="carousel-control-next" href="#op-hero-one" role="button" data-slide="next">
                                <?php echo esc_html_e( 'Next', 'rogan-core' ); ?>
                                <span class="sr-only">Next</span>
                            </a>
                            <a class="carousel-control-prev" href="#op-hero-one" role="button" data-slide="prev">
                                <?php echo esc_html_e( 'Prev', 'rogan-core' ); ?>
                                <span class="sr-only">Prev</span>
                            </a>
                        </div>
                    </div> <!-- /.carousel-nav -->
                </div>
            </div> <!-- /.main-carousel-wrapper -->
        </div> <!-- /.op-hero-area-one -->
        <?php
    }
}