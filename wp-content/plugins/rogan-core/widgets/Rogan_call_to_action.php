<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_call_to_action extends Widget_Base {

    public function get_name() {
        return 'Rogan_contact_banner';
    }

    public function get_title() {
        return esc_html__( 'Call to Action', 'rogan-core' );
    }

    public function get_icon() {
        return ' eicon-counter-circle';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ----------------------------------------  Section Style ------------------------------
        $this->start_controls_section(
            'sec_style',
            [
                'label' => esc_html__( 'Select Style', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'banner_style', [
                'label' => esc_html__( 'Select Style', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'style_1' => esc_html__('Style One (Saas)', 'rogan-core'),
                    'style_2' => esc_html__('Style Two (SEO)', 'rogan-core'),
                ],
                'default' => 'style_1'
            ]
        );

        $this->end_controls_section();

        //******************************* Title Section***************************************//
        $this->start_controls_section(
            'title_section',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'Getting connected with us & <br>for the update.',
            ]
        );

        $this->end_controls_section();

        /****************************** shape Image **************************/
        $this->start_controls_section(
            'bg_letter_shape_sec',
            [
                'label' => esc_html__( 'Background Letter Shape', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'letter_shape1',
            [
                'label' => esc_html__( 'Left Letter Shape', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'condition' => [
                    'banner_style' => 'style_1'
                ],
                'default' => 'U'
            ]
        );

        $this->add_control(
            'letter_shape2',
            [
                'label' => esc_html__( 'Right Letter Shape', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'condition' => [
                    'banner_style' =>  [ 'style_1', 'style_2' ]
                ],
                'default' => 'D'
            ]
        );

        $this->end_controls_section();


        //******************************* Button Section***************************************//
        $this->start_controls_section(
            'button_sec',
            [
                'label' => esc_html__( 'Button', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'btn_label', [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'btn_url', [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                        'url' => '#'
                ]
            ]
        );

        //---------------------------- Normal and Hover ---------------------------//
        $this->start_controls_tabs(
            'style_tabs'
        );


        // Normal Color
        $this->start_controls_tab(
            'normal_btn_style',
            [
                'label' => __( 'Normal', 'saasland-core' ),
            ]
        );

        $this->add_control(
            'normal_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-contact-banner .contact-button' => 'color: {{VALUE}}',
                    '{{WRAPPER}} .subscribe-button' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'normal_bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-contact-banner .contact-button' => 'background: {{VALUE}}',
                    '{{WRAPPER}} .subscribe-button' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'normal_border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-contact-banner .contact-button' => 'border-color: {{VALUE}}',
                    '{{WRAPPER}} .subscribe-button' => 'border-color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();


        // Hover Color
        $this->start_controls_tab(
            'hover_btn_style',
            [
                'label' => __( 'Hover', 'saasland-core' ),
            ]
        );

        $this->add_control(
            'hover_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-contact-banner .contact-button:hover' => 'color: {{VALUE}}',
                    '{{WRAPPER}} .subscribe-button:hover' => 'color: {{VALUE}}',
                ]
            ]
        );

        $this->add_control(
            'hover_bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-contact-banner .contact-button:hover' => 'background: {{VALUE}}',
                    '{{WRAPPER}} .subscribe-button:hover' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'hover_border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-contact-banner .contact-button:hover' => 'border-color: {{VALUE}}',
                    '{{WRAPPER}} .subscribe-button:hover' => 'border-color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        /**
         * Style Tab
         * Background Shape
         */
        /****************************** Section Title Color **************************/
        $this->start_controls_section(
            'sec_title_style',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        // Style One
        $this->add_control(
            'saas_title_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .sass-contact-banner .title' => 'color: {{VALUE}}',
                    '{{WRAPPER}} .seo-contact-banner .title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'typography_saas_title',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .sass-contact-banner .title,
                    {{WRAPPER}} .seo-contact-banner .title
                    ',
            ]
        );

        $this->end_controls_section();

        //-------------------------------------------- Background Shape Color --------------------------------------------//
        $this->start_controls_section(
            'bg_shape_color_sec',
            [
                'label' => esc_html__( 'Background Shape Color', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        // Seo Contact banner
        $this->add_control(
            'round_shape_one',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-contact-banner .round-shape-one' => 'background: {{VALUE}};',
                ],
                'condition' => [
                    'banner_style' => 'style_2'
                ]
            ]
        );

        $this->add_control(
            'round_shape_two',
            [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-contact-banner .round-shape-two' => 'background: {{VALUE}};',
                ],
                'condition' => [
                    'banner_style' => 'style_2'
                ]
            ]
        );


        $this->end_controls_section();

        //-------------------------------------------- Background Gradient --------------------------------------------//
        $this->start_controls_section(
            'gradient_bg_color_sec',
            [
                'label' => esc_html__( 'Background Gradient', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        // Style One Gradient color
        $this->add_control(
            'bg_color', [
                'label' => esc_html__( 'Background Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );

        $this->add_control(
            'bg_color2', [
                'label' => esc_html__( 'Background Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .sass-contact-banner' => 'background: linear-gradient( -134deg, {{bg_color.VALUE}} 0%, {{VALUE}} 100%);',
                    '{{WRAPPER}} .seo-contact-banner' => 'background: linear-gradient( -135deg, {{bg_color.VALUE}} 0%, {{VALUE}} 100%);',
                ],
            ]
        );

        $this->end_controls_section(); // End


        /**
         * Background Shape
         */
        /****************************** shape Image **************************/
        $this->start_controls_section(
            'sec_bg_style',
            [
                'label' => esc_html__( 'Section Style', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        // Background Image
        $this->add_control(
            'bg_img',
            [
                'label' => esc_html__( 'Background Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $this->add_responsive_control(
            'margin',
            [
                'label' => __( 'Margin', 'rogan-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .seo-contact-banner' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    '{{WRAPPER}} .sass-contact-banner' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'padding',
            [
                'label' => __( 'Padding', 'rogan-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .seo-contact-banner' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    '{{WRAPPER}} .sass-contact-banner' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        ?>
        <?php
        if ( $settings['banner_style'] == 'style_1' ) :
            ?>
            <div class="sass-contact-banner">
                <?php if ( !empty( $settings['bg_img']['url'] ) ) : ?>
                    <style>
                        .seo-contact-banner:before {
                            background-image: url(<?php echo esc_url($settings['bg_img']['url']) ?>);
                        }
                    </style>
                <?php endif; ?>
                <?php if (!empty($settings['letter_shape1'])) : ?>
                    <div class="u-shape"><?php echo esc_html($settings['letter_shape1']) ?></div>
                <?php endif; ?>
                <?php if(!empty($settings['letter_shape2'])) : ?>
                    <div class="d-shape"><?php echo esc_html($settings['letter_shape2']) ?></div>
                <?php endif; ?>
                <div class="container">
                    <?php if (!empty($settings['title_text'])) : ?>
                        <h2 class="title"><?php echo wp_kses_post(nl2br($settings['title_text'])) ?></h2>
                    <?php endif; ?>
                    <?php if (!empty($settings['btn_label'])) : ?>
                        <a href="<?php echo esc_url($settings['btn_url']['url']) ?>" class="subscribe-button"
                            <?php rogan_is_external($settings['btn_url']); rogan_is_nofollow($settings['btn_url'])  ?>>
                            <?php echo esc_html($settings['btn_label']) ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        <?php
        elseif ( $settings['banner_style'] == 'style_2' ) :
            ?>
            <div class="seo-contact-banner">
                <?php if ( !empty( $settings['bg_img']['url'] ) ) : ?>
                    <style>
                        .seo-contact-banner:before {
                            background-image: url(<?php echo esc_url($settings['bg_img']['url']) ?>);
                        }
                    </style>
                <?php endif; ?>
                <div class="round-shape-one"></div>
                <div class="round-shape-two"></div>
                <?php if (!empty($settings['letter_shape2'])) : ?>
                    <div class="d-shape"><?php echo esc_html($settings['letter_shape2']) ?></div>
                <?php endif; ?>
                <div class="container">
                    <?php if (!empty($settings['title_text'])) : ?>
                        <h2 class="title"><?php echo wp_kses_post(nl2br($settings['title_text'])) ?></h2>
                    <?php endif; ?>
                    <?php if (!empty($settings['btn_label'])) : ?>
                        <a href="<?php echo esc_url($settings['btn_url']['url']) ?>" class="contact-button"
                            <?php rogan_is_external($settings['btn_url']); rogan_is_nofollow($settings['btn_url'])  ?>>
                            <?php echo esc_html($settings['btn_label']) ?>
                        </a>
                    <?php endif; ?>
                </div> <!-- /.contianer -->
            </div>
            <?php
        endif;
    }
}