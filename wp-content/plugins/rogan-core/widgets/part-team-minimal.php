<div class="team-minimal our-team pt-150 pb-200">
    <div class="container">
        <div class="row">
            <?php
            if ( !empty($members) ) {
                foreach ($members as $member) {
                    $image = isset($member['image']['id']) ? $member['image']['id'] : '';
                    $image = wp_get_attachment_image_src($image, 'full');
                    ?>
                    <div class="col-lg-4 col-md-6 elementor-repeater-item-<?php echo $member['_id'] ?>">
                        <div class="single-team-member">
                            <div class="wrapper">
                                <div class="img-box">
                                    <?php if ( !empty($image[0]) ) : ?>
                                        <img src="<?php echo $image[0] ?>" alt="<?php echo esc_attr($member['name']) ?>">
                                    <?php endif; ?>
                                    <div class="hover-content">
                                        <ul>
                                            <?php if ( !empty($member['facebook']['url']) ) : ?>
                                                <li><a href="<?php echo esc_url($member['facebook']['url']) ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <?php endif ?>
                                            <?php if ( !empty($member['twitter']['url']) ) : ?>
                                                <li><a href="<?php echo esc_url($member['twitter']['url']) ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <?php endif; ?>
                                            <?php if ( !empty($member['linkedin']['url']) ) : ?>
                                                <li><a href="<?php echo esc_url($member['linkedin']['url']) ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            <?php endif; ?>
                                            <?php if ( !empty($member['instagram']['url']) ) : ?>
                                                <li><a href="<?php echo esc_url($member['instagram']['url']) ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            <?php endif; ?>
                                            <?php if ( !empty($member['google_plus']['url']) ) : ?>
                                                <li><a href="<?php echo esc_url($member['google_plus']['url']) ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                            <?php endif; ?>
                                            <?php if ( !empty($member['dribbble']['url']) ) : ?>
                                                <li><a href="<?php echo esc_url($member['dribbble']['url']) ?>"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div> <!-- /.hover-content -->
                                </div>
                                <div class="info-meta">
                                    <?php if ( !empty($member['name']) ) : ?>
                                        <h6 class="name"><?php echo esc_html($member['name']) ?></h6>
                                    <?php endif; ?>
                                    <?php if ( !empty($member['designation']) ) : ?>
                                        <span><?php echo esc_html($member['designation']) ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div> <!-- /.single-team-member -->
                    </div>
                    <?php
                }}
            ?>
        </div> <!-- /.row -->
        <?php if (!empty($settings['btn_label'])) : ?>
            <div class="text-center pt-15">
                <a href="<?php echo esc_url($settings['btn_url']['url']) ?>" class="theme-button-one"
                    <?php rogan_is_external($settings['btn_url']); rogan_is_nofollow($settings['btn_url']) ?>>
                    <?php echo esc_html($settings['btn_label']) ?>
                </a>
            </div>
        <?php endif; ?>
    </div> <!-- /.container -->
</div>