<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Scheme_color;
use Elementor\Group_Control_Typography;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_our_goal_app extends Widget_Base {

    public function get_name() {
        return 'Rogan_our_goal_app';
    }

    public function get_title() {
        return esc_html__( 'Our Goal App', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-device-desktop';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ----------------------------------------  Hero Title ------------------------------
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Title Section', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Why choose us'
            ]
        );
        $this->end_controls_section();

        // ----------------------------------------  Section Section  ------------------------------
        $this->start_controls_section(
            'subtitle_sec',
            [
                'label' => esc_html__( 'Subtitle Section', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'subtitle',
            [
                'label' => esc_html__( 'Subtitle Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );
        $this->end_controls_section();

        // ----------------------------------------  Contents  -----------------------------------------
        $this->start_controls_section(
            'content_sec',
            [
                'label' => esc_html__( 'Content Section', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'contents_list',
            [
                'label' => esc_html__( 'Contents List', 'rogan-core' ),
                'type' => Controls_Manager::WYSIWYG,
            ]
        );

        $this->end_controls_section();

        // ********************************* Author Section  ***********************************//
        $this->start_controls_section(
            'author_sec',
            [
                'label' => esc_html__( 'Author Section', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'name',
            [
                'label' => esc_html__( 'Author Name', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default'   => 'Eh Jewel'
            ]
        );

        $this->add_control(
            'designating',
            [
                'label' => esc_html__( 'Designating', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default'   => 'Managing Director inc.'
            ]
        );

        $this->add_control(
            'author_img',
            [
                'label' => esc_html__( 'Author Image', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
            ]
        );

        $this->add_control(
            'signature',
            [
                'label' => esc_html__( 'Author Image', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
            ]
        );

        $this->end_controls_section();




        //*****************************  First Featured image *******************************************//
        $this->start_controls_section(
            'featured_sec',
            [
                'label' => esc_html__( 'Featured Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'featured_img',
            [
                'label' => esc_html__( 'Featured Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );
        $this->end_controls_section();

        /**
         * Style Tab
         * Font Color
         * Background Color
         * ------------------------------ Style Title ------------------------------
         */
        $this->start_controls_section(
            'style_title_sec', [
                'label' => esc_html__( 'Title Section', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .why-choose-us-app .text-wrapper .title-box' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'border_color',
            [
                'label' => esc_html__( 'Border Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .why-choose-us-app .text-wrapper .title-box' => 'border-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'title_typography',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} .why-choose-us-app .text-wrapper .title-box',
            ]
        );

        $this->end_controls_section();

         /* ------------------------------ Style Subtitle ------------------------------*/
        $this->start_controls_section(
            'style_subtitle_sec', [
                'label' => esc_html__( 'Subtitle Section', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'subtitle_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .why-choose-us-app .text-wrapper .bottom-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'subtitle_typography',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} .why-choose-us-app .text-wrapper .bottom-title',
            ]
        );

        $this->end_controls_section();

         /* ------------------------------ Style Contents ------------------------------*/
        $this->start_controls_section(
            'style_contents_sec', [
                'label' => esc_html__( 'Content Section', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'contents_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .why-choose-us-app .text-wrapper ul li' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'content_typography',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} .why-choose-us-app .text-wrapper ul li',
            ]
        );

        $this->end_controls_section();

        //------------------------------ Gradient Color ------------------------------
        $this->start_controls_section(
            'bg_gradient_sec', [
                'label' => esc_html__( 'Background Gradient', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'bg_color', [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
            ]
        );

        $this->add_control(
            'bg_color2', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .why-choose-us-app' => 'background: linear-gradient( -35deg, {{bg_color.VALUE}} 0%, {{VALUE}} 100%);',
                ],
            ]
        );
        $this->end_controls_section();

        /* ------------------------------ Style Contents ------------------------------*/
        $this->start_controls_section(
            'style_round_sec', [
                'label' => esc_html__( 'Background Round Wave Color', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'round_one',
            [
                'label' => esc_html__( 'Round One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .why-choose-us-app .bg-shape-holder .big-round-one' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'round_two',
            [
                'label' => esc_html__( 'Round Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .why-choose-us-app .bg-shape-holder .big-round-two' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'round_three',
            [
                'label' => esc_html__( 'Round Three', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .why-choose-us-app .bg-shape-holder .big-round-three' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();


    }

    protected function render() {

        $settings = $this->get_settings();
        $image = isset($settings['author_img']['id']) ? $settings['author_img']['id'] : '';
        $image = wp_get_attachment_image_src($image, 'full');
        $signature = isset($settings['signature']['id']) ? $settings['signature']['id'] : '';
        $signature = wp_get_attachment_image_src($signature, 'full');
        ?>
        <div class="why-choose-us-app">
            <div class="bg-shape-holder">
                <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
                <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
                <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
            </div>
            <div class="container">
                <div class="clearfix">
                    <div class="text-wrapper">
                        <?php if (!empty($settings['title'])) : ?>
                            <div class="title-box"><?php echo esc_html($settings['title']) ?></div>
                        <?php endif; ?>
                        <?php if (!empty($settings['subtitle'])) : ?>
                            <h5 class="bottom-title"><?php echo esc_html($settings['subtitle']) ?></h5>
                        <?php endif; ?>
                        <?php if (!empty($settings['contents_list'])) : ?>
                            <?php echo wp_kses_post($settings['contents_list']) ?>
                        <?php endif; ?>
                        <div class="director-speech clearfix">
                            <img src="<?php echo esc_url($image[0]) ?>" alt="<?php echo esc_attr($settings['title']) ?>" class="d-img">
                            <?php if(!empty($settings['name'])) : ?>
                                <div class="bio-block">
                                    <h6 class="name"><?php echo esc_html($settings['name']) ?></h6>
                                    <span><?php echo esc_html($settings['designating']) ?></span>
                                </div> <!-- /.bio-block -->
                            <?php endif ?>
                            <img src="<?php echo esc_url($signature[0]) ?>" alt="<?php echo esc_attr($settings['name']) ?>" class="sign">
                        </div> <!-- /.director-speech -->
                    </div> <!-- /.text-wrapper -->
                </div>
            </div> <!-- /.container -->
            <div class="screen-preview">
                <?php echo wp_get_attachment_image($settings['featured_img']['id'], 'full') ?>
            </div>
        </div>
        <?php
    }
}
