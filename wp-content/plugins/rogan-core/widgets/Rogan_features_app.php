<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_features_app extends Widget_Base {

    public function get_name() {
        return 'rogan_features_app';
    }

    public function get_title() {
        return esc_html__( 'Features (App)', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-home-heart';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ***************************** Service Title **************************************//
        $this->start_controls_section(
            'title_sec', [
                'label' => esc_html__( 'Title Section', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text',
            [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Service We Offer',
            ]
        );

        $this->end_controls_section();


        // *************************************** Service List *************************************//
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__( 'Service List', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'default' => esc_html__( 'Default Title' , 'rogan-core' ),
                'label_block' => true
            ]
        );

        $repeater->add_control(
            'title_url',
            [
                'label' => esc_html__( 'Title URL', 'rogan-core' ),
                'type' =>Controls_Manager::URL,
                'default' => [
                    'url' => '#',
                ]
            ]
        );

        $repeater->add_control(
            'subtitle',
            [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
                'type' =>Controls_Manager::TEXTAREA,
            ]
        );

        $repeater->add_control(
            'image',
            [
                'label' => esc_html__( 'Background Image', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-33.svg', __FILE__)
                ]
            ]
        );

        $repeater->add_control(
            'images',
            [
                'label' => esc_html__( 'Featured Images', 'rogan-core' ),
                'type' =>Controls_Manager::GALLERY,
            ]
        );

        $this->end_controls_tab();
        $this->add_control(
            'service_list',
            [
                'label' => esc_html__( 'Services', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{title}}}'
            ]
        );

        $this->end_controls_section();

        // *************************************** Service List *************************************//
        $this->start_controls_section(
            'buttons_sec',
            [
                'label' => esc_html__( 'Button', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'btn_label',
            [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'See More Details',
            ]
        );

        $this->add_control(
            'btn_url',
            [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'label_block' => true,
                'default' => [
                    'url' => '#',
                ]
            ]
        );

        $this->add_control(
            'feature_url',
            [
                'label' => esc_html__( 'Feature URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'label_block' => true,
                'default' => [
                    'url' => '#',
                ]
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tab
         * @Title
         */
        /********************************* Title ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_title',
            [
                'label' => esc_html__('Text Color', 'rogan-core'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );


        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_upper_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'label' => esc_html__( 'Upper Title Typography', 'rogan-core' ),
                'selector' => '
                    {{WRAPPER}} .theme-title-one .main-title',
            ]
        );


        $this->end_controls_section();


        /********************************* Service List Style ********************************/
        $this->start_controls_section(
            'style_sec_subtitle', [
                'label' => esc_html__( 'Section Service List', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_service_title',
            [
                'label' => esc_html__( 'Title Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-service-app .main-content .inner-wrapper .title a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'color_service_subtitle',
            [
                'label' => esc_html__( 'Subtitle Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-service-app .main-content .inner-wrapper p' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_service_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'label' => esc_html__( 'Title Typography', 'rogan-core' ),
                'selector' => '
                    {{WRAPPER}} .our-service-app .main-content .inner-wrapper .title a',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_service_subtitle',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'label' => esc_html__( 'Subtitle Typography', 'rogan-core' ),
                'selector' => '
                    {{WRAPPER}} .our-service-app .main-content .inner-wrapper p',
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        $services = $settings['service_list'];
        ?>
        <div class="our-service-app">
            <div class="theme-title-one text-center">
                <?php if (!empty($settings['title_text'])) : ?>
                    <h2 class="main-title"><?php echo esc_html($settings['title_text']) ?></h2>
                <?php endif; ?>
            </div> <!-- /.theme-title-one -->

            <div class="main-content hide-pr">
                <div class="row">
                    <?php
                    if (!empty($services)) {
                        foreach ($services as $service) {
                            ?>
                            <div class="col-lg-4 single-block elementor-repeater-item-<?php echo $service['_id'] ?>">
                                <div class="inner-wrapper">
                                    <div class="illustration-box">
                                        <img src="<?php echo esc_url($service['image']['url']) ?>"
                                             alt="<?php echo esc_attr($service['title']) ?>" class="bg-shape">
                                        <?php
                                        if( !empty($service['images']) ) {
                                            foreach ($service['images'] as $i => $image) {
                                                switch ($i) {
                                                    case 0:
                                                        $img_class = 'block-shape-one';
                                                        break;
                                                    case 1:
                                                        $img_class = 'block-shape-two';
                                                        break;
                                                    case 2:
                                                        $img_class = 'block-shape-three';
                                                        break;
                                                    case 3:
                                                        $img_class = 'block-shape-four';
                                                        break;
                                                    default:
                                                        $img_class = 'block-shape-' . $i;
                                                        break;
                                                }
                                                echo wp_get_attachment_image($image['id'], 'full', '', array('class' => $img_class));
                                            }
                                        }
                                        ?>
                                    </div>
                                    <?php if (!empty($service['title'])) : ?>
                                        <h4 class="title">
                                            <a href="<?php echo esc_url($service['title_url']['url']) ?>">
                                            <?php echo esc_html($service['title']) ?>
                                            </a>
                                        </h4>
                                    <?php endif; ?>
                                    <?php echo !empty($service['subtitle']) ? wpautop($service['subtitle']) : ''; ?>
                                </div> <!-- /.inner-wrapper -->
                            </div>
                            <?php
                        }}
                    ?>
                </div> <!-- /.row -->
                <?php if (!empty($settings['btn_label'])) : ?>
                    <a href="<?php echo esc_url($settings['btn_url']['url']) ?>" class="more-button"
                        <?php rogan_is_external($settings['btn_url']); rogan_is_nofollow($settings['btn_url']) ?>>
                        <?php echo esc_html($settings['btn_label']) ?>
                    </a>
                <?php endif; ?>

                <a href="#feature" class="down-arrow scroll-target"><span><i class="flaticon-back"></i></span></a>

            </div> <!-- /.main-content -->
        </div>
        <?php
    }
}