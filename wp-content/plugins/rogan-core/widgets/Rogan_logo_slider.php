<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_logo_slider extends Widget_Base {

    public function get_name() {
        return 'rogan_logo_slider';
    }

    public function get_title() {
        return esc_html__( 'Logo Slider', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-carousel';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_style_depends() {
        return ['owl-carousel', 'owl-theme', 'animate'];
    }

    public function get_script_depends() {
        return ['owl-carousel'];
    }

    protected function _register_controls() {

        // ------------------------------ slider Logo ------------------------------
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__( 'Logo', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'alt', [
                'label' => esc_html__( 'Image alt Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Company Name'
            ]
        );

        $repeater->add_control(
            'sliders', [
                'label' => esc_html__( 'Logos', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $repeater->add_control(
            'logo_url', [
                'label' => esc_html__( 'Logo URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'label_block' => true,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $this->add_control(
            'logo_slides', [
                'label' => esc_html__( 'Logo Slider', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ alt }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        ?>
        <div class="trusted-partner">
            <div class="container">
                <div class="partner-slider">
                    <?php
                    if (!empty($settings['logo_slides'])) {
                    foreach ($settings['logo_slides'] as $slider) {
                        $logo_slider = isset($slider['sliders']['id']) ? $slider['sliders']['id'] : '';
                        $logo_slider = wp_get_attachment_image_src($logo_slider, 'full');
                        $target = $slider['logo_url']['is_external'] ? 'target="_blank"' : '';
                        ?>
                        <?php if (!empty($logo_slider[0])) : ?>
                            <div class="item <?php echo $slider['_id'] ?>">
                                <a href="<?php echo esc_url($slider['slides_url']['url']) ?>" <?php echo $target ?>>
                                    <img src="<?php echo $logo_slider[0] ?>" alt="<?php echo esc_attr($slider['alt']) ?>">
                                </a>
                            </div>
                        <?php endif; ?>
                        <?php
                    }}
                    ?>
                </div>
            </div>
        </div>

        <script>
            (function ( $ ) {
                "use strict";
                $(document).on ('ready', function (){
                    var logoslider = $ (".partner-slider");
                    if(logoslider.length) {
                        logoslider.owlCarousel({
                            loop:true,
                            nav:false,
                            dots:false,
                            autoplay:true,
                            autoplayTimeout:4000,
                            autoplaySpeed:1000,
                            lazyLoad:true,
                            singleItem:true,
                            center:true,
                            responsive:{
                                0:{
                                    items:1
                                },
                                550:{
                                    items:3
                                },
                                992:{
                                    items:5
                                }
                            }
                        });
                    }
                });
            }( jQuery ));
        </script>
        <?php
    }
}