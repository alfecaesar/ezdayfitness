<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_hero_seo extends Widget_Base {

    public function get_name() {
        return 'Rogan_hero_seo';
    }

    public function get_title() {
        return esc_html__( 'Hero SEO', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-device-desktop';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ----------------------------------------  Hero Title ------------------------------
        $this->start_controls_section(
            'hero_title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text',
            [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'Boostup your <br> web traffic is just <br>a click away.'
            ]
        );

        $this->end_controls_section();

        // ----------------------------------------  Hero Subtitle  -----------------------------------------
        $this->start_controls_section(
            'hero_subtitle',
            [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'subtitle_text',
            [
                'label' => esc_html__( 'Subtitle Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->end_controls_section(); // End Subtitle

        //*****************************  First Featured image *******************************************//
        $this->start_controls_section(
            'featured_sec',
            [
                'label' => esc_html__( 'Featured Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'featured_img', [
                'label' => esc_html__( 'Featured Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/banner-shape4.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'horizontal',
            [
                'label' => __( 'Horizontal', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-two .illustration' => 'left: {{SIZE}}{{UNIT}}; right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'vertical',
            [
                'label' => __( 'vertical', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-two .illustration' => 'top: {{SIZE}}{{UNIT}}; bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        /// --------------------------------------- Buttons ----------------------------------///
        $this->start_controls_section(
            'buttons_sec',
            [
                'label' => esc_html__( 'Buttons', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'btn_label', [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Learn More'
            ]
        );

        $repeater->add_control(
            'btn_url',
            [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        //---------------------------- Normal and Hover ---------------------------//
        $repeater->start_controls_tabs(
            'style_tabs'
        );


        // Normal ColorFeatured Color
        $repeater->start_controls_tab(
            'normal_btn_style',
            [
                'label' => __( 'Normal', 'saasland-core' ),
            ]
        );

        $repeater->add_control(
            'normal_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'color: {{VALUE}} !important',
                ],
            ]
        );

        $repeater->add_control(
            'normal_bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'background: {{VALUE}} !important',
                ],
            ]
        );

        $repeater->add_control(
            'normal_border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}{{CURRENT_ITEM}}' => 'border-color: {{VALUE}} !important',
                ],
            ]
        );

        $repeater->end_controls_tab();


        // Hover Color
        $repeater->start_controls_tab(
            'hover_btn_style',
            [
                'label' => __( 'Hover', 'saasland-core' ),
            ]
        );

        $repeater->add_control(
            'hover_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'color: {{VALUE}} !important',
                ]
            ]
        );

        $repeater->add_control(
            'hover_bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'background: {{VALUE}} !important',
                ],
            ]
        );

        $repeater->add_control(
            'hover_border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'border-color: {{VALUE}} !important',
                ],
            ]
        );

        $repeater->end_controls_tab();

        $repeater->end_controls_tabs();

        $this->add_control(
            'buttons', [
                'label' => esc_html__( 'Create buttons', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ btn_label }}}',
                'fields' => $repeater->get_controls(),
            ]
        );
        
        $this->end_controls_section();

        /**
         * Style Tab
         * Font Color
         * Background Color
         * ------------------------------ Style Title ------------------------------
         */
        $this->start_controls_section(
            'style_title', [
                'label' => esc_html__( 'Style Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_prefix', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-two .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_prefix',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} #theme-banner-two .main-title',
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Subtitle ------------------------------
        $this->start_controls_section(
            'style_subtitle', [
                'label' => esc_html__( 'Style Subtitle', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_subtitle', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-two .sub-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_subtitle',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} #theme-banner-two .sub-title',
            ]
        );

        $this->end_controls_section();


        //------------------------------ Featured Background Style  ------------------------------
        $this->start_controls_section(
            'featured_bg_style',
            [
                'label' => esc_html__( 'Bottom Right Corner Shadow', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'fbg_color',
            [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
            ]
        );

        $this->add_control(
            'fbg_color2', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-two .bg-round-five' => 'background: linear-gradient( -90deg, {{fbg_color.VALUE}} 0%, {{VALUE}} 100%);',
                ],
            ]
        );

        $this->end_controls_section();

        //------------------------------ Section Background ------------------------------ //
        $this->start_controls_section(
            'bg_color_section', [
                'label' => esc_html__( 'Section Background', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_control(
            'sec_bg_color', [
                'label' => esc_html__( 'Background Color 01', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );

        $this->add_control(
            'bg_color2', [
                'label' => esc_html__( 'Background Color 02', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-two' => 'background: linear-gradient( -120deg, {{sec_bg_color.VALUE}} 0%, {{VALUE}} 100%);',
                ],
            ]
        );

        $this->add_control(
            'bg_img', [
                'label' => esc_html__( 'Background Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {

        $settings = $this->get_settings();
        $buttons = $settings['buttons'];
        ?>
        <div id="theme-banner-two">
            <style>
                #theme-banner-two:before {
                    background: url(<?php echo esc_url($settings['bg_img']['url']) ?>)no-repeat left top;
                }
            </style>
            <div class="bg-round-one wow zoomIn animated" data-wow-duration="5s"></div>
            <div class="bg-round-two wow zoomIn animated" data-wow-duration="5s"></div>
            <div class="bg-round-three wow zoomIn animated" data-wow-duration="5s"></div>
            <div class="bg-round-four wow zoomIn animated" data-wow-duration="5s"></div>
            <div class="bg-round-five wow zoomIn animated" data-wow-duration="5s"></div>
            <div class="illustration wow fadeInRight animated" data-wow-duration="2s" data-wow-delay="0.4s">
                <?php if (!empty($settings['featured_img']['url'])) : ?>
                    <img src="<?php echo esc_url($settings['featured_img']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title_text'])) ?>"></div>
                <?php endif; ?>
            <div class="container">
                <div class="main-wrapper">
                    <?php if (!empty($settings['title_text'])) : ?>
                        <h1 class="main-title wow fadeInUp animated" data-wow-delay="0.4s"><?php echo wp_kses_post(nl2br($settings['title_text'])) ?></h1>
                    <?php endif; ?>
                    <?php if (!empty($settings['subtitle_text'])) : ?>
                        <p class="sub-title wow fadeInUp animated" data-wow-delay="0.9s"><?php echo wp_kses_post(nl2br($settings['subtitle_text'])) ?></p>
                    <?php endif; ?>
                    <ul class="button-group">
                        <?php
                        $i = 0;
                        foreach ($buttons as $button) {
                            ++$i;
                            $strip_class = ($i % 2 == 1) ? 'more-button' : 'contact-button';
                            $strip_anim = ($i % 2 == 1) ? '1' : '1.5';
                            $icon_class = ($i % 2 == 1) ? '<i class="fa fa-angle-right" aria-hidden="true"></i>' : '';
                            ?>
                            <li>
                                <a href="<?php echo esc_url($button['btn_url']['url']) ?>" class="<?php echo esc_attr($strip_class) ?> wow fadeInLeft elementor-repeater-item-<?php echo $button['_id'] ?>" data-wow-delay="<?php echo esc_attr($strip_anim) ?>s"
                                    <?php rogan_is_external($button['btn_url']); rogan_is_nofollow($button['btn_url']) ?>>
                                    <?php echo esc_html($button['btn_label']) ?>
                                    <?php if (!empty($icon_class)) : ?>
                                        <?php echo $icon_class ?>
                                    <?php endif; ?>
                                </a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php
    }
}
