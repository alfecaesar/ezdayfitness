<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_pricing_table_features extends Widget_Base {

    public function get_name() {
        return 'rogan_pricing_table_features';
    }

    public function get_title() {
        return esc_html__( 'Features Pricing Table', 'rogan-hero' );
    }

    public function get_icon() {
        return ' eicon-price-table';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {


        // ----------------------------------------  Section Style ------------------------------
        $this->start_controls_section(
            'sec_style',
            [
                'label' => esc_html__( 'Select Style', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'pricing_table_style', [
                'label' => esc_html__( 'Select Style', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'style_1' => esc_html__('Style One (Agency)', 'rogan-core'),
                    'style_2' => esc_html__('Style Two (App)', 'rogan-core'),
                ],
                'default' => 'style_1'
            ]
        );

        $this->end_controls_section();


        //*****************************  Title Section  *********************************//
        $this->start_controls_section(
            'title_sec', [
                'label' => esc_html__( 'Title Section', 'rogan-core' ),
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'upper_title',
            [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Pricing Plan',
                'condition' => [
                    'pricing_table_style' => 'style_1'
                ]
            ]
        );


        $this->add_control(
            'main_title', [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'No Hidden Charges! Choose <br> your Plan.',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->end_controls_section();


        // ********************************* Features Table tabs ***********************************//
        $this->start_controls_section(
            'features_table_sec',
            [
                'label' => esc_html__( 'Features Section', 'rogan-core' ),
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'A Sample Feature Title',
            ]
        );

        $repeater->add_control(
            'is_yes',
            [
                'label' => esc_html__( 'Plan 01', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Yes', 'rogan-core' ),
                'label_off' => esc_html__( 'No', 'rogan-core' ),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $repeater->add_control(
            'is_yes_two',
            [
                'label' => esc_html__( 'Plan 02', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Yes', 'rogan-core' ),
                'label_off' => esc_html__( 'No', 'rogan-core' ),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $repeater->add_control(
            'is_yes_three',
            [
                'label' => esc_html__( 'Plan 03', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Yes', 'rogan-core' ),
                'label_off' => esc_html__( 'No', 'rogan-core' ),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'features',
            [
                'label' => esc_html__( 'Pricing Tables', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{title}}}'
            ]
        );

        $this->end_controls_section();

        //*****************************  Pricing Plan One  *********************************//
        $this->start_controls_section(
            'plan_one_sec',
            [
                'label' => esc_html__( 'Pricing Plan One', 'rogan-core' ),
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan1_title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Sandwich',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan1_subtitle',
            [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'REGULAR',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan1_price',
            [
                'label' => esc_html__( 'Price', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '$2300',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan1_duration',
            [
                'label' => esc_html__( 'Duration', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'MO',
                'condition' => [
                    'pricing_table_style' => 'style_1',
                ]
            ]
        );

        $this->add_control(
            'plan1_coins',
            [
                'label' => esc_html__( 'Coins', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '99',
                'condition' => [
                    'pricing_table_style' => 'style_2',
                ]
            ]
        );

        $this->add_control(
            'hr_one',
            [
                'type' => Controls_Manager::DIVIDER,
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan1_btn_label',
            [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Choose Plan',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan1_btn_url',
            [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url'   => '#'
                ],
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan1_btm_text',
            [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Get your 30 day free trial',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->end_controls_section();

        //*****************************  Pricing Plan Two  *********************************//
        $this->start_controls_section(
            'plan_two_sec',
            [
                'label' => esc_html__( 'Pricing Plan Two', 'rogan-core' ),
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan2_title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Subway',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan2_subtitle',
            [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'PREMIUM',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan2_price',
            [
                'label' => esc_html__( 'Price', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '$6000',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan2_duration',
            [
                'label' => esc_html__( 'Duration', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'MO',
                'condition' => [
                    'pricing_table_style' => 'style_1'
                ]
            ]
        );

        $this->add_control(
            'plan2_coins',
            [
                'label' => esc_html__( 'Coins', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '99',
                'condition' => [
                    'pricing_table_style' => 'style_2'
                ]
            ]
        );

        $this->add_control(
            'hr_two',
            [
                'type' => Controls_Manager::DIVIDER,
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan2_btn_label',
            [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Choose Plan',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan2_btn_url',
            [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url'   => '#'
                ],
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan2_btm_text',
            [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Get your 30 day free trial',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );



        $this->end_controls_section();

        //*****************************  Pricing Plan Three  *********************************//
        $this->start_controls_section(
            'plan_three_sec',
            [
                'label' => esc_html__( 'Pricing Plan Three', 'rogan-core' ),
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan3_title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Burger',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan3_subtitle',
            [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'CORPORATE',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan3_price',
            [
                'label' => esc_html__( 'Price', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '$9500',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan3_duration',
            [
                'label' => esc_html__( 'Duration', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'MO',
                'condition' => [
                    'pricing_table_style' => 'style_1'
                ]
            ]
        );

        $this->add_control(
            'plan3_coins',
            [
                'label' => esc_html__( 'Coins', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '99',
                'condition' => [
                    'pricing_table_style' => 'style_2'
                ]
            ]
        );

        $this->add_control(
            'hr_three',
            [
                'type' => Controls_Manager::DIVIDER,
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan3_btn_label',
            [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Choose Plan',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan3_btn_url',
            [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url'   => '#'
                ],
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan3_btm_text',
            [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Get your 30 day free trial',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->end_controls_section();
        
        /**
         * Style Tabs
         * Color
         * Background Shapes
         */
        // *********************************** Section Title Color ******************************//
        $this->start_controls_section(
            'style_upper_title_sec', [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'pricing_table_style' => 'style_1',
                ]
            ]
        );

        $this->add_control(
            'upper_title_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .upper-title' => 'color: {{VALUE}}',
                ],
                'condition' => [
                    'pricing_table_style' => 'style_1'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'upper_title_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one .upper-title',
                'condition' => [
                    'pricing_table_style' => 'style_1'
                ]
            ]
        );

        $this->end_controls_section();

        //---------------------------------------- Main Title -------------------------------------------//
        $this->start_controls_section(
            'style_main_title_sec', [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'main_title_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}}',
                ],
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'main_title_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one .main-title',
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->end_controls_section();

        // *********************************** Section Features Tables Color ******************************//
        $this->start_controls_section(
            'pricing_plan_sec', [
                'label' => esc_html__( 'Pricing Plan Section', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'pricing_table_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'plan_title_color',
            [
                'label' => esc_html__( 'Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-our-pricing .table-wrapper .pr-column .pr-header .title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'plan_subtitle_color',
            [
                'label' => esc_html__( 'Subtitle Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-our-pricing .table-wrapper .pr-column .pr-header .package' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'plan_price_color',
            [
                'label' => esc_html__( 'Price Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-our-pricing .table-wrapper .pr-column .pr-header .price' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'plan_title_typography',
                'label' => esc_html__( 'Title Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .agn-our-pricing .table-wrapper .pr-column .pr-header .title',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'plan_subtitle_typography',
                'label' => esc_html__( 'Subtitle Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .agn-our-pricing .table-wrapper .pr-column .pr-header .package',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'plan_price_typography',
                'label' => esc_html__( 'Price Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .agn-our-pricing .table-wrapper .pr-column .pr-header .price',
            ]
        );

        $this->end_controls_section();

        //************************************* Background Shape ***************************************//
        $this->start_controls_section(
            'bg_shapes',
            [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'pricing_table_style' => 'style_1',
                ]
            ]
        );

        $this->add_control(
            'shape1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-55.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'shape2', [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-62.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'shape3', [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-1.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'shape4', [
                'label' => esc_html__( 'Shape Four', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-60.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'shape5', [
                'label' => esc_html__( 'Shape Five', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-57.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .agn-our-pricing' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    '{{WRAPPER}} .pricing-app' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {

        $settings = $this->get_settings();
        ?>
        <?php
        if ($settings['pricing_table_style'] == 'style_1') :
            ?>
        <div class="agn-our-pricing pt-150 pb-200">
            <?php if (!empty($settings['shape1']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape1']['url']) ?>" alt="<?php echo esc_attr($settings['upper_title']) ?>" class="shape-one">
            <?php endif; ?>
            <?php if (!empty($settings['shape2']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape2']['url']) ?>" alt="<?php echo esc_attr($settings['upper_title']) ?>" class="shape-two">
            <?php endif; ?>
            <?php if (!empty($settings['shape3']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape3']['url']) ?>" alt="<?php echo esc_attr($settings['upper_title']) ?>" class="shape-three">
            <?php endif; ?>
            <?php if (!empty($settings['shape4']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape4']['url']) ?>" alt="<?php echo esc_attr($settings['upper_title']) ?>" class="shape-four">
            <?php endif; ?>
            <?php if (!empty($settings['shape5']['url'])) : ?>
            <img src="<?php echo esc_url($settings['shape5']['url']) ?>" alt="<?php echo esc_attr($settings['upper_title']) ?>" class="shape-five">
            <?php endif ?>
            <div class="container">
                <div class="theme-title-one text-center">
                    <?php if (!empty($settings['upper_title'])) : ?>
                        <div class="upper-title"><?php echo esc_html($settings['upper_title']) ?></div>
                    <?php endif; ?>
                    <?php if (!empty($settings['main_title'])) : ?>
                        <h2 class="main-title"><?php echo wp_kses_post(nl2br($settings['main_title'])) ?></h2>
                    <?php endif; ?>
                </div> <!-- /.theme-title-one -->
            </div> <!-- /.container -->
            <div class="table-wrapper">
                <div class="inner-table clearfix">
                    <ul class="pr-list-text">
                        <?php
                        if(!empty($settings['features'])) {
                            foreach($settings['features'] as $feature) {
                                echo "<li>{$feature['title']}</li>";
                            }
                        }
                        ?>
                    </ul> <!-- /.pr-list-text -->
                    <div class="pr-column">
                        <div class="pr-header">
                            <?php if(!empty($settings['plan1_price'])) : ?>
                                <div class="price"> <?php echo esc_html($settings['plan1_price']) ?> <span> <?php echo esc_html($settings['plan1_duration']) ?></span></div>
                            <?php endif; ?>
                            <?php if(!empty($settings['plan1_title'])) : ?>
                                <h4 class="title"> <?php echo esc_html($settings['plan1_title']) ?> </h4>
                            <?php endif; ?>
                            <?php if(!empty($settings['plan1_subtitle'])) : ?>
                                <div class="package"> <?php echo esc_html($settings['plan1_subtitle']) ?> </div>
                            <?php endif; ?>
                        </div> <!-- /.pr-header -->
                        <ul class="pr-body">
                            <?php
                            if(!empty($settings['features'])) {
                                foreach($settings['features'] as $feature) {
                                    $is_yes = ($feature['is_yes'] == 'yes') ? 'flaticon-tick available' : 'flaticon-cancel none';
                                    ?>
                                    <li>
                                        <div class="pr-text"> <?php echo esc_html($feature['title'])  ?> </div>
                                        <i class="icon <?php echo esc_attr($is_yes) ?>"></i>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                        <div class="pr-footer">
                            <?php if (!empty($settings['plan1_btn_label'])) :  ?>
                                <a href="<?php echo esc_url($settings['plan1_btn_url']['url']) ?>"
                                    <?php rogan_is_external($settings['plan1_btn_url']); rogan_is_nofollow($settings['plan1_btn_url']) ?>
                                    class="choose-button line-button-two">
                                    <?php echo esc_html($settings['plan1_btn_label']) ?>
                                </a>
                            <?php endif; ?>
                                <?php echo !empty($settings['plan1_btm_text']) ? wpautop($settings['plan1_btm_text']) : ''; ?>
                        </div>
                    </div> <!-- /.pr-column -->

                    <div class="pr-column">
                        <div class="pr-header">
                            <?php if(!empty($settings['plan2_price'])) : ?>
                                <div class="price"> <?php echo esc_html($settings['plan2_price']) ?> <span> <?php echo esc_html($settings['plan2_duration']) ?></span></div>
                            <?php endif; ?>
                            <?php if(!empty($settings['plan2_title'])) : ?>
                                <h4 class="title"> <?php echo esc_html($settings['plan2_title']) ?> </h4>
                            <?php endif; ?>
                            <?php if(!empty($settings['plan2_subtitle'])) : ?>
                                <div class="package"> <?php echo esc_html($settings['plan2_subtitle']) ?> </div>
                            <?php endif; ?>
                        </div> <!-- /.pr-header -->
                        <ul class="pr-body">
                            <?php
                            if(!empty($settings['features'])) {
                                foreach($settings['features'] as $feature) {
                                    $is_yes = ($feature['is_yes_two'] == 'yes') ? 'flaticon-tick available' : 'flaticon-cancel none';
                                    ?>
                                    <li>
                                        <div class="pr-text"> <?php echo esc_html($feature['title'])  ?> </div>
                                        <i class="icon <?php echo esc_attr($is_yes) ?>"></i>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                        <div class="pr-footer">
                            <?php if (!empty($settings['plan2_btn_label'])) :  ?>
                                <a href="<?php echo esc_url($settings['plan2_btn_url']['url']) ?>"
                                    <?php rogan_is_external($settings['plan2_btn_url']); rogan_is_nofollow($settings['plan2_btn_url']) ?>
                                    class="choose-button line-button-two">
                                    <?php echo esc_html($settings['plan2_btn_label']) ?>
                                </a>
                            <?php endif; ?>
                            <?php echo !empty($settings['plan2_btm_text']) ? wpautop($settings['plan2_btm_text']) : ''; ?>
                        </div>
                    </div> <!-- /.pr-column -->
                    <div class="pr-column">
                        <div class="pr-header">
                            <?php if(!empty($settings['plan3_price'])) : ?>
                                <div class="price"> <?php echo esc_html($settings['plan3_price']) ?> <span> <?php echo esc_html($settings['plan3_duration']) ?></span></div>
                            <?php endif; ?>
                            <?php if(!empty($settings['plan3_title'])) : ?>
                                <h4 class="title"> <?php echo esc_html($settings['plan3_title']) ?> </h4>
                            <?php endif; ?>
                            <?php if(!empty($settings['plan3_subtitle'])) : ?>
                                <div class="package"> <?php echo esc_html($settings['plan3_subtitle']) ?> </div>
                            <?php endif; ?>
                        </div> <!-- /.pr-header -->
                        <ul class="pr-body">
                            <?php
                            if(!empty($settings['features'])) {
                                foreach($settings['features'] as $feature) {
                                    $is_yes = ($feature['is_yes_three'] == 'yes') ? 'flaticon-tick available' : 'flaticon-cancel none';
                                    ?>
                                    <li>
                                        <div class="pr-text"> <?php echo esc_html($feature['title'])  ?> </div>
                                        <i class="icon <?php echo esc_attr($is_yes) ?>"></i>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                        <div class="pr-footer">
                            <?php if (!empty($settings['plan3_btn_label'])) :  ?>
                                <a href="<?php echo esc_url($settings['plan3_btn_url']['url']) ?>"
                                    <?php rogan_is_external($settings['plan3_btn_url']); rogan_is_nofollow($settings['plan3_btn_url']) ?>
                                   class="choose-button line-button-two">
                                    <?php echo esc_html($settings['plan3_btn_label']) ?>
                                </a>
                            <?php endif; ?>
                            <?php echo !empty($settings['plan3_btm_text']) ? wpautop($settings['plan3_btm_text']) : ''; ?>
                        </div>
                    </div> <!-- /.pr-column -->
                </div> <!-- /.inner-table -->
            </div> <!-- /.table-wrapper -->
        </div> <!-- /.agn-our-pricing -->

        <?php
        elseif ($settings['pricing_table_style'] == 'style_2') :
            ?>
            <div class="agn-our-pricing pricing-app pt-150 pb-200">
                <div class="container">
                    <?php if (!empty($settings['main_title'])) : ?>
                        <div class="theme-title-one upper-bar text-center">
                            <h2 class="main-title"><?php echo wp_kses_post(nl2br($settings['main_title'])) ?></h2>
                        </div> <!-- /.theme-title-one -->
                    <?php endif; ?>
                </div> <!-- /.container -->
                <div class="table-wrapper">
                    <div class="inner-table clearfix">
                        <ul class="pr-list-text">
                            <?php
                            if(!empty($settings['features'])) {
                                foreach($settings['features'] as $feature) {
                                    echo "<li>{$feature['title']}</li>";
                                }
                            }
                            ?>
                        </ul> <!-- /.pr-list-text -->
                        <div class="pr-column">
                            <div class="pr-header">
                                <?php if(!empty($settings['plan1_subtitle'])) : ?>
                                    <div class="package"><?php echo esc_html($settings['plan1_subtitle']) ?></div>
                                <?php endif; ?>
                                <?php if(!empty($settings['plan1_price'])) : ?>
                                    <div class="price"><?php echo esc_html($settings['plan1_price']) ?><sup><?php echo esc_html($settings['plan1_coins']) ?></sup></div>
                                <?php endif; ?>
                                <?php if(!empty($settings['plan1_title'])) : ?>
                                    <h4 class="title"><?php echo esc_html($settings['plan1_title']) ?></h4>
                                <?php endif; ?>
                            </div> <!-- /.pr-header -->

                            <ul class="pr-body">
                                <?php
                                if(!empty($settings['features'])) {
                                    foreach($settings['features'] as $feature) {
                                        $is_yes = ($feature['is_yes'] == 'yes') ? 'flaticon-tick available' : 'flaticon-cancel none';
                                        ?>
                                        <li>
                                            <div class="pr-text"> <?php echo esc_html($feature['title'])  ?> </div>
                                            <i class="icon <?php echo esc_attr($is_yes) ?>"></i>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                            <div class="pr-footer">
                                <?php if (!empty($settings['plan1_btn_label'])) :  ?>
                                    <a href="<?php echo esc_url($settings['plan1_btn_url']['url']) ?>"
                                        <?php rogan_is_external($settings['plan1_btn_url']); rogan_is_nofollow($settings['plan1_btn_url']) ?>
                                        class="choose-button line-button-two">
                                        <?php echo esc_html($settings['plan1_btn_label']) ?>
                                    </a>
                                <?php endif; ?>
                                <?php echo !empty($settings['plan1_btm_text']) ? wpautop($settings['plan1_btm_text']) : ''; ?>
                            </div>
                        </div> <!-- /.pr-column -->
                        <div class="pr-column">
                            <div class="pr-header">
                                <?php if(!empty($settings['plan2_subtitle'])) : ?>
                                    <div class="package"><?php echo esc_html($settings['plan2_subtitle']) ?></div>
                                <?php endif; ?>
                                <?php if(!empty($settings['plan2_price'])) : ?>
                                    <div class="price"><?php echo esc_html($settings['plan2_price']) ?><sup><?php echo esc_html($settings['plan2_coins']) ?></sup></div>
                                <?php endif; ?>
                                <?php if(!empty($settings['plan2_title'])) : ?>
                                    <h4 class="title"><?php echo esc_html($settings['plan2_title']) ?></h4>
                                <?php endif; ?>

                            </div> <!-- /.pr-header -->

                            <ul class="pr-body">
                                <?php
                                if(!empty($settings['features'])) {
                                    foreach($settings['features'] as $feature) {
                                        $is_yes = ($feature['is_yes_two'] == 'yes') ? 'flaticon-tick available' : 'flaticon-cancel none';
                                        ?>
                                        <li>
                                            <div class="pr-text"> <?php echo esc_html($feature['title'])  ?> </div>
                                            <i class="icon <?php echo esc_attr($is_yes) ?>"></i>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                            <div class="pr-footer">
                                <?php if (!empty($settings['plan2_btn_label'])) :  ?>
                                    <a href="<?php echo esc_url($settings['plan2_btn_url']['url']) ?>"
                                        <?php rogan_is_external($settings['plan2_btn_url']); rogan_is_nofollow($settings['plan2_btn_url']) ?>
                                       class="choose-button line-button-two">
                                        <?php echo esc_html($settings['plan2_btn_label']) ?>
                                    </a>
                                <?php endif; ?>
                                <?php echo !empty($settings['plan2_btm_text']) ? wpautop($settings['plan2_btm_text']) : ''; ?>
                            </div>
                        </div> <!-- /.pr-column -->
                        <div class="pr-column">
                            <div class="pr-header">
                                <?php if(!empty($settings['plan3_subtitle'])) : ?>
                                    <div class="package"><?php echo esc_html($settings['plan3_subtitle']) ?></div>
                                <?php endif; ?>
                                <?php if(!empty($settings['plan3_price'])) : ?>
                                    <div class="price"><?php echo esc_html($settings['plan3_price']) ?><sup><?php echo esc_html($settings['plan3_coins']) ?></sup></div>
                                <?php endif; ?>
                                <?php if(!empty($settings['plan3_title'])) : ?>
                                    <h4 class="title"><?php echo esc_html($settings['plan3_title']) ?></h4>
                                <?php endif; ?>
                            </div>
                            <ul class="pr-body">
                                <?php
                                if(!empty($settings['features'])) {
                                    foreach($settings['features'] as $feature) {
                                        $is_yes = ($feature['is_yes_three'] == 'yes') ? 'flaticon-tick available' : 'flaticon-cancel none';
                                        ?>
                                        <li>
                                            <div class="pr-text"> <?php echo esc_html($feature['title'])  ?> </div>
                                            <i class="icon <?php echo esc_attr($is_yes) ?>"></i>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                            <div class="pr-footer">
                                <?php if (!empty($settings['plan3_btn_label'])) :  ?>
                                    <a href="<?php echo esc_url($settings['plan3_btn_url']['url']) ?>"
                                        <?php rogan_is_external($settings['plan3_btn_url']); rogan_is_nofollow($settings['plan3_btn_url']) ?>
                                       class="choose-button line-button-two">
                                        <?php echo esc_html($settings['plan3_btn_label']) ?>
                                    </a>
                                <?php endif; ?>
                                <?php echo !empty($settings['plan3_btm_text']) ? wpautop($settings['plan3_btm_text']) : ''; ?>
                            </div>
                        </div> <!-- /.pr-column -->
                    </div> <!-- /.inner-table -->
                </div> <!-- /.table-wrapper -->
            </div>
            <?php
        endif; ?>
        <?php
    }
}