<div class="our-service service-creative">
    <?php if( $settings['top_sec'] == 'yes' ) : ?>
        <div class="our-history mb-200">
            <div class="container">
                <div class="row <?php echo $is_row_reverse_top_sec; ?>">
                    <div class="col-lg-7 order-lg-last">
                        <div class="text-wrapper">
                            <?php echo wp_kses_post(wpautop($settings['content'])) ?>
                        </div>
                    </div>
                    <div class="col-lg-5 order-lg-first">
                        <div class="img-box">
                            <?php echo ( !empty($settings['image_01']['id']) ) ? wp_get_attachment_image($settings['image_01']['id'], 'full') : ''; ?>
                            <?php if ( !empty($settings['image_02']['id'])) {
                                echo wp_get_attachment_image($settings['image_02']['id'], 'full', '', array( 'class' => 'bottom-img', 'data-aos' => 'fade-up' ));
                            }
                            ?>
                        </div> <!-- /.img-box -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.our-history -->
    <?php endif; ?>

    <?php
    $i = 0;
    while ( $service->have_posts() ) : $service->the_post();
        $bg_color = function_exists('get_field') ? get_field('service_background_color') : '';
        $number = ( $i < 10 ) ? '0'.( 1 + $i) : $i;
        ++$i;
        ?>
        <div class="service-block" <?php echo !empty( $bg_color ) ? "style=\"background: $bg_color;\"" : 'style="background: #04cce8;"'; ?>>
            <div class="container">
                <div class="demo-container-1100">
                    <div class="row">
                        <div class="col-lg-6 order-lg-<?php echo ($i % 2 == 0) ? 'first' : 'last'; ?>" data-aos="fade-<?php echo ($i % 2 == 0) ? 'left' : 'right'; ?>">
                            <div class="service-info">
                                <div class="num"> <?php echo $number; ?> </div>
                                <h2 class="service-title">
                                    <a href="<?php the_permalink() ?>">
                                        <?php the_title() ?>
                                    </a>
                                </h2>
                                <?php the_excerpt() ?>
                                <a href="<?php the_permalink() ?>" class="read-more"><i class="flaticon-next-1"></i></a>
                            </div> <!-- /.service-info -->
                        </div> <!-- /.col- -->
                        <div class="col-lg-6 order-lg-<?php echo ($i % 2 == 0) ? 'last' : 'first'; ?>" data-aos="fade-right">
                            <div class="img-box"> <?php the_post_thumbnail('full') ?> </div>
                        </div>
                    </div> <!-- /.row -->
                </div> <!-- /.demo-container-1100 -->
            </div> <!-- /.container -->
        </div> <!-- /.service-block -->
    <?php
    endwhile;
    wp_reset_postdata();
    ?>
</div>