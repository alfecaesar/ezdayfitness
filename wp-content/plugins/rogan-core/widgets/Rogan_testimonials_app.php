<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_testimonials_app extends Widget_Base {

    public function get_name() {
        return 'rogan_testimonials_app';
    }

    public function get_title() {
        return __( 'Testimonials <br> with Partner Logo', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-testimonial-carousel';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_keywords() {
        return [ 'logo carousel', 'testimonial', 'App Testimonail' ];
    }

    public function get_script_depends() {
        return ['owl-carousel'];
    }

    public function get_style_depends() {
        return ['owl-carousel', 'owl-theme', 'animate'];
    }

    protected function _register_controls() {

        //***********************************  Testimonials Title ******************************************//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Title Section', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'What’s Our Customer <br> Saying.'
            ]
        );


        $this->end_controls_section();

        //*********************************** Round Letter Section ******************************************//
        $this->start_controls_section(
            'latter_sec',
            [
                'label' => esc_html__( 'Round Letter Text', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'latter_shape1',
            [
                'label' => esc_html__( 'Letter', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'D'
            ]
        );

        $this->add_control(
            'latter_shape2',
            [
                'label' => esc_html__( 'Another Letter', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'U'
            ]
        );


        $this->end_controls_section();


        //********************************* Testimonials List **********************************//
        $this->start_controls_section(
            'testimonials_sec',
            [
                'label' => esc_html__( 'Testimonials Slider', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'author_name',
            [
                'label' => esc_html__( 'Author Name', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Eh Jewel'
            ]
        );

        $repeater->add_control(
            'designation',
            [
                'label' => esc_html__( 'Designation', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Manager, Walton'
            ]
        );

        $repeater->add_control(
            'author_img',
            [
                'label' => esc_html__( 'Author Image', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
            ]
        );

        $repeater->add_control(
            'contents',
            [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' =>Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'testimonials_sliders',
            [
                'label' => esc_html__( 'Sliders', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ author_name }}}',
            ]
        );

        $this->end_controls_section();


        // ------------------------------ slider Logo ------------------------------
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__( 'Partner Logo Section', 'rogan-core' ),
            ]
        );

        $image = new \Elementor\Repeater();

        $image->add_control(
            'alt', [
                'label' => esc_html__( 'Image alt Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Company Name'
            ]
        );

        $image->add_control(
            'sliders', [
                'label' => esc_html__( 'Logos', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $image->add_control(
            'logo_url', [
                'label' => esc_html__( 'Logo URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'label_block' => true,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $this->add_control(
            'logo_slides', [
                'label' => esc_html__( 'Logo Slider', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ alt }}}',
                'fields' => $image->get_controls(),
                'prevent_empty' => false
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tabs
         */
        /********************************* Section Title ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_title',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'label' => esc_html__( 'Text Typography', 'rogan-core' ),
                'selector' => '
                    {{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();


        //********************************* Background Shape Image ********************************//
        $this->start_controls_section(
            'bg_shapes',
            [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE
            ]
        );

        $this->add_control(
            'shape1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-52.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape2',
            [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-53.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape3',
            [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-45.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape4',
            [
                'label' => esc_html__( 'Shape Four', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-46.svg', __FILE__)
                ],
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding',
            [
                'label' => __( 'Padding', 'plugin-name' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'devices' => [ 'desktop', 'tablet', 'mobile' ],
                'selectors' => [
                    '{{WRAPPER}} .apps-testimonial' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'background_color',
            [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .apps-testimonial' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        $testimonials = $settings['testimonials_sliders'];

        ?>

        <div class="apps-testimonial">
            <?php if (!empty($settings['latter_shape1'])) : ?>
            <div class="d-shape" data-aos="fade-right" data-aos-duration="2000"><?php echo esc_html($settings['latter_shape1']) ?></div>
            <?php endif; ?>
            <?php if (!empty($settings['latter_shape2'])) : ?>
            <div class="u-shape" data-aos="fade-left" data-aos-duration="2000"><?php echo esc_html($settings['latter_shape2']) ?></div>
            <?php endif; ?>
            <?php if (!empty($settings['shape1']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape1']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title_text'])) ?>" class="shape-one">
            <?php endif; ?>
            <?php if (!empty($settings['shape2']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape2']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title_text'])) ?>" class="shape-two">
            <?php endif; ?>
            <?php if (!empty($settings['shape3']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape3']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title_text'])) ?>" class="shape-three">
            <?php endif; ?>
            <?php if (!empty($settings['shape4']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape4']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title_text'])) ?>" class="shape-four">
            <?php endif; ?>
            <div class="container">
                <div class="theme-title-one upper-bar">
                    <?php if (!empty($settings['title_text'])) : ?>
                        <h2 class="main-title"><?php echo wp_kses_post($settings['title_text']) ?></h2>
                    <?php endif; ?>
                </div> <!-- /.theme-title-one -->

                <div class="customer-slider">
                    <?php
                    if (!empty($testimonials)) {
                    foreach ($testimonials as $testimonial) {
                        $image = isset($testimonial['author_img']['id']) ? $testimonial['author_img']['id'] : '';
                        $image = wp_get_attachment_image_src($image, 'full');
                        ?>
                        <div class="item elementor-repeater-item-<?php echo $testimonial['_id'] ?>">
                            <div class="customer-block">
                                <?php echo !empty($testimonial['contents']) ? wpautop($testimonial['contents']) : ''; ?>
                                <div class="clearfix">
                                    <img src="<?php echo $image[0] ?>" alt="<?php echo esc_attr($testimonial['author_name']) ?>" class="customer-img">
                                    <div class="customer-info">
                                        <?php if (!empty($testimonial['author_name'])) : ?>
                                            <h5 class="name"><?php echo esc_html($testimonial['author_name']) ?></h5>
                                        <?php endif; ?>
                                        <?php if (!empty($testimonial['designation'])) : ?>
                                            <span><?php echo esc_html($testimonial['designation']) ?></span>
                                        <?php endif; ?>
                                    </div> <!-- /.customer-info -->
                                </div>
                            </div> <!-- /.customer-block -->
                        </div> <!-- /.item -->
                        <?php
                    }}
                    ?>
                </div>
            </div> <!-- /.container -->

            <?php
            if ( !empty($settings['logo_slides']) ) :
                ?>
                <div class="trusted-partner">
                    <div class="container">
                        <div class="partner-slider">
                            <?php
                            foreach ($settings['logo_slides'] as $slider) {
                                $logo_slider = isset($slider['sliders']['id']) ? $slider['sliders']['id'] : '';
                                $logo_slider = wp_get_attachment_image_src($logo_slider, 'full');
                                $this->add_render_attribute('logo_url', array(
                                    'href' => $slider['logo_url']['url'],
                                ));
                                ?>
                                <?php if (!empty($logo_slider[0])) : ?>
                                    <div class="item <?php echo $slider['_id'] ?>">
                                        <a <?php echo $this->get_render_attribute_string( 'logo_url' ); ?> <?php rogan_is_external($slider['logo_url']) ?>>
                                            <img src="<?php echo $logo_slider[0] ?>" alt="<?php echo esc_attr($slider['alt']) ?>">
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            endif;
            ?>
        </div>


        <script>
            (function ( $ ) {
                "use strict";
                $(document).ready(function (){
                    var agnTslider = $ (".agn-testimonial-slider");
                    if(agnTslider.length) {
                        agnTslider.owlCarousel({
                            loop:true,
                            nav:true,
                            navText: ["<i class='flaticon-back'></i>" , "<i class='flaticon-next'></i>"],
                            dots:false,
                            autoplay:true,
                            autoplayTimeout:4000,
                            smartSpeed:1200,
                            autoplayHoverPause:true,
                            lazyLoad:true,
                            items:1
                        });
                    }
                });
            }( jQuery ));
        </script>

        <script>
            (function ( $ ) {
                "use strict";
                $(document).ready(function (){
                    var logoslider = $ (".partner-slider");
                    if(logoslider.length) {
                        logoslider.owlCarousel({
                            loop:true,
                            nav:false,
                            dots:false,
                            autoplay:true,
                            autoplayTimeout:4000,
                            autoplaySpeed:1000,
                            lazyLoad:true,
                            singleItem:true,
                            center:true,
                            responsive:{
                                0:{
                                    items:1
                                },
                                550:{
                                    items:3
                                },
                                992:{
                                    items:5
                                }
                            }
                        });
                    }
                });
            }( jQuery ));
        </script>
        <?php
    }
}