<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_three_column_features extends Widget_Base {

    public function get_name() {
        return 'rogan_three_column_features';
    }

    public function get_title() {
        return esc_html__( 'Multi Row Features', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-home-heart';
    }

    public function get_keywords() {
        return [ 'row features', 'three row' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return [ 'tilt-jquery' ];
    }

    protected function _register_controls() {

        /**
         * Row 01 Tab
         * @ Title
         * @ Content
         * @ Buttons
         */
        \Elementor\Controls_Manager::add_tab(
            'row1', esc_html__( 'Row 01', 'rogan-core' )
        );

        //**************************** Title Section ***********************//
        $this->start_controls_section(
            'row1_settings', [
                'label' => esc_html__( 'Row 01 Settings', 'rogan-core' ),
                'tab' => 'row1'
            ]
        );

        $this->add_control(
            'is_row1',
            [
                'label' => esc_html__( 'Row 01 Visibility', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'rogan-core' ),
                'label_off' => esc_html__( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
                'default' => true
            ]
        );

        $this->end_controls_section();


        //**************************** Row 01 Title Section ***********************//
        $this->start_controls_section(
            'row1_title_sec', [
                'label' => esc_html__( 'Right Column', 'rogan-core' ),
                'tab' => 'row1',
                'condition' => [
                    'is_row1' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'row1_title1', [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'row1_title1_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .text-wrapper.row1 .title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'row1_title1_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .text-wrapper.row1 .title',
            ]
        );

        $this->add_control(
            'row1_title1_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        // Title Icon
        $this->add_control(
            'row1_icon1',
            [
                'label' => esc_html__( 'Title Icon', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/icon30.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row1_icon1_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        /// Subtitle
        $this->add_control(
            'row1_subtitle', [
                'label' => esc_html__( 'Subtitle Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'row1_subtitle_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .text-wrapper.row1 .sub-heading' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'row1_subtitle_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .our-feature-app .text-wrapper.row1 .sub-heading',
            ]
        );

        $this->add_control(
            'row1_subtitle1_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        /// Contents
        $this->add_control(
            'row1_contents', [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'row1_contents_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .text-wrapper.row1 p' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'row1_content_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .our-feature-app .text-wrapper.row1 p',
            ]
        );

        $this->add_control(
            'row1_content_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        //**************************** Row 01 Button Section ***********************//
        $this->add_control(
            'row1_btn_label', [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'More About us'
            ]
        );

        $this->add_control(
            'row1_btn_url', [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $this->start_controls_tabs(
            'row1_style_tabs'
        );

        // Normal Button Style
        $this->start_controls_tab(
            'row1_style_normal_btn',
            [
                'label' => esc_html__( 'Normal', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'row1_font_color', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .single-feature-box:nth-child(1) .text-wrapper .explore-button' => 'color: {{VALUE}}',
                ],

            ]
        );

        $this->add_control(
            'row1_bg_color',
            [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .single-feature-box:nth-child(1) .text-wrapper .explore-button' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();

        /// Hover Button Style
        $this->start_controls_tab(
            'row1_style_hover_btn',
            [
                'label' => esc_html__( 'Hover', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'row1_hover_font_color', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-feature-app .single-feature-box:nth-child(1) .text-wrapper .explore-button:hover' => 'color: {{VALUE}}',
                )
            ]
        );

        $this->add_control(
            'row1_hover_bg_color', [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-feature-app .single-feature-box:nth-child(1) .text-wrapper .explore-button:hover' => 'background: {{VALUE}}',
                )
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
       $this->end_controls_section();



        //******************************** Shape Images  ***********************************//
        $this->start_controls_section(
            'row1_shape1_sec', [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab' => 'row1',
                'condition' => [
                    'is_row1' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'row1_shape1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-45.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row1_shape2',
            [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-46.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row1_shape3',
            [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-47.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row1_shape4',
            [
                'label' => esc_html__( 'Shape Four', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-48.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row1_shape5',
            [
                'label' => esc_html__( 'Shape Five', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-49.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();


        //**************************** Row 01 Box 01 ***********************//
        $this->start_controls_section(
            'row1_title2_sec', [
                'label' => esc_html__( 'Box 01', 'rogan-core' ),
                'tab' => 'row1',
                'condition' => [
                    'is_row1' => 'yes'
                ]
            ]
        );

        // Title Section
        $this->add_control(
            'row1_title2', [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Great Support.'
            ]
        );

        $this->add_control(
            'row1_title2_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .feature-img-box .feature-offer-box .title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'row1_title2_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .our-feature-app .feature-img-box .feature-offer-box .title',
            ]
        );

        $this->add_control(
            'row1_title2_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        // Title Icon Section
        $this->add_control(
            'row1_icon2',
            [
                'label' => esc_html__( 'Title Icon', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/icon31.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row1_icon2_bg_color', [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );

        $this->add_control(
            'row1_icon2_bg_color2', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .feature-img-box .support-feature .icon-box' => 'background: linear-gradient( 120deg, {{row1_icon2_bg_color.VALUE}} 0%, {{VALUE}} 100%);',
                ],
            ]
        );


        $this->add_control(
            'row1_icon2_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        // Contents Section
        $this->add_control(
            'row1_contents2', [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'row1_contents2_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .feature-img-box .feature-offer-box p' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'row1_content2_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .our-feature-app .feature-img-box .feature-offer-box p',
            ]
        );

        $this->end_controls_section(); //End Row 01 Box 01


        //**************************** Row 01 Box 02 ***********************//
        $this->start_controls_section(
            'row1_title3_sec', [
                'label' => esc_html__( 'Box 02', 'rogan-core' ),
                'tab' => 'row1',
                'condition' => [
                    'is_row1' => 'yes'
                ]
            ]
        );

        // Title Section
        $this->add_control(
            'row1_title3', [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Affordable Price'
            ]
        );

        $this->add_control(
            'row1_title3_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        // Title Icon Section
        $this->add_control(
            'row1_icon3',
            [
                'label' => esc_html__( 'Title Icon', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/icon26.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row1_icon3_bg_color', [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );

        $this->add_control(
            'row1_icon3_bg_color2', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .feature-img-box .price-feature .icon-box' => 'background: linear-gradient( 120deg, {{row1_icon3_bg_color.VALUE}} 0%, {{VALUE}} 100%);',
                ],
            ]
        );


        $this->add_control(
            'row1_icon3_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        // Contents Section
        $this->add_control(
            'row1_contents3', [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->end_controls_section();


        //**************************** Row 01 Box 03 ***********************//
        $this->start_controls_section(
            'row1_title4_sec', [
                'label' => esc_html__( 'Box 03', 'rogan-core' ),
                'tab' => 'row1',
                'condition' => [
                    'is_row1' => 'yes'
                ]
            ]
        );

        // Title Section
        $this->add_control(
            'row1_title4', [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Quick Access.'
            ]
        );

        $this->add_control(
            'row1_title4_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        // Title Icon Section
        $this->add_control(
            'row1_icon4',
            [
                'label' => esc_html__( 'Title Icon', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/icon32.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row1_icon4_bg_color', [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );

        $this->add_control(
            'row1_icon4_bg_color2', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .feature-img-box .access-feature .icon-box' => 'background: linear-gradient( 120deg, {{row1_icon4_bg_color.VALUE}} 0%, {{VALUE}} 100%);',
                ],
            ]
        );


        $this->add_control(
            'row1_icon4_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        // Contents Section
        $this->add_control(
            'row1_contents4', [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->end_controls_section();


        /**
         * Row 02 Tab
         * @Title
         * @Content
         * @Buttons
         */

        \Elementor\Controls_Manager::add_tab(
            'row2', esc_html__( 'Row 02', 'rogan-core' )
        );

        //**************************** Row 02 Settings ***********************//
        $this->start_controls_section(
            'row2_settings', [
                'label' => esc_html__( 'Row 02 Settings', 'rogan-core' ),
                'tab' => 'row2'
            ]
        );

        $this->add_control(
            'is_row2',
            [
                'label' => esc_html__( 'Row 02 Visibility', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'rogan-core' ),
                'label_off' => esc_html__( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
                'default' => true
            ]
        );

        $this->end_controls_section();


        // Left Column (Row 2)
        $this->start_controls_section(
            'row2_title1_sec', [
                'label' => esc_html__( 'Left Column', 'rogan-core' ),
                'tab' => 'row2',
                'condition' => [
                    'is_row2' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'row2_title1', [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'row2_title1_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .text-wrapper.row2 .title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'row2_title1_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .our-feature-app .text-wrapper.row2 .title',
            ]
        );

        $this->add_control(
            'row2_title1_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        // Title Icon
        $this->add_control(
            'row2_icon1',
            [
                'label' => esc_html__( 'Title Icon', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/icon33.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row2_icon1_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        /// Contents
        $this->add_control(
            'row2_contents', [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'row2_contents_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .text-wrapper.row2 p' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'row2_contents_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .our-feature-app .text-wrapper.row2 p',
            ]
        );

        $this->add_control(
            'row2_contents_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        //**************************** Row 01 Button Section ***********************//
        $this->add_control(
            'row2_btn_label', [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Get Access'
            ]
        );

        $this->add_control(
            'row2_btn_url', [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $this->start_controls_tabs(
            'row2_style_tabs'
        );

        // Normal Button Style
        $this->start_controls_tab(
            'row2_style_normal_btn',
            [
                'label' => esc_html__( 'Normal', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'row2_font_color', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .single-feature-box:nth-child(2) .text-wrapper .explore-button' => 'color: {{VALUE}}',
                ],

            ]
        );

        $this->add_control(
            'row2_bg_color',
            [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .single-feature-box:nth-child(2) .text-wrapper .explore-button' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();

        /// Hover Button Style
        $this->start_controls_tab(
            'row2_style_hover_btn',
            [
                'label' => esc_html__( 'Hover', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'row2_hover_font_color', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-feature-app .single-feature-box:nth-child(2) .text-wrapper .explore-button:hover' => 'color: {{VALUE}}',
                )
            ]
        );

        $this->add_control(
            'row2_hover_bg_color', [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-feature-app .single-feature-box:nth-child(2) .text-wrapper .explore-button:hover' => 'background: {{VALUE}}',
                )
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();
        $this->end_controls_section();



        //******************************** Shape Images  ***********************************//
        $this->start_controls_section(
            'row2_shape1_sec', [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab' => 'row2',
                'condition' => [
                    'is_row2' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'row2_shape1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-45.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row2_shape2',
            [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-46.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row2_shape3',
            [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-47.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row2_shape4',
            [
                'label' => esc_html__( 'Shape Four', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-48.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row2_shape5',
            [
                'label' => esc_html__( 'Shape Five', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-49.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();

        //******************************** Row 02 Featured Images  ***********************************//
        $this->start_controls_section(
            'row2_screen_sec', [
                'label' => esc_html__( 'Featured Images', 'rogan-core' ),
                'tab' => 'row2',
                'condition' => [
                    'is_row2' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'row2_screen1',
            [
                'label' => esc_html__( 'Featured One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/screen10.png', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row2_screen2',
            [
                'label' => esc_html__( 'Featured Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/screen11.png', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row2_screen3',
            [
                'label' => esc_html__( 'Featured Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/screen12.png', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();


        /**
         * Row 03 Tab
         * @Title
         * @Content
         * @Buttons
         */

        \Elementor\Controls_Manager::add_tab(
            'row3', esc_html__( 'Row 03', 'rogan-core' )
        );

        // Row 03 Settings
        $this->start_controls_section(
            'row3_settings', [
                'label' => esc_html__( 'Row 03 Settings', 'rogan-core' ),
                'tab' => 'row3'
            ]
        );

        $this->add_control(
            'is_row3',
            [
                'label' => esc_html__( 'Row 02 Visibility', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'rogan-core' ),
                'label_off' => esc_html__( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
                'default' => true
            ]
        );

        $this->end_controls_section();


        // Right Column (Row 03)
        $this->start_controls_section(
            'row3_title_sec', [
                'label' => esc_html__( 'Right Column', 'rogan-core' ),
                'tab' => 'row3',
                'condition' => [
                    'is_row3' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'row3_title1', [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'row3_title1_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .text-wrapper.row3 .title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'row3_title1_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .our-feature-app .text-wrapper.row3 .title',
            ]
        );

        $this->add_control(
            'row3_title1_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        // Title Icon
        $this->add_control(
            'row3_icon1',
            [
                'label' => esc_html__( 'Title Icon', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/icon34.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row3_icon1_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        /// Subtitle
        $this->add_control(
            'row3_subtitle1', [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'row3_subtitle1_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .text-wrapper.row3 .sub-heading' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'row3_subtitle_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .our-feature-app .text-wrapper .sub-heading',
            ]
        );

        $this->add_control(
            'row3_subtitle_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );
        /// Subtitle
        $this->add_control(
            'row3_contents', [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' => Controls_Manager::WYSIWYG,
            ]
        );


        $this->add_control(
            'row3_contents_hr',
            [
                'type' => Controls_Manager::DIVIDER,
            ]
        );

        //**************************** Row 01 Button Section ***********************//
        $this->add_control(
            'row3_btn_label', [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Explore More'
            ]
        );

        $this->add_control(
            'row3_btn_url', [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $this->start_controls_tabs(
            'row3_style_tabs'
        );

        // Normal Button Style
        $this->start_controls_tab(
            'row3_style_normal_btn',
            [
                'label' => esc_html__( 'Normal', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'row3_font_color', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .single-feature-box:nth-child(3) .text-wrapper .explore-button' => 'color: {{VALUE}}',
                ],

            ]
        );

        $this->add_control(
            'row3_bg_color',
            [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .single-feature-box:nth-child(3) .text-wrapper .explore-button' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();

        /// Hover Button Style
        $this->start_controls_tab(
            'row3_style_hover_btn',
            [
                'label' => esc_html__( 'Hover', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'row3_hover_font_color', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .single-feature-box:nth-child(3) .text-wrapper .explore-button:hover' => 'color: {{VALUE}}',
                ]
            ]
        );

        $this->add_control(
            'row3_hover_bg_color', [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-app .single-feature-box:nth-child(3) .text-wrapper .explore-button:hover' => 'background: {{VALUE}}',
                ]
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();
        $this->end_controls_section();



        //******************************** Shape Images  ***********************************//
        $this->start_controls_section(
            'row3_shape_sec', [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab' => 'row3',
                'condition' => [
                    'is_row3' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'row3_shape1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-45.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row3_shape2',
            [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-46.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row3_shape3',
            [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-47.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row3_shape4',
            [
                'label' => esc_html__( 'Shape Four', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-48.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row3_shape5',
            [
                'label' => esc_html__( 'Shape Five', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-49.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();

        //******************************** Row 03 Featured Images  ***********************************//
        $this->start_controls_section(
            'row3_screen_sec', [
                'label' => esc_html__( 'Featured Images', 'rogan-core' ),
                'tab' => 'row3',
                'condition' => [
                    'is_row3' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'row3_screen1',
            [
                'label' => esc_html__( 'Featured One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/screen13.png', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row3_screen2',
            [
                'label' => esc_html__( 'Featured Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/screen14.png', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row3_screen3',
            [
                'label' => esc_html__( 'Featured Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/screen15.png', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        ?>
        <div class="our-feature-app" id="feature">
            <div class="container">
                <?php
                if ( $settings['is_row1'] == 'yes' ) :
                    ?>
                    <div class="row single-feature-box row1">
                        <div class="col-lg-5 order-lg-last">
                            <div class="text-wrapper row1">
                                <?php if (!empty($settings['row1_icon1']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row1_icon1']['url']) ?>" alt="<?php echo esc_attr($settings['row1_title1']); ?>" class="icon">
                                <?php endif; ?>
                                <?php if (!empty($settings['row1_title1'])) : ?>
                                    <h2 class="title"><?php echo esc_html($settings['row1_title1']) ?></h2>
                                <?php endif; ?>
                                <?php if (!empty($settings['row1_subtitle'])) : ?>
                                    <div class="sub-heading"><?php echo esc_html($settings['row1_subtitle']) ?></div>
                                <?php endif; ?>
                                <?php if (!empty($settings['row1_contents'])) : ?>
                                    <p><?php echo wp_kses_post($settings['row1_contents']) ?></p>
                                <?php endif; ?>
                                <?php if (!empty($settings['row1_btn_label'])) : ?>
                                    <a href="<?php echo esc_url($settings['row1_btn_url']['url']); ?>" class="explore-button"
                                        <?php rogan_is_external($settings['row1_btn_url']); rogan_is_nofollow($settings['row1_btn_url']) ?>>
                                        <?php echo esc_html($settings['row1_btn_label']); ?>
                                    </a>
                                <?php endif; ?>
                            </div> <!-- /.text-wrapper -->
                        </div> <!-- /.col- -->

                        <div class="col-lg-7 order-lg-first">
                            <div class="feature-img-box">
                                <?php if (!empty($settings['row1_shape1']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row1_shape1']['url']) ?>" alt="<?php echo esc_attr($settings['row1_title2']) ?>" class="shape-one">
                                <?php endif; ?>
                                <?php if (!empty($settings['row1_shape2']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row1_shape2']['url']) ?>" alt="<?php echo esc_attr($settings['row1_title2']) ?>" class="shape-two">
                                <?php endif; ?>
                                <?php if (!empty($settings['row1_shape3']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row1_shape3']['url']) ?>" alt="<?php echo esc_attr($settings['row1_title2']) ?>" class="shape-three">
                                <?php endif; ?>
                                <?php if (!empty($settings['row1_shape4']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row1_shape4']['url']) ?>" alt="<?php echo esc_attr($settings['row1_title2']) ?>" class="shape-four">
                                <?php endif; ?>
                                <?php if (!empty($settings['row1_shape5']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row1_shape5']['url']) ?>" alt="<?php echo esc_attr($settings['row1_title2']) ?>" class="shape-five">
                                <?php endif; ?>
                                <div class="row">

                                    <div class="col-lg-6">
                                        <div class="feature-offer-box support-feature js-tilt">
                                            <?php if (!empty($settings['row1_icon2']['url'])) : ?>
                                                <div class="icon-box">
                                                    <img src="<?php echo esc_url($settings['row1_icon2']['url']) ?>" alt="<?php echo esc_attr($settings['row1_title2']) ?>">
                                                </div>
                                            <?php endif; ?>
                                            <?php if (!empty($settings['row1_title2'])) : ?>
                                                <h4 class="title"><?php echo esc_html($settings['row1_title2']) ?></h4>
                                            <?php endif; ?>
                                            <?php echo !empty($settings['row1_contents2']) ? wpautop($settings['row1_contents2']) : ''; ?>
                                        </div> <!-- /.feature-offer-box -->
                                    </div> <!-- /.col- -->

                                    <div class="col-lg-6">
                                        <div class="feature-offer-box price-feature js-tilt">
                                            <?php if (!empty($settings['row1_icon3']['url'])) : ?>
                                                <div class="icon-box">
                                                    <img src="<?php echo esc_url($settings['row1_icon3']['url']) ?>" alt="<?php echo esc_attr($settings['row1_title3']) ?>">
                                                </div>
                                            <?php endif; ?>
                                            <?php if (!empty($settings['row1_title3'])) : ?>
                                                <h4 class="title"><?php echo esc_html($settings['row1_title3']) ?></h4>
                                            <?php endif; ?>
                                            <?php echo !empty($settings['row1_contents3']) ? wpautop($settings['row1_contents3']) : ''; ?>
                                        </div> <!-- /.feature-offer-box -->

                                        <div class="feature-offer-box access-feature js-tilt">
                                            <?php if (!empty($settings['row1_icon4']['url'])) : ?>
                                                <div class="icon-box">
                                                    <img src="<?php echo esc_url($settings['row1_icon4']['url']) ?>" alt="<?php echo esc_attr($settings['row1_title4']) ?>">
                                                </div>
                                            <?php endif; ?>
                                            <?php if (!empty($settings['row1_title4'])) : ?>
                                                <h4 class="title"><?php echo esc_html($settings['row1_title4']) ?></h4>
                                            <?php endif; ?>
                                            <?php echo !empty($settings['row1_contents4']) ? wpautop($settings['row1_contents4']) : ''; ?>
                                        </div> <!-- /.feature-offer-box -->
                                    </div> <!-- /.col- -->
                                </div> <!-- /.row -->
                            </div> <!-- /.feature-img-box -->
                        </div> <!-- /.col- -->
                    </div> <!-- /.row1 -->
                <?php endif; ?>

                <?php
                if ( $settings['is_row2'] == 'yes' ) :
                    ?>
                    <div class="row single-feature-box row2">
                        <div class="col-lg-5">
                            <div class="text-wrapper row2">
                                <?php if (!empty($settings['row2_icon1']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row2_icon1']['url']) ?>" alt="<?php echo esc_attr($settings['row2_title1']); ?>" class="icon">
                                <?php endif; ?>
                                <?php if (!empty($settings['row2_title1'])) : ?>
                                    <h2 class="title"><?php echo esc_html($settings['row2_title1']) ?></h2>
                                <?php endif; ?>
                                <?php if (!empty($settings['row2_contents'])) : ?>
                                    <p><?php echo wp_kses_post($settings['row2_contents']) ?></p>
                                <?php endif; ?>
                                <?php if (!empty($settings['row2_btn_label'])) : ?>
                                    <a href="<?php echo esc_url($settings['row2_btn_url']['url']); ?>" class="explore-button"
                                        <?php rogan_is_external($settings['row2_btn_url']); rogan_is_nofollow($settings['row2_btn_url']) ?>>
                                        <?php echo esc_html($settings['row2_btn_label']); ?>
                                    </a>
                                <?php endif; ?>
                            </div> <!-- /.text-wrapper -->
                        </div> <!-- /.col- -->

                        <div class="col-lg-7">
                            <div class="feature-img-box">
                                <?php if (!empty($settings['row2_shape1']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row2_shape1']['url']) ?>" alt="<?php echo esc_attr($settings['row2_title1']) ?>" class="shape-one">
                                <?php endif; ?>
                                <?php if (!empty($settings['row2_shape2']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row2_shape2']['url']) ?>" alt="<?php echo esc_attr($settings['row2_title1']) ?>" class="shape-two">
                                <?php endif; ?>
                                <?php if (!empty($settings['row2_shape3']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row2_shape3']['url']) ?>" alt="<?php echo esc_attr($settings['row2_title1']) ?>" class="shape-three">
                                <?php endif; ?>
                                <?php if (!empty($settings['row2_shape4']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row2_shape4']['url']) ?>" alt="<?php echo esc_attr($settings['row2_title1']) ?>" class="shape-four">
                                <?php endif; ?>
                                <?php if (!empty($settings['row2_shape5']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row2_shape5']['url']) ?>" alt="<?php echo esc_attr($settings['row2_title1']) ?>" class="shape-five">
                                <?php endif; ?>
                                <div class="access-screen-holder">
                                    <div class="clearfix js-tilt">
                                        <?php if (!empty($settings['row2_screen1']['url'])) : ?>
                                            <img src="<?php echo esc_url($settings['row2_screen1']['url']) ?>" alt="<?php echo esc_attr($settings['row2_title1']) ?>" class="screen" data-aos="fade-up" data-aos-duration="1200">
                                        <?php endif; ?>
                                        <?php if (!empty($settings['row2_screen2']['url'])) : ?>
                                            <img src="<?php echo esc_url($settings['row2_screen2']['url']) ?>" alt="<?php echo esc_attr($settings['row2_title1']) ?>" class="screen" data-aos="fade-up" data-aos-duration="1200" data-aos-delay="250">
                                        <?php endif; ?>
                                        <?php if (!empty($settings['row2_screen3']['url'])) : ?>
                                            <img src="<?php echo esc_url($settings['row2_screen3']['url']) ?>" alt="<?php echo esc_attr($settings['row2_title1']) ?>" class="screen" data-aos="fade-up" data-aos-duration="1200" data-aos-delay="400">
                                        <?php endif; ?>
                                    </div>
                                </div> <!-- /.access-screen-holder -->
                            </div> <!-- /.feature-img-box -->
                        </div> <!-- /.col- -->
                    </div> <!-- /.row2 -->
                <?php endif; ?>

                <?php
                if ( $settings['is_row3'] == 'yes' ) :
                    ?>
                    <div class="row single-feature-box">
                        <div class="col-lg-5 order-lg-last">
                            <div class="text-wrapper row3">
                                <?php if (!empty($settings['row3_icon1']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row3_icon1']['url']) ?>" alt="<?php echo esc_attr($settings['row3_title1']); ?>" class="icon">
                                <?php endif; ?>
                                <?php if (!empty($settings['row3_title1'])) : ?>
                                    <h2 class="title"><?php echo esc_html($settings['row3_title1']) ?></h2>
                                <?php endif; ?>
                                <?php if (!empty($settings['row3_subtitle1'])) : ?>
                                    <div class="sub-heading"><?php echo esc_html($settings['row3_subtitle1']) ?></div>
                                <?php endif; ?>
                                <?php if (!empty($settings['row3_contents'])) : ?>
                                    <?php echo wp_kses_post($settings['row3_contents']) ?>
                                <?php endif; ?>
                                <?php if (!empty($settings['row3_btn_label'])) : ?>
                                    <a href="<?php echo esc_url($settings['row3_btn_url']['url']); ?>" class="explore-button"
                                        <?php rogan_is_external($settings['row3_btn_url']); rogan_is_nofollow($settings['row3_btn_url']) ?>>
                                        <?php echo esc_html($settings['row3_btn_label']); ?>
                                    </a>
                                <?php endif; ?>
                            </div> <!-- /.text-wrapper -->
                        </div> <!-- /.col- -->

                        <div class="col-lg-7 order-lg-first">
                            <div class="feature-img-box">
                                <?php if (!empty($settings['row3_shape1']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row3_shape1']['url']) ?>" alt="<?php echo esc_attr($settings['row3_title1']) ?>" class="shape-one">
                                <?php endif; ?>
                                <?php if (!empty($settings['row3_shape2']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row3_shape2']['url']) ?>" alt="<?php echo esc_attr($settings['row3_title1']) ?>" class="shape-two">
                                <?php endif; ?>
                                <?php if (!empty($settings['row3_shape3']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row3_shape3']['url']) ?>" alt="<?php echo esc_attr($settings['row3_title1']) ?>" class="shape-three">
                                <?php endif; ?>
                                <?php if (!empty($settings['row3_shape4']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row3_shape4']['url']) ?>" alt="<?php echo esc_attr($settings['row3_title1']) ?>" class="shape-four">
                                <?php endif; ?>
                                <?php if (!empty($settings['row3_shape5']['url'])) : ?>
                                    <img src="<?php echo esc_url($settings['row3_shape5']['url']) ?>" alt="<?php echo esc_attr($settings['row3_title1']) ?>" class="shape-five">
                                <?php endif; ?>
                                <div class="screen-mockup js-tilt">
                                    <?php if (!empty($settings['row3_screen1']['url'])) : ?>
                                        <img src="<?php echo esc_url($settings['row3_screen1']['url']) ?>" alt="<?php echo esc_attr($settings['row3_title1']) ?>" class="mockup" data-aos="fade-up" data-aos-duration="1200">
                                    <?php endif; ?>
                                    <?php if (!empty($settings['row3_screen2']['url'])) : ?>
                                        <img src="<?php echo esc_url($settings['row3_screen2']['url']) ?>" alt="<?php echo esc_attr($settings['row3_title1']) ?>" class="screen-one" data-aos="fade-left" data-aos-duration="1200" data-aos-delay="150">
                                    <?php endif; ?>
                                    <?php if (!empty($settings['row3_screen3']['url'])) : ?>
                                        <img src="<?php echo esc_url($settings['row3_screen3']['url']) ?>" alt="<?php echo esc_attr($settings['row3_title1']) ?>" class="screen-two" data-aos="fade-right" data-aos-duration="1200" data-aos-delay="250">
                                    <?php endif; ?>
                                </div> <!-- /.screen-mockup -->
                            </div> <!-- /.feature-img-box -->
                        </div> <!-- /.col- -->
                    </div> <!-- /.row3 -->
                <?php endif; ?>

            </div> <!-- /.container -->
        </div>
        <?php
    }
}