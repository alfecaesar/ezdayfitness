<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_our_goal extends Widget_Base {

    public function get_name() {
        return 'Rogan_our_goal';
    }

    public function get_title() {
        return esc_html__( 'Our Goal', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-device-desktop';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ----------------------------------------  Hero Title ------------------------------
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Title Section', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'upper_title',
            [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Marketing Gaol'
            ]
        );

        $this->add_control(
            'main_title',
            [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Best Market Research Tool for You.'
            ]
        );

        $this->end_controls_section();

        // ----------------------------------------  Contents  -----------------------------------------
        $this->start_controls_section(
            'subtitle_sec',
            [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'subtitle',
            [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->end_controls_section();

        // ********************************* Pricing Repeater Table tabs ***********************************//
        $this->start_controls_section(
            'content_list_sec',
            [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'content_list',
            [
                'label' => esc_html__( 'Item', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'conten_item_color',
            [
                'label' => esc_html__( 'Icon Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-our-goal .text-wrapper .list-item li:before' => 'color: {{VALUE}}',
                ],
            ]
        );

        $repeater->end_controls_tab();

        $this->add_control(
            'contents_list',
            [
                'label' => esc_html__( 'Pricing Tables', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{content_list}}}'
            ]
        );

        $this->end_controls_section();


        //*****************************  First Featured image *******************************************//
        $this->start_controls_section(
            'featured_sec',
            [
                'label' => esc_html__( 'Featured Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'featured_img',
            [
                'label' => esc_html__( 'Featured Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $this->add_control(
            'position',
            [
                'label' => esc_html__( 'Position', 'rogan-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .seo-our-goal .right-shape' => 'top: {{TOP}}{{UNIT}}; right: {{RIGHT}}{{UNIT}}; bottom: {{BOTTOM}}{{UNIT}}; left: {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'isLinked' => false
                ]
            ]
        );

        $this->end_controls_section();

        /// --------------------------------------- Buttons One ----------------------------------///
        $this->start_controls_section(
            'buttons',
            [
                'label' => esc_html__( 'Button', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'btn_label', [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Request Free Consultation'
            ]
        );

        $this->add_control(
            'btn_url',
            [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        //---------------------------- Normal and Hover ---------------------------//
        $this->start_controls_tabs(
            'style_tabs'
        );

        // Normal Color
        $this->start_controls_tab(
            'normal_btn_style',
            [
                'label' => __( 'Normal', 'saasland-core' ),
            ]
        );

        $this->add_control(
            'normal_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .request-button' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'normal_text_typo',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .request-button',
            ]
        );

        $this->end_controls_tab();


        // Hover Color
        $this->start_controls_tab(
            'hover_btn_style',
            [
                'label' => __( 'Hover', 'saasland-core' ),
            ]
        );

        $this->add_control(
            'hover_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .request-button:hover' => 'color: {{VALUE}}',
                ]
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        /**
         * Style Tab
         * Font Color
         * Background Color
         * ------------------------------ Style Title ------------------------------
         */
        $this->start_controls_section(
            'style_upper_title_sec', [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'upper_title_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .upper-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'upper_title_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} .theme-title-one .upper-title',
            ]
        );

        $this->end_controls_section();

        //-------------------------------------- Main Title
        $this->start_controls_section(
            'style_main_title_sec', [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'main_title_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'main_title_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();

        //--------------------------------------  Subtitle
        $this->start_controls_section(
            'style_subtitle_sec', [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'style_subtitle_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .text-wrapper p' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'style_subtitle_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .text-wrapper p',
            ]
        );

        $this->end_controls_section();

        //--------------------------------------  Subtitle
        $this->start_controls_section(
            'style_item_color_sec', [
                'label' => esc_html__( 'Item Text', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'style_item_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .text-wrapper .list-item li' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'style_item_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .text-wrapper .list-item li',
            ]
        );

        $this->end_controls_section();

        //-------------------------------------- Section Background
        $this->start_controls_section(
            'section_bg_sec', [
                'label' => esc_html__( 'Section Background', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'sec_bg_color',
            [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-our-goal' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'sec_padding',
            [
                'label' => __( 'Section Padding', 'plugin-domain' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .seo-our-goal' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'bg_img',
            [
                'label' => esc_html__( 'Background Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {

        $settings = $this->get_settings();
        $contents = $settings['contents_list'];
        $bg_img = !empty( $settings['bg_img'] ) ? $settings['bg_img'] : '';
        ?>
        <div class="seo-our-goal" style="background: url(<?php echo esc_url($settings['bg_img']['url']) ?>)no-repeat center;
                background-size: cover;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="text-wrapper">
                            <div class="theme-title-one title-underline">
                                <?php if (!empty($settings['upper_title'])) : ?>
                                    <div class="upper-title"><?php echo esc_html($settings['upper_title']) ?></div>
                                <?php endif;
                                if (!empty($settings['main_title'])) : ?>
                                    <h2 class="main-title"><?php echo esc_html($settings['main_title']) ?></h2>
                                <?php endif; ?>
                            </div> <!-- /.theme-title-one -->
                            <?php if (!empty($settings['subtitle'])) : ?>
                                <?php echo wp_kses_post(wpautop($settings['subtitle'])) ?>
                            <?php endif; ?>
                            <ul class="list-item">
                                <?php
                                if (!empty($contents)) {
                                foreach ($contents as $content) {
                                    if (!empty($content['content_list'])) : ?>
                                        <li><?php echo esc_html($content['content_list']) ?></li>
                                    <?php
                                    endif;
                                }}
                                ?>
                            </ul>
                            <?php if (!empty($settings['btn_label'])) : ?>
                                <a href="<?php echo esc_url($settings['btn_url']['url']) ?>" class="request-button"
                                    <?php rogan_is_external($settings['btn_url']); rogan_is_nofollow($settings['btn_url']) ?>>
                                    <?php echo esc_html($settings['btn_label']) ?>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="right-shape" style="background: url(<?php echo esc_url($settings['featured_img']['url']) ?>)" data-aos="fade-left"></div>
            </div> <!-- /.container -->
        </div>
        <?php
    }
}
