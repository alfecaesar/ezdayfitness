<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_hero_saas extends Widget_Base {

    public function get_name() {
        return 'rogan_hero_saas';
    }

    public function get_title() {
        return esc_html__( 'Hero Sass', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-device-desktop';
    }

    public function get_script_depends() {
        return [ 'fancybox' ];
    }

    public function get_style_depends() {
        return [ 'fancybox' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ----------------------------------------  Slogan Text ------------------------------
        $this->start_controls_section(
            'slogan_text_sec',
            [
                'label' => esc_html__( 'Slogan', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'slogan_text',
            [
                'label' => esc_html__( 'Slogan Text', 'rogan-core' ),
                'description' => esc_html__( 'Wrap up the colored text with span tag (<span>text here</span>)', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => '<span>70% Off</span> for first 3 months',
            ]
        );

        $this->end_controls_section();


        // ----------------------------------------  Hero content ------------------------------
        $this->start_controls_section(
            'hero_content',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => esc_html__( 'Title text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'Ultimate web app <br> for your customer <br> support.',
            ]
        );

        $this->add_control(
            'subtitle',
            [
                'label' => esc_html__( 'Subtitle text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->end_controls_section(); // End Hero content


        // -------------------------------------------------- Featured image 1 ------------------------------
        $this->start_controls_section(
            'fimage1_sec', [
                'label' => esc_html__( 'Featured Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'fimage1', [
                'label' => esc_html__( 'Upload the featured image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/1.png', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'fimg1_horizontal',
            [
                'label' => __( 'Horizontal', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-four .screen-one' => 'left: {{SIZE}}{{UNIT}}; right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'fimg1_vertical',
            [
                'label' => __( 'Vertical', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-four .screen-one' => 'top: {{SIZE}}{{UNIT}}; bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        /// ************************ Second Featured image ************************************//
        $this->start_controls_section(
            'fimage2_sec', [
                'label' => esc_html__( 'Second Featured image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'fimage2', [
                'label' => esc_html__( 'Upload the featured image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/2.png', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'fimg2_horizontal',
            [
                'label' => __( 'Horizontal', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-four .screen-two' => 'left: {{SIZE}}{{UNIT}}; right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'fimg2_vertical',
            [
                'label' => __( 'Vertical', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-four .screen-two' => 'top: {{SIZE}}{{UNIT}}; bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();

        /// ---------------------------------------   Buttons ----------------------------------///
        $this->start_controls_section(
            'buttons_sec',
            [
                'label' => esc_html__( 'Buttons', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'btn_title', [
                'label' => esc_html__( 'Button Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'More About us'
            ]
        );

        $repeater->add_control(
            'btn_url', [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $repeater->start_controls_tabs(
            'btn_style_tabs'
        );

        //----------------- Normal-----------------------//
        $repeater->start_controls_tab(
            'style_normal_tab',
            [
                'label' => __( 'Normal', 'rogan-core' ),
            ]
        );

        $repeater->add_control(
            'btn_text_color', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-four .main-wrapper .button-group .more-button' => 'color: {{VALUE}};',
                ],
            ]
        );

        $repeater->add_control(
            'btn_bg_color', [
                'label' => esc_html__( 'Background Color 01', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );

        $repeater->add_control(
            'btn_bg_color2', [
                'label' => esc_html__( 'Background Color 02', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-four .main-wrapper .button-group .more-button' => 'background: linear-gradient( -135deg, {{btn_bg_color.VALUE}} 0%, {{VALUE}} 100%);',
                ],
            ]
        );

        $repeater->end_controls_tab();

        // ------------------ Hover ------------------------//
        $repeater->start_controls_tab(
            'style_hover_tab',
            [
                'label' => __( 'Hover', 'rogan-core' ),
            ]
        );

        $repeater->add_control(
            'btn_text_color_hover', [
                'label' => esc_html__( 'Text Color Hover', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-four .main-wrapper .button-group .more-button:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $repeater->add_control(
            'btn_bg_color_hover', [
                'label' => esc_html__( 'Background Color 01', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );

        $repeater->add_control(
            'btn_bg_color2_hover', [
                'label' => esc_html__( 'Background Color 02', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-four .main-wrapper .button-group .more-button:hover' => 'background: linear-gradient( -135deg, {{btn_bg_color_hover.VALUE}} 0%, {{VALUE}} 100%);',
                ],
            ]
        );

        $repeater->end_controls_tab();

        $repeater->end_controls_tabs();


        // Buttons repeater field
        $this->add_control(
            'buttons', [
                'label' => esc_html__( 'Create buttons', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ btn_title }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        // Play button
        $this->add_control(
            'is_play_btn',
            [
                'label' => esc_html__( 'Play Button', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Yes', 'rogan-core' ),
                'label_off' => esc_html__( 'No', 'rogan-core' ),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'play_btn_title', [
                'label' => esc_html__( 'Play Button Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'Watch Video',
                'condition' => [
                    'is_play_btn' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'play_url', [
                'label' => esc_html__( 'Video URL', 'rogan-core' ),
                'description' => esc_html__( 'Enter here a YouTube or other video URL', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'https://www.youtube.com/embed/aXFSJTjVjw0',
                'condition' => [
                    'is_play_btn' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'play_btn_color', [
                'label' => esc_html__( 'Button Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .video-button-one' => 'color: {{VALUE}};',
                    '{{WRAPPER}} .video-button-one i' => 'color: {{VALUE}};',
                ],
                'condition' => [
                    'is_play_btn' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'play_btn_hover_color', [
                'label' => esc_html__( 'Hover Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .main-wrapper a.icon_class:hover' => 'color: {{VALUE}} !important;',
                    '{{WRAPPER}} .main-wrapper a.icon_class:hover i' => 'color: {{VALUE}} !important;',
                ],
                'condition' => [
                    'is_play_btn' => 'yes'
                ]
            ]
        );

        $this->end_controls_section(); // End Buttons



        /**
         * Style Tab
         * ------------------------------ Style Title ------------------------------
         */
        $this->start_controls_section(
            'style_title', [
                'label' => esc_html__( 'Style Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_title', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-four .main-wrapper .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} #theme-banner-four .main-wrapper .main-title',
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(), [
                'name' => 'text_shadow_prefix',
                'selector' => '
                    {{WRAPPER}} #theme-banner-four .main-wrapper .main-title',
            ]
        );

        $this->end_controls_section();



        //------------------------------ Style Subtitle ------------------------------
        $this->start_controls_section(
            'style_subtitle', [
                'label' => esc_html__( 'Style Subtitle', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_subtitle', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-four .main-wrapper .sub-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_subtitle',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} #theme-banner-four .main-wrapper .sub-title',
            ]
        );

        $this->end_controls_section();



        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'bg_shapes', [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'shape1', [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-8.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape2', [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-9.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape3', [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-10.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape4', [
                'label' => esc_html__( 'Shape Four', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-11.svg', __FILE__)
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {

        $settings = $this->get_settings();
        $buttons = $settings['buttons'];
        ?>
        <div id="theme-banner-four">
            <?php if(!empty($settings['fimage1']['url'])) : ?>
                <img src="<?php echo esc_url($settings['fimage1']['url']) ?>" alt="" class="screen-one wow fadeInRight animated" data-wow-duration="2s">
            <?php endif; ?>
            <?php if(!empty($settings['fimage1']['url'])) : ?>
                <img src="<?php echo esc_url($settings['fimage2']['url']) ?>" alt="" class="screen-two wow fadeInUp animated" data-wow-duration="2s">
            <?php endif; ?>
            <?php if(!empty($settings['shape1']['url'])) : ?>
                <img src="<?php echo $settings['shape1']['url'] ?>" alt="<?php echo esc_attr($settings['title']) ?>" class="shape-one">
            <?php endif; ?>
            <?php if(!empty($settings['shape2']['url'])) : ?>
                <img src="<?php echo $settings['shape2']['url'] ?>" alt="<?php echo esc_attr($settings['title']) ?>" class="shape-two">
            <?php endif; ?>
            <?php if(!empty($settings['shape3']['url'])) : ?>
                <img src="<?php echo $settings['shape3']['url'] ?>" alt="<?php echo esc_attr($settings['title']) ?>" class="shape-three">
            <?php endif; ?>
            <?php if(!empty($settings['shape4']['url'])) : ?>
                <img src="<?php echo $settings['shape4']['url'] ?>" alt="<?php echo esc_attr($settings['title']) ?>" class="shape-four">
            <?php endif; ?>
            <div class="round-shape-one"></div>
            <div class="round-shape-two"></div>
            <div class="round-shape-three"></div>
            <div class="round-shape-four"></div>
            <div class="container">
                <div class="main-wrapper">
                    <?php if(!empty($settings['slogan_text'])) : ?>
                        <div class="slogan wow fadeInDown animated" data-wow-delay="0.2s"> <?php echo wp_kses_post($settings['slogan_text']) ?> </div>
                    <?php endif; ?>
                    <?php if(!empty($settings['title'])) : ?>
                        <h1 class="main-title wow fadeInUp animated" data-wow-delay="0.4s"> <?php echo wp_kses_post(nl2br($settings['title'])); ?> </h1>
                    <?php endif; ?>
                    <?php if(!empty($settings['subtitle'])) : ?>
                        <p class="sub-title wow fadeInUp animated" data-wow-delay="0.9s"> <?php echo wp_kses_post($settings['subtitle']); ?> </p>
                    <?php endif; ?>
                    <ul class="button-group lightgallery">
                        <?php
                        $i = 0;
                        foreach ($buttons as $button) {
                            ++$i;
                            echo "<li> <a " . rogan_is_nofollow($button['btn_url']) . rogan_is_external($button['btn_url']) .
                                "href='{$button['btn_url']['url']}' " .
                                "data-wow-delay='1.5s'" .
                                "class='more-button wow fadeInLeft animated elementor-repeater-item-{$button['_id']}'> " .
                                "{$button['btn_title']} " .
                                "<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>".
                                "</a> </li>";
                        }
                        if( $settings['is_play_btn'] == 'yes' ) :
                            wp_enqueue_script( 'fancybox' );
                            wp_enqueue_style( 'fancybox' );
                            if ( !empty( $settings['play_btn_title'] ) ) : ?>
                                <li>
                                    <a href="<?php echo esc_url($settings['play_url']['url']) ?>" class="icon_class fancybox video-button video-button-one wow fadeInRight animated" data-wow-delay="1.5s">
                                        <i class="flaticon-play-button"></i> <?php echo esc_html($settings['play_btn_title']) ?>
                                    </a>
                                </li>
                            <?php endif;
                        endif; ?>
                    </ul>
                </div> <!-- /.main-wrapper -->
            </div> <!-- /.container -->
        </div>
        <?php
    }
}
