<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;
use WP_Query;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_portfolio_dark extends Widget_Base {

    public function get_name() {
        return 'rogan_portfolio_dark';
    }

    public function get_title() {
        return esc_html__( 'Filterable Portfolio (Dark)', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-gallery-grid';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_keywords() {
        return [ 'projects', 'portfolio', 'isotop', 'filterable portfolio' ];
    }

    public function get_style_depends() {
        return [ 'fancybox' ];
    }

    public function get_script_depends() {
        return [ 'isotope', 'fancybox' ];
    }

    protected function _register_controls() {

        //***********************************  Section Title ******************************************//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Projects'
            ]
        );

        $this->add_control(
            'title_append',
            [
                'label' => esc_html__( 'Title Append', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => '.'
            ]
        );

        $this->add_control(
            'subtitle',
            [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
            ]
        );

        $this->end_controls_section();


        //************************* Page Number ****************************//
        $this->start_controls_section(
            'number_sec',
            [
                'label' => esc_html__( 'Section Number', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'number1',
            [
                'label' => esc_html__( 'Number One', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
            ]
        );

        $this->add_control(
            'number2',
            [
                'label' => esc_html__( 'Number Two', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
            ]
        );

        $this->end_controls_section();


        //************************* Button Settings ***************************//
        $this->start_controls_section(
            'btn_sec',
            [
                'label' => esc_html__( 'Button Settings', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'btn_title',
            [
                'label' => esc_html__( 'Button Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'View Gallery'
            ]
        );

        $this->add_control(
            'btn_url',
            [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
            ]
        );
        
        $this->start_controls_tabs(
            'style_tabs'
        );
        
        /// Normal Button Style
        $this->start_controls_tab(
            'style_normal_btn',
            [
                'label' => __( 'Normal', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'font_color', [
                'label' => __( 'Font Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-project-portfo .text-content .gallery-button' => 'color: {{VALUE}}',
                )
            ]
        );

        $this->add_control(
            'bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-project-portfo .text-content .gallery-button' => 'background-color: {{VALUE}}; border-color: {{VALUE}}',
                )
            ]
        );

        $this->add_control(
            'border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-project-portfo .text-content .gallery-button' => 'border-color: {{VALUE}}',
                )
            ]
        );

        $this->end_controls_tab();
        
        $this->start_controls_tab(
            'style_hover_btn',
            [
                'label' => __( 'Hover', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'hover_font_color', [
                'label' => __( 'Font Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-project-portfo .text-content .gallery-button:hover' => 'color: {{VALUE}}',
                )
            ]
        );

        $this->add_control(
            'hover_bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-project-portfo .text-content .gallery-button:hover' => 'background: {{VALUE}}',
                )
            ]
        );

        $this->add_control(
            'hover_border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-project-portfo .text-content .gallery-button:hover' => 'border-color: {{VALUE}}',
                )
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_section();


        /**
         * Style Tabs
         */
        /********************************* Section Title ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_sec_title',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-two' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_sce_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .theme-title-two',
            ]
        );

        $this->add_control(
            'title_first_letter_style_hr',
            [
                'type' => \Elementor\Controls_Manager::DIVIDER,
            ]
        );

        $this->add_control(
            'color_sec_title_first_letter',
            [
                'label' => esc_html__( 'First Letter Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-two:first-letter' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(), [
                'name' => 'title_first_letter_shadow',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .theme-title-two:first-letter',
            ]
        );

        $this->add_control(
            'title_style_hr',
            [
                'type' => \Elementor\Controls_Manager::DIVIDER,
            ]
        );


        $this->add_control(
            'color_sec_title_append',
            [
                'label' => esc_html__( 'Title Append Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-two span' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();


        /********************************* Page Number ********************************/
        $this->start_controls_section(
            'style_page_number', [
                'label' => esc_html__( 'Number Style', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_page_number',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .section-portfo .section-num' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_page_number',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .section-portfo .section-num',
            ]
        );

        $this->end_controls_section();


        // ---------------------------------- Filter Options ------------------------
        $this->start_controls_section(
            'filter', [
                'label' => esc_html__( 'Filter Options', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'all_label', [
                'label' => esc_html__( 'All filter label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'All'
            ]
        );

        $this->add_control(
            'show_count', [
                'label' => esc_html__( 'Show Posts Count', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 8
            ]
        );

        $this->add_control(
            'except_limit', [
                'label' => esc_html__( 'Excerpt Limit', 'rogan-core' ),
                'description' => esc_html__( 'Limit the excerpt count in words', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 15
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__( 'Order', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->add_control(
            'exclude', [
                'label' => esc_html__( 'Exclude Portfolios', 'saasland-core' ),
                'description' => __( 'Enter the portfolio post ID to hide. Input the multiple ID with comma separated. <a href="https://goo.gl/nb2zwe">Find post ID</a>', 'saasland-core' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->end_controls_section();


    }

    protected function render() {
        $settings = $this->get_settings();
        $title_append = !empty($settings['title_append']) ? '<span>'.$settings['title_append'].'</span>' : '';
        $portfolios = new WP_Query(array(
            'post_type'     => 'portfolio',
            'posts_per_page'=> $settings['show_count'],
            'order' => $settings['order'],
            'post__not_in' => !empty($settings['exclude']) ? explode(',', $settings['exclude']) : ''
        ));

        $portfolio_cats = get_terms(array(
            'taxonomy' => 'portfolio_cat',
            'hide_empty' => true
        ));
        ?>

        <div class="section-portfo our-project-portfo">
            <div class="section-num hide-pr">
                <?php if(isset($settings['number1'])) : ?>
                    <span> <?php echo esc_html($settings['number1']) ?> </span>
                <?php endif; ?>
                <?php if(isset($settings['number2'])) : ?>
                    <span> <?php echo esc_html($settings['number2']) ?> </span>
                <?php endif; ?>
            </div>
            <div class="container">
                <?php if(!empty($settings['title'])) : ?>
                    <h2 class="theme-title-two">
                        <?php
                        echo wp_kses_post($settings['title']);
                        echo wp_kses_post($title_append);
                        ?>
                    </h2>
                <?php endif; ?>
                <ul class="isotop-menu-wrapper hide-pr">
                    <?php if(!empty($settings['all_label'])) : ?>
                        <li class="is-checked" data-filter="*">
                            <?php echo esc_html($settings['all_label']); ?>
                        </li>
                    <?php endif; ?>
                    <?php
                    if(is_array($portfolio_cats)) {
                        foreach ($portfolio_cats as $portfolio_cat) {
                            ?>
                            <li data-filter=".<?php echo $portfolio_cat->slug ?>">
                                <?php echo $portfolio_cat->name ?>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
            <div class="text-content">
                <div class="container">
                    <?php echo (!empty($settings['subtitle'])) ? wpautop($settings['subtitle']) : ''; ?>
                </div>
                <?php if( !empty($settings['btn_title']) ) : ?>
                    <a href="<?php echo esc_url($settings['btn_url']['url']) ?>" <?php rogan_is_external($settings['btn_url']); rogan_is_nofollow($settings['btn_url']) ?> class="gallery-button" data-aos="fade-left" data-aos-duration="2000">
                        <?php echo esc_html($settings['btn_title']) ?>
                    </a>
                <?php endif; ?>
            </div>
            <div id="isotop-gallery-wrapper">
                <div class="grid-sizer"></div>
                <?php
                while ($portfolios->have_posts()) : $portfolios->the_post();
                    $cats = get_the_terms(get_the_ID(), 'portfolio_cat');
                    $cat_slug = '';
                    if(is_array($cats)) {
                        foreach ($cats as $cat) {
                            $cat_slug .= $cat->slug.' ';
                        }
                    }
                    ?>
                    <div class="isotop-item <?php echo esc_attr($cat_slug) ?>">
                        <div class="effect-zoe">
                            <?php the_post_thumbnail('rogan_380x420') ?>
                            <div class="inner-caption">
                                <h2><a href="<?php the_permalink() ?>"> <?php the_title(); ?> </a></h2>
                                <p class="icon-links">
                                    <a href="<?php the_post_thumbnail_url() ?>" class="icon zoom fancybox" data-fancybox="images"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                </p>
                                <p class="description">
                                    <?php echo wp_trim_words( get_the_excerpt(), $settings['except_limit'], '' ) ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </div>
        </div>

        <script>
            (function ( $ ) {
                "use strict";
                $(window).load(function () {
                    var $grid = $('#isotop-gallery-wrapper').isotope({
                        // options
                        itemSelector: '.isotop-item',
                        percentPosition: true,
                        masonry: {
                            // use element for option
                            columnWidth: '.grid-sizer'
                        }

                    });

                    // filter items on button click
                    $('.isotop-menu-wrapper').on('click', 'li', function () {
                        var filterValue = $(this).attr('data-filter');
                        $grid.isotope({filter: filterValue});
                    });

                    // change is-checked class on buttons
                    $('.isotop-menu-wrapper').each(function (i, buttonGroup) {
                        var $buttonGroup = $(buttonGroup);
                        $buttonGroup.on('click', 'li', function () {
                            $buttonGroup.find('.is-checked').removeClass('is-checked');
                            $(this).addClass('is-checked');
                        });
                    });
                });
            }( jQuery ));
        </script>
        <?php
    }
}