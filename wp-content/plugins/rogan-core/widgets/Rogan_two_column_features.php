<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_two_column_features extends Widget_Base {

    public function get_name() {
        return 'rogan_two_column_features';
    }

    public function get_title() {
        return esc_html__( 'Two Column Features', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-home-heart';
    }

    public function get_keywords() {
        return [ 'row features', 'two row' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return ['aos-next'];
    }

    protected function _register_controls() {

        /**
         * Row 01 Tab
         * @ Title
         * @ Content
         * @ Buttons
         */
        \Elementor\Controls_Manager::add_tab(
            'row1', esc_html__( 'Row 01', 'rogan-core' )
        );

        //**************************** Title Section ***********************//
        $this->start_controls_section(
            'row1_settings', [
                'label' => esc_html__( 'Row 01 Settings', 'rogan-core' ),
                'tab' => 'row1'
            ]
        );

        $this->add_control(
            'is_row1',
            [
                'label' => esc_html__( 'Row 01 Visibility', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'rogan-core' ),
                'label_off' => esc_html__( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
                'default' => true
            ]
        );

        $this->end_controls_section();

        //**************************** Navigate to button ***********************//
        $this->start_controls_section(
            'navigate_btn', [
                'label' => esc_html__( 'Navigate Button', 'rogan-core' ),
                'tab' => 'row1',
                'condition' => [
                    'is_row1' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'is_nav_btn',
            [
                'label' => esc_html__( 'Navigate Button Visibility', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'rogan-core' ),
                'label_off' => esc_html__( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
                'default' => true
            ]
        );

        $this->add_control(
            'nav_btn_id', [
                'label' => esc_html__( 'Target Section ID', 'rogan-core' ),
                'description' => esc_html__( 'Input here the target section CSS ID where you want to navigate by clicking on the button.', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'condition' => [
                    'is_nav_btn' => 'yes',
                ]
            ]
        );

        $this->add_control(
            'nav_btn_icon', [
                'label' => esc_html__( 'Button Icon', 'rogan-core' ),
                'type' => Controls_Manager::ICON,
                'condition' => [
                    'is_nav_btn' => 'yes',
                ]
            ]
        );

        $this->end_controls_section();

        //**************************** Title Section ***********************//
        $this->start_controls_section(
            'row1', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'tab' => 'row1',
                'condition' => [
                    'is_row1' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'title', [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => esc_html__( 'Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .row1 .theme-title-one .main-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .row1 .theme-title-one .main-title',
            ]
        );

        $this->add_control(
            'line_show',
            [
                'label' => esc_html__( 'Title line show', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'rogan-core' ),
                'label_off' => esc_html__( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'title_line_color',
            [
                'label' => esc_html__( 'Line Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .row1 .text-box .theme-title-one .main-title .line:before' => 'background: {{VALUE}}',
                ],
                'condition' => [
                    'line_show' => 'yes'
                ]
            ]
        );

        $this->end_controls_section();

        // ********************************** Icon Box *************************************//
        $this->start_controls_section(
            'icon_box',
            [
                'label' => esc_html__( 'Icon Box', 'rogan-core' ),
                'tab' => 'row1',
                'condition' => [
                    'is_row1' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'icon_img',
            [
                'label' => esc_html__( 'Icon Image', 'rogan-core' ),
                'description' => esc_html__( 'Recommended to upload a PNG image.', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/icon24.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'bg_shape',
            [
                'label' => esc_html__( 'Icon Background', 'rogan-core' ),
                'description' => esc_html__( 'Recommended to use SVG to PNG image as the background shape.', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/bg-shape2.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();


        // ------------------------------ Contents ------------------------------
        $this->start_controls_section(
            'content_sec',
            [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'tab' => 'row1',
                'condition' => [
                    'is_row1' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'content', [
                'label' => esc_html__( 'Content', 'rogan-core' ),
                'type' => Controls_Manager::WYSIWYG,
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'btn_title', [
                'label' => esc_html__( 'Button Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Get Started'
            ]
        );

        $repeater->add_control(
            'btn_url', [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $repeater->start_controls_tabs(
            'style_tabs'
        );

        // Normal Button Style
        $repeater->start_controls_tab(
            'style_normal_btn',
            [
                'label' => esc_html__( 'Normal', 'rogan-core' ),
            ]
        );

        $repeater->add_control(
            'font_color', [
                'label' => esc_html__( 'Font Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'color: {{VALUE}}',
                )
            ]
        );

        $repeater->add_control(
            'bg_color', [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'background: {{VALUE}}',
                )
            ]
        );

        $repeater->add_control(
            'border_color', [
                'label' => esc_html__( 'Border Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'border-color: {{VALUE}}',
                )
            ]
        );

        $repeater->end_controls_tab();

        /// Hover Button Style
        $repeater->start_controls_tab(
            'style_hover_btn',
            [
                'label' => esc_html__( 'Hover', 'rogan-core' ),
            ]
        );
        $repeater->add_control(
            'hover_font_color',
            [
                'label' => esc_html__( 'Font Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'color: {{VALUE}}',
                )
            ]
        );
        $repeater->add_control(
            'hover_bg_color', [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'background: {{VALUE}}',
                )
            ]
        );
        $repeater->add_control(
            'hover_border_color', [
                'label' => esc_html__( 'Border Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'border-color: {{VALUE}}',
                )
            ]
        );
        $repeater->end_controls_tab();

        $this->add_control(
            'buttons', [
                'label' => esc_html__( 'Create Buttons', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ btn_title }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section();

        /*
         * Featured Image Column
         */
        $this->start_controls_section(
            'featured_sec',
            [
                'label' => esc_html__( 'Featured Image Column', 'rogan-core' ),
                'tab'   => 'row1',
                'condition' => [
                    'is_row1' => 'yes'
                ]
            ]
        );

        $images = new \Elementor\Repeater();

        $images->add_control(
            'alt',
            [
                'label' => esc_html__( 'Alt Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Featured Image'
            ]
        );

        $images->add_control(
            'image',
            [
                'label' => esc_html__( 'Upload the featured image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/screen1.png', __FILE__)
                ]
            ]
        );

        $images->add_control(
            'row01_horizontal',
            [
                'label' => __( 'Horizontal', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'left: {{SIZE}}{{UNIT}}; right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $images->add_control(
            'row01_vertical',
            [
                'label' => __( 'Vertical', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'top: {{SIZE}}{{UNIT}}; bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'images', [
                'label' => esc_html__( 'Images', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ alt }}}',
                'fields' => $images->get_controls(),
            ]
        );

        $this->add_control(
            'row1_shape1',
            [
                'label' => esc_html__( 'Background Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-15.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'row1_shape2',
            [
                'label' => esc_html__( 'Background Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-16.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();


        /**
         * Row 02 Tab
         * @ Title
         * @ Content
         * @ Buttons
         */

        \Elementor\Controls_Manager::add_tab(
            'row2', esc_html__( 'Row 02', 'rogan-core' )
        );

        //**************************** Title Section ***********************//
        $this->start_controls_section(
            'row2_settings', [
                'label' => esc_html__( 'Row 02 Settings', 'rogan-core' ),
                'tab' => 'row2'
            ]
        );

        $this->add_control(
            'is_row2',
            [
                'label' => esc_html__( 'Row 02 Visibility', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'rogan-core' ),
                'label_off' => esc_html__( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
                'default' => true
            ]
        );

        $this->end_controls_section();


        $this->start_controls_section(
            'row2', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'tab' => 'row2',
                'condition' => [
                    'is_row2' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'row2_title', [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'row2_title_color',
            [
                'label' => esc_html__( 'Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .row2 .theme-title-one .main-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'row2_title_typography',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .row2 .theme-title-one .main-title',
            ]
        );

        $this->add_control(
            'row2_line_show',
            [
                'label' => esc_html__( 'Title line show', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'rogan-core' ),
                'label_off' => esc_html__( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'row2_title_line_color',
            [
                'label' => esc_html__( 'Line Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .row2 .text-box .theme-title-one .main-title .line:before' => 'background: {{VALUE}}',
                ],
                'condition' => [
                    'row2_line_show' => 'yes'
                ]
            ]
        );

        $this->end_controls_section();

        //********************************** Icon Box ********************************//
        $this->start_controls_section(
            'row2_icon_box',
            [
                'label' => esc_html__( 'Icon Box', 'rogan-core' ),
                'tab' => 'row2',
                'condition' => [
                    'is_row2' => 'yes'
                ]
            ]
        );


        $this->add_control(
            'row2_icon_img',
            [
                'label' => esc_html__( 'Icon Image', 'rogan-core' ),
                'description' => esc_html__( 'Recommended to upload a png image.', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/icon25.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'row2_bg_shape',
            [
                'label' => esc_html__( 'Icon Background', 'rogan-core' ),
                'description' => esc_html__( 'Recommended to use SVG to PNG image as the background shape.', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/bg-shape3.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();




        // ------------------------------ Contents 02 ------------------------------
        $this->start_controls_section(
            'row2_content_sec',
            [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'tab' => 'row2',
                'condition' => [
                    'is_row2' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'row2_content', [
                'label' => esc_html__( 'Content', 'rogan-core' ),
                'type' => Controls_Manager::WYSIWYG,
            ]
        );

        $row2_repeater = new \Elementor\Repeater();
        $row2_repeater->add_control(
            'row2_btn_title', [
                'label' => esc_html__( 'Button Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Get Started'
            ]
        );

        $row2_repeater->add_control(
            'row2_btn_url', [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $row2_repeater->start_controls_tabs(
            'style_tabs'
        );

        /// Normal Button Style
        $row2_repeater->start_controls_tab(
            'row2_style_normal_btn',
            [
                'label' => esc_html__( 'Normal', 'rogan-core' ),
            ]
        );

        $row2_repeater->add_control(
            'row2_font_color', [
                'label' => esc_html__( 'Font Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'color: {{VALUE}}',
                )
            ]
        );

        $row2_repeater->add_control(
            'row2_bg_color', [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'background: {{VALUE}}',
                )
            ]
        );

        $row2_repeater->add_control(
            'row2_border_color', [
                'label' => esc_html__( 'Border Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'border-color: {{VALUE}}',
                )
            ]
        );

        $row2_repeater->end_controls_tab();

        /// Hover Button Style
        $row2_repeater->start_controls_tab(
            'row2_style_hover_btn',
            [
                'label' => esc_html__( 'Hover', 'rogan-core' ),
            ]
        );

        $row2_repeater->add_control(
            'row2_hover_font_color',
            [
                'label' => esc_html__( 'Font Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'color: {{VALUE}}',
                )
            ]
        );

        $row2_repeater->add_control(
            'row2_hover_bg_color',
            [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'background: {{VALUE}}',
                )
            ]
        );

        $row2_repeater->add_control(
            'row2_hover_border_color',
            [
                'label' => esc_html__( 'Border Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'border-color: {{VALUE}}',
                )
            ]
        );
        $row2_repeater->end_controls_tab();

        $this->add_control(
            'row2_buttons',
            [
                'label' => esc_html__( 'Create Buttons', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ row2_btn_title }}}',
                'fields' => $row2_repeater->get_controls(),
            ]
        );

        $this->end_controls_section();

        //***************************************** Image Box *********************************************//
        $this->start_controls_section(
            'row2_featured_sec',
            [
                'label' => esc_html__( 'Featured Image Column', 'rogan-core' ),
                'tab'   => 'row2',
                'condition' => [
                    'is_row2' => 'yes'
                ]
            ]
        );

        $row2_images = new \Elementor\Repeater();

        $row2_images->add_control(
            'row2_alt',
            [
                'label' => esc_html__( 'Alt Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Featured Image'
            ]
        );

        $row2_images->add_control(
            'row2_image',
            [
                'label' => esc_html__( 'Upload the featured image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/screen5.png', __FILE__)
                ]
            ]
        );

        $images->add_control(
            'row_02_horizontal',
            [
                'label' => __( 'Horizontal', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'left: {{SIZE}}{{UNIT}}; right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $images->add_control(
            'row_02_vertical',
            [
                'label' => __( 'Vertical', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                ],
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'top: {{SIZE}}{{UNIT}}; bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'row2_images', [
                'label' => esc_html__( 'Images', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ row2_alt }}}',
                'fields' => $row2_images->get_controls(),
            ]
        );

        $this->add_control(
            'row2_shape1',
            [
                'label' => esc_html__( 'Background Shape', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-17.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();

        /**
         * @ Style Tabs
         */
        // ------------------------------ Background Shapes ----------------------//
        $this->start_controls_section(
            'bg-shapes',
            [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'shape1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-18.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape2',
            [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-18.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape3',
            [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-20.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape4',
            [
                'label' => esc_html__( 'Shape Four', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-21.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape5',
            [
                'label' => esc_html__( 'Shape Five', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-22.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape6',
            [
                'label' => esc_html__( 'Shape Six', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-19.svg', __FILE__)
                ],
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_margin',
            [
                'label' => __( 'Margin', 'rogan-core' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'devices' => [ 'desktop', 'tablet', 'mobile' ],
                'selectors' => [
                    '{{WRAPPER}} .our-feature-sass' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'sec_padding',
            [
                'label' => __( 'Padding', 'rogan-core' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'devices' => [ 'desktop', 'tablet', 'mobile' ],
                'selectors' => [
                    '{{WRAPPER}} .our-feature-sass' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'background_color',
            [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-feature-sass' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        ?>

        <div class="our-feature-sass">
            <div class="section-shape-one"></div>
            <div class="section-shape-two">
                <?php if (!empty($settings['shape1']['url'])) : ?>
                    <img src="<?php echo esc_url($settings['shape1']['url']) ?>" alt="">
                <?php endif; ?>
            </div>
            <?php if (!empty($settings['shape2']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape2']['url']) ?>" alt="" class="section-shape-three">
            <?php endif; ?>
            <div class="section-shape-four"></div>
            <?php if (!empty($settings['shape3']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape3']['url']) ?>" alt="" class="section-shape-five">
            <?php endif; ?>
            <?php if (!empty($settings['shape4']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape4']['url']) ?>" alt="" class="section-shape-six">
            <?php endif; ?>
            <?php if (!empty($settings['shape5']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape5']['url']) ?>" alt="" class="section-shape-seven">
            <?php endif; ?>
            <?php if (!empty($settings['shape6']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape6']['url']) ?>" alt="" class="section-shape-eight">
            <?php endif; ?>
            <?php
            if ( $settings['is_nav_btn'] == 'yes' ) :
                $nav_btn_id = !empty($settings['nav_btn_id']) ? $settings['nav_btn_id'] : 'feature-sass';
                $nav_btn_icon = !empty($settings['nav_btn_icon']) ? $settings['nav_btn_icon'] : 'flaticon-back';
                ?>
                <a href="#<?php echo esc_attr($nav_btn_id) ?>" class="down-arrow scroll-target">
                    <span><i class="<?php echo esc_attr($nav_btn_icon) ?>"></i></span>
                </a>
            <?php endif; ?>
            <div class="feature-wrapper" id="feature-sass">
                <?php
                if ( $settings['is_row1'] == 'yes' ) :
                    ?>
                    <div class="single-feature-block row1">
                        <div class="container clearfix">
                            <div class="text-box">
                                <div class="theme-title-one hide-pr">
                                    <div class="icon-box hide-pr">
                                        <?php if (!empty($settings['bg_shape']['url'])) : ?>
                                            <img src="<?php echo esc_url($settings['bg_shape']['url']) ?>" alt="<?php echo esc_attr($settings['title']) ?>" class="bg-shape">
                                        <?php endif; ?>
                                        <?php if (!empty($settings['icon_img']['url'])) : ?>
                                            <img src="<?php echo esc_url($settings['icon_img']['url']) ?>" alt="<?php echo esc_attr($settings['title']) ?>" class="icon">
                                        <?php endif; ?>
                                    </div>
                                    <?php if (!empty($settings['title'])) : ?>
                                        <h2 class="main-title"> <?php echo wp_kses_post($settings['title']) ?> <b class="line"></b></h2>
                                    <?php endif; ?>
                                </div> <!-- /.theme-title-one -->
                                <?php echo wp_kses_post(wpautop($settings['content'])) ?>
                                <?php
                                if(!empty($settings['buttons'])) {
                                foreach ($settings['buttons'] as $button) {
                                    if(!empty($button['btn_title'])) {
                                        ?>
                                        <a href="<?php echo esc_url($button['btn_url']['url']) ?>"
                                           class="read-more elementor-repeater-item-<?php echo $button['_id'] ?>"
                                           <?php rogan_is_external($button['btn_url']); rogan_is_nofollow($button['btn_url']) ?>>
                                           <?php echo esc_html($button['btn_title']) ?>
                                        </a>
                                        <?php
                                    }
                                }}
                                ?>
                            </div> <!-- /.text-box -->
                        </div> <!-- /.container -->

                        <div class="img-box">
                            <?php if(!empty($settings['row1_shape1']['url'])) : ?>
                            <img src="<?php echo esc_url($settings['row1_shape1']['url']) ?>" alt="<?php echo esc_attr($settings['title']); ?>" class="main-shape" data-aos="fade-right" data-aos-delay="200">
                            <?php endif; ?>
                            <?php if(!empty($settings['row1_shape2']['url'])) : ?>
                                <img src="<?php echo esc_url($settings['row1_shape2']['url']) ?>" alt="<?php echo esc_attr($settings['title']); ?>" class="bg-shape" data-aos="fade-right" data-aos-delay="400">
                            <?php endif; ?>
                            <?php
                            if(!empty($settings['images'])) {
                                $delay = 600;
                                foreach ($settings['images'] as $i => $image) {
                                    switch ($i) {
                                        case 0:
                                            $img_class = 'screen-one';
                                            break;
                                        case 1:
                                            $img_class = 'screen-two';
                                            break;
                                        case 2:
                                            $img_class = 'screen-three';
                                            break;
                                        case 3:
                                            $img_class = 'screen-four';
                                            break;
                                        default:
                                            $img_class = 'screen-'.$i;
                                            break;
                                    }
                                    ?>
                                    <img src="<?php echo esc_url($image['image']['url']) ?>"
                                         alt="<?php echo esc_attr($image['alt']) ?>"
                                         class="<?php echo esc_attr($img_class) ?> elementor-repeater-item-<?php echo $image['_id'] ?>"
                                         data-aos="fade-down"
                                         data-aos-delay="<?php echo $delay; ?>">
                                        <?php
                                    $delay = $delay + 200;
                                }
                            }
                            ?>

                        </div>
                    </div> <!-- /.single-feature-block -->
                    <?php
                endif;
                ?>

                <?php
                if ( $settings['is_row2'] == 'yes' ) :
                    ?>
                    <div class="single-feature-block row2">
                        <div class="container clearfix">
                            <div class="text-box">
                                <div class="theme-title-one hide-pr">
                                    <div class="icon-box hide-pr">
                                        <?php if (!empty($settings['row2_bg_shape']['url'])) : ?>
                                            <img src="<?php echo esc_url($settings['row2_bg_shape']['url']) ?>" alt="<?php echo esc_attr($settings['row2_title']) ?>" class="bg-shape">
                                        <?php endif; ?>
                                        <?php if (!empty($settings['row2_icon_img']['url'])) : ?>
                                            <img src="<?php echo esc_url($settings['row2_icon_img']['url']) ?>" alt="<?php echo esc_attr($settings['row2_title']) ?>" class="icon">
                                        <?php endif; ?>
                                    </div>
                                    <?php if(!empty($settings['row2_title'])) : ?>
                                        <h2 class="main-title"> <?php echo wp_kses_post($settings['row2_title']) ?> <b class="line"></b></h2>
                                    <?php endif; ?>
                                </div> <!-- /.theme-title-one -->
                                <?php if (!empty($settings['row2_content'])) : ?>
                                    <?php echo wp_kses_post(wpautop($settings['row2_content'])) ?>
                                <?php endif; ?>
                                <?php
                                if(!empty($settings['row2_buttons'])) {
                                    foreach ($settings['row2_buttons'] as $button) {
                                        if(!empty($button['row2_btn_url'])) {
                                            ?>
                                            <a href="<?php echo esc_url($button['row2_btn_url']['url']) ?>"
                                               class="read-more elementor-repeater-item-<?php echo $button['_id'] ?>"
                                                <?php rogan_is_external($button['row2_btn_url']); rogan_is_nofollow($button['row2_btn_url']) ?>>
                                                <?php echo esc_html($button['row2_btn_title']) ?>
                                            </a>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </div> <!-- /.text-box -->
                        </div> <!-- /.container -->

                        <div class="img-box">
                            <?php if(!empty($settings['row2_shape']['url'])) : ?>
                                <img src="<?php echo esc_url($settings['row2_shape']['url']) ?>" alt="<?php echo esc_attr($settings['row2_title']) ?>" class="main-shape" data-aos="fade-left" data-aos-delay="200">
                            <?php endif; ?>
                            <?php
                            if(!empty($settings['row2_images'])) {
                                $delay = 400;
                                foreach ($settings['row2_images'] as $i => $image) {
                                    switch ($i) {
                                        case 0:
                                            $img_class = 'screen-one';
                                            break;
                                        case 1:
                                            $img_class = 'screen-two';
                                            break;
                                        default:
                                            $img_class = 'screen-'.$i;
                                            break;
                                    }
                                    ?>
                                    <img src="<?php echo esc_url($image['row2_image']['url']) ?>"
                                         alt="<?php echo esc_attr($image['row2_alt']) ?>"
                                         class="<?php echo esc_attr($img_class) ?>"
                                         data-aos="fade-down"
                                         data-aos-delay="<?php echo $delay; ?>">
                                    <?php
                                    $delay = $delay + 200;
                                }
                            }
                            ?>
                        </div>
                    </div> <!-- /.single-feature-block -->
                    <?php
                endif;
                ?>

            </div> <!-- /.feature-wrapper -->
        </div>
        <?php
    }
}