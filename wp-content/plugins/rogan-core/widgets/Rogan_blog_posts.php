<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use WP_Query;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_blog_posts extends Widget_Base {

    public function get_name() {
        return 'rogan_blog';
    }

    public function get_title() {
        return esc_html__( 'Blog posts', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-post';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return ['owl-carousel'];
    }

    public function get_style_depends() {
        return ['owl-carousel', 'owl-theme', 'animate'];
    }

    protected function _register_controls() {

        // ----------------------------------------  Section Style ------------------------------
        $this->start_controls_section(
            'style_sec',
            [
                'label' => esc_html__( 'Select Style', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'style', [
                'label' => esc_html__( 'Select Style', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'style_01' => esc_html__('Style One', 'rogan-core'),
                    'style_02' => esc_html__('Style Two', 'rogan-core'),
                ],
                'default' => 'style_01'
            ]
        );

        $this->end_controls_section();


        $this->start_controls_section(
            'blog_sec', [
                'label' => esc_html__( 'Title Setion', 'rogan-core' ),
                'condition' => [
                    'style' => 'style_01',
                ]
            ]
        );

        $this->add_control(
            'upper_title', [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'OUR BLOG'
            ]
        );

        $this->add_control(
            'main_title', [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Our Company News'
            ]
        );

        $this->end_controls_section();


        // ---------------------------------- Filter Options ------------------------
        $this->start_controls_section(
            'filter', [
                'label' => esc_html__( 'Filter', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'show_count', [
                'label' => esc_html__( 'Show Posts Count', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 3
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__( 'Order', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->end_controls_section();

        // -------------------------------------- Column Grid Section ---------------------------------//
        $this->start_controls_section(
            'btn_sec', [
                'label' => __( 'Button', 'saasland-core' ),
                'condition' => [
                    'style' => 'style_02'
                ]
            ]
        );

        $this->add_control(
            'read_more_btn', [
                'label' => __( 'Button', 'saasland-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Read More'
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tab
         * SVG Shape Images
         */
        // *************************** Section Upper Title Style ***********************************************
        $this->start_controls_section(
            'style_upper_title_sec',
            [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'style' => 'style_01',
                ]

            ]
        );

        $this->add_control(
            'style_upper_title_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .upper-title' => 'color: {{VALUE}};',
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_upper_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one .upper-title',
            ]
        );

        $this->end_controls_section();//upper title


        //----------------------------- Main Title ---------------------------------------//
        $this->start_controls_section(
            'style_main_title_sec',
            [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'style' => 'style_01',
                ]

            ]
        );

        $this->add_control(
            'style_main_title_sec',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_main_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();


        //--------------------------- Blog Item Hover Color -------------------------------------//
        $this->start_controls_section(
            'blog_item_hover_color_sec', [
                'label' => esc_html__( 'Blog Item Hover', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'style' => 'style_01',
                ]

            ]
        );

        $this->add_control(
            'hover_bg_color', [
                'label' => esc_html__( 'Item Hover Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .agn-home-blog .single-blog-post .flip-box-back' => 'background: {{VALUE}};',
                )
            ]
        );

        $this->end_controls_section();


        /****************************** shape Image **************************/
        $this->start_controls_section(
            'bg_shape', [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'style' => 'style_01',
                ]
            ]
        );

        $this->add_control(
            'alt', [
                'label' => esc_html__( 'Image alt Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Blog Shape'
            ]
        );

        $this->add_control(
            'shape1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-58.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape2',
            [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-57.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape3',
            [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-9.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape4',
            [
                'label' => esc_html__( 'Shape Four', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-55.svg', __FILE__)
                ],
            ]
        );

        $this->end_controls_section();


        // -------------------------------------- Column Grid Section ---------------------------------//
        $this->start_controls_section(
            'column_sec', [
                'label' => __( 'Grid Column', 'saasland-core' ),
                'tab'   => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'style' => 'style_02'
                ]
            ]
        );

        $this->add_control(
            'column', [
                'label' => __( 'Grid Column', 'saasland-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '6' => __( 'Two column', 'saasland-core' ),
                    '4' => __( 'Three column', 'saasland-core' ),
                    '3' => __( 'Four column', 'saasland-core' ),
                ],
                'default' => '4'
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .agn-home-blog' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    '{{WRAPPER}} .arch-blog' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {

        $settings = $this->get_settings();

        $blog_posts = new WP_Query(array(
            'post_type'     => 'post',
            'posts_per_page'=> $settings['show_count'],
            'order' => $settings['order'],
        ));

        if ($settings['style'] == 'style_01') {
            include 'blog/blog-1.php';
        }

        if ($settings['style'] == 'style_02' ) {
            include 'blog/blog-2.php';
        }
    }
}