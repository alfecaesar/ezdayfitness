<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_processes extends Widget_Base {

    public function get_name() {
        return 'Rogan_processes';
    }

    public function get_title() {
        return esc_html__( 'Processes', 'rogan-hero' );
    }

    public function get_icon() {
        return 'eicon-product-related';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ----------------------------------------  Hero content ------------------------------
        $this->start_controls_section(
            'contents_sec',
            [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'title',
            [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Research Your Internal Objectives.',
            ]
        );

        $repeater->add_control(
            'subtitle',
            [
                'label' => esc_html__( 'Description Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $repeater->add_control(
            'content_list',
            [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' => Controls_Manager::WYSIWYG,
            ]
        );

        $repeater->add_control(
            'icon_box', [
                'label' => esc_html__( 'Image Icon', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default'=> [
                    'url'   => plugins_url('images/icon/icon12.svg',__FILE__)
                ]
            ]
        );

        $repeater->add_control(
            'icon_box_bg_color',
            [
                'label' => esc_html__( 'Icon Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}.single-work-list .text-wrapper .icon-box' => 'Background: {{VALUE}}',
                ],
            ]
        );

        $repeater->add_control(
            'icon_wave_color',
            [
                'label' => esc_html__( 'Icon Wave Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}.single-work-list .text-wrapper .icon-box:before' => 'Background: {{VALUE}}',
                ],
            ]
        );

        $repeater->add_control(
            'shape_image', [
                'label' => esc_html__( 'Featured Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default'=> [
                    'url'   => plugins_url('images/shape/shape-3.svg',__FILE__)
                ]
            ]
        );

        $repeater->add_control(
            'is_row_reverse',
            [
                'label' => esc_html__( 'Row Reverse', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Yes', 'rogan-core' ),
                'label_off' => esc_html__( 'No', 'rogan-core' ),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'rows', [
                'label' => esc_html__( 'Processes', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ title }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section(); // End Hero content


        /**
         * Style Tab
         * Font Style
         * Typography
         */
        //------------------------------ Title Section ------------------------------
        $this->start_controls_section(
            'item_title_style_sec',
            [
                'label' => esc_html__( 'Item Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'item_title_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-work-progress .single-work-list .text-wrapper .title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'item_title_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .seo-work-progress .single-work-list .text-wrapper .title',
            ]
        );

        $this->end_controls_section(); // End


        //--------------------------------------- Subtitle
        $this->start_controls_section(
            'item_subtitle_style_sec',
            [
                'label' => esc_html__( 'Item Subtitle', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'item_subtitle_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-work-progress .single-work-list .text-wrapper p' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'item_subtitle_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .seo-work-progress .single-work-list .text-wrapper p',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'item_check_mark_style_sec',
            [
                'label' => esc_html__( 'Item Icon', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'item_check_mark_color',
            [
                'label' => esc_html__( 'Icon Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .seo-work-progress .single-work-list .text-wrapper ul li:before' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        $rows = isset($settings['rows']) ? $settings['rows'] : '';
        ?>

        <div class="seo-work-progress">
            <div class="container">
                <?php
                if (!empty($rows)) {
                foreach ($rows as $row) {
                    ?>
                    <div class="row single-work-list elementor-repeater-item-<?php echo $row['_id'] ?> <?php echo $row['is_row_reverse'] == 'yes' ? 'flex-row-reverse' : ''; ?>">
                        <div class="col-lg-6 order-lg-last">
                            <div class="text-wrapper">
                                <?php if (!empty($row['icon_box']['url'])) : ?>
                                    <div class="icon-box">
                                        <img src="<?php echo esc_url($row['icon_box']['url']) ?>" alt="<?php echo esc_attr($row['title']) ?>">
                                    </div>
                                <?php endif; ?>
                                <?php if(!empty($row['title'])) : ?>
                                    <h2 class="title"><?php echo esc_html($row['title'])?></h2>
                                <?php endif; ?>
                                <?php echo !empty($row['subtitle']) ? wpautop($row['subtitle']) : ''; ?>
                                <?php if (!empty($row['content_list'])) : ?>
                                    <?php echo wp_kses_post($row['content_list']) ?>
                                <?php endif; ?>
                            </div> <!-- /.text-wrapper -->
                        </div>
                        <?php if (!empty($row['icon_box']['url'])) : ?>
                            <div class="col-lg-6 order-lg-first">
                                <div class="img-box clearfix">
                                    <img src="<?php echo esc_url($row['shape_image']['url']) ?>" alt="<?php echo esc_attr($row['title']) ?>">
                                </div> <!-- /.img-box -->
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php
                }}
                ?>
            </div> <!-- /.container -->
        </div>
        <?php
    }
}