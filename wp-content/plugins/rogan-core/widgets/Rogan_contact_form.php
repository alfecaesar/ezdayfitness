<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_contact_form extends Widget_Base {

    public function get_name() {
        return 'rogan_contact_form';
    }

    public function get_title() {
        return esc_html__( 'Contact Form', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-form-horizontal';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_keywords() {
        return [ 'Count', 'Stats' ];
    }

    public function get_script_depends() {
        return [ 'countto' ];
    }

    protected function _register_controls()
    {

        //******************************* Title Section***************************************//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__('Section Title', 'rogan-core'),
            ]
        );

        $this->add_control(
            'upper_title', [
                'label' => esc_html__('Upper Title', 'rogan-core'),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'Contact Us',
            ]
        );

        $this->add_control(
            'title', [
                'label' => esc_html__('Title', 'rogan-core'),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'Send me a message, I will be touch ',
            ]
        );

        $this->add_control(
            'icon', [
                'label' => esc_html__( 'Icon', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/icon22.svg')
                ]
            ]
        );

        $this->end_controls_section();


        //----------------------------- Counter Section --------------------------------------//
        $this->start_controls_section(
            'contact_form_sec',
            [
                'label' => esc_html__( 'Contact Form', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'cf7_shortcode',
            [
                'label' => esc_html__( 'Contact Form Shortcode', 'rogan-core' ),
                'description' => __( 'Make the contact form with Contact From 7 plugin and place the From shortcode here. Get the contact form template <a href="https://is.gd/GOu9XL">here</a>', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );


        $this->end_controls_section();


        /**
         * Style Tab
         * Upper Title
         */
        $this->start_controls_section(
            'upper_title_color_sec',
            [
                'label' => esc_html__( 'Upper Title', 'rogan-core'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'upper_title_color',
            [
                'label' => esc_html__( 'Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .upper-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'upper_title_typography',
                'label' => esc_html__('Typography', 'rogan-core'),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .upper-title',
            ]
        );

        $this->end_controls_section();

        // Section Title
        $this->start_controls_section(
            'section_title_color',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => esc_html__( 'Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .main-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'label' => esc_html__('Typography', 'rogan-core'),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .main-title',
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tab
         * @ Top Vertical Line
         */
        $this->start_controls_section(
            'top_vertical_line_sec',
            [
                'label' => esc_html__('Top Vertical Line', 'rogan-core'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'is_vertical_line',
            [
                'label' => esc_html__( 'Vertical Line', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Yes', 'rogan-core' ),
                'label_off' => esc_html__( 'No', 'rogan-core' ),
                'return_value' => 'yes',
                'defualt' => 'yes'
            ]
        );

        $this->add_control(
            'line_color', [
                'label' => esc_html__( 'Background Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .portfo-footer:before' => 'background: {{VALUE}};',
                ],
                'condition' => [
                    'is_vertical_line' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'line_width',
            [
                'label' => esc_html__( 'Line Width', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px' ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 10,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .portfo-footer:before' => 'width: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'is_vertical_line' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'line_height',
            [
                'label' => esc_html__( 'Line Height', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px' ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                    'size' => 182,
                ],
                'selectors' => [
                    '{{WRAPPER}} .portfo-footer:before' => 'height: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'is_vertical_line' => 'yes'
                ]
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tab
         * @ Background Style
         */
        $this->start_controls_section(
            'bg_style_sec',
            [
                'label' => esc_html__('Background Style', 'rogan-core'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'rogan-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .portfo-footer' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'isLinked' => false,
                ],
            ]
        );

        $this->add_control(
            'shape',
            [
                'label' => esc_html__( 'Top Left Shape', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-7.svg', __FILE__)
                ],
            ]
        );

        // Gradient Color
        $this->add_control(
            'bg_color', [
                'label' => esc_html__( 'Background Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );

        $this->add_control(
            'bg_color2', [
                'label' => esc_html__( 'Background Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .portfo-footer' => 'background: linear-gradient( -180deg, {{bg_color.VALUE}} 0%, {{VALUE}} 100%);',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings();
        ?>
        <div class="portfo-footer hide-pr show-pr">
            <?php if( !empty($settings['shape']['url'] )) : ?>
                <img src="<?php echo esc_url($settings['shape']['url']) ?>" alt="<?php echo esc_attr($settings['title']) ?>" class="round-shape">
            <?php endif; ?>
            <div class="container">
                <div class="theme-title-one text-center hide-pr">
                    <?php if( !empty($settings['upper_title']) ) : ?>
                        <div class="upper-title"> <?php echo esc_html($settings['upper_title']) ?> </div>
                    <?php endif; ?>
                    <?php if (!empty($settings['title'])) : ?>
                        <h2 class="main-title"><?php echo wp_kses_post(nl2br($settings['title'])) ?></h2>
                    <?php endif; ?>
                </div> <!-- /.theme-title-one -->
                <?php echo !empty($settings['cf7_shortcode']) ? do_shortcode($settings['cf7_shortcode']) : ''; ?>
            </div>
        </div>
        <?php
    }
}