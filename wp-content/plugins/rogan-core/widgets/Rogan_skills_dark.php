<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_skills_dark extends Widget_Base {

    public function get_name() {
        return 'rogan_skils_dark';
    }

    public function get_title() {
        return esc_html__( 'Skills (Dark)', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-skill-bar';
    }

    public function get_keywords() {
        return [ 'expertise', 'portfolio', 'skill', 'progress', 'skill bar' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return [ 'countto' ];
    }

    protected function _register_controls() {

        //***********************************  Section Title ******************************************//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Experties'
            ]
        );

        $this->add_control(
            'title_append',
            [
                'label' => esc_html__( 'Title Append', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => '.'
            ]
        );

        $this->end_controls_section();


        //************************* Page Number ****************************//
        $this->start_controls_section(
            'number_sec',
            [
                'label' => esc_html__( 'Section Number', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'number1',
            [
                'label' => esc_html__( 'Number One', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
            ]
        );

        $this->add_control(
            'number2',
            [
                'label' => esc_html__( 'Number Two', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
            ]
        );

        $this->end_controls_section();


        //*************************** Skills Options ******************************//
        $this->start_controls_section(
            'skills_sec',
            [
                'label' => esc_html__( 'Skills Options', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Default Title'
            ]
        );

        $repeater->add_control(
            'percent',
            [
                'label' => esc_html__( 'Percent', 'rogan-core' ),
                'description' => esc_html__( 'Percent of the Skill', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ '%' ],
                'range' => [
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => '%',
                    'size' => 80,
                ],
            ]
        );

        $repeater->add_control(
            'tools',
            [
                'label' => esc_html__( 'Tools', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'HTML, CSS'
            ]
        );

        $this->add_control(
            'skills',
            [
                'label' => esc_html__( 'Skills', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ title }}}',
            ]
        );

        $this->end_controls_section();



        /**
         * Style Tabs
         */
        /********************************* Section Title ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_sec_title',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-two' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_sce_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .theme-title-two',
            ]
        );

        $this->add_control(
            'title_style_hr',
            [
                'type' => \Elementor\Controls_Manager::DIVIDER,
            ]
        );


        $this->add_control(
            'color_sec_title_append',
            [
                'label' => esc_html__( 'Title Append Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-two span' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();


        /********************************* Page Number ********************************/
        $this->start_controls_section(
            'style_page_number', [
                'label' => esc_html__( 'Number Style', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_page_number',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .section-portfo .section-num' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_page_number',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .section-portfo .section-num',
            ]
        );

        $this->end_controls_section();


        //------------------------------ Section Style ------------------------------
        $this->start_controls_section(
            'sec_style',
            [
                'label' => esc_html__( 'Style Section', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE
            ]
        );

        $this->add_control(
            'column', [
                'label' => esc_html__( 'Column', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '6' => esc_html__('Two', 'rogan-core'),
                    '4' => esc_html__('Three', 'rogan-core'),
                    '3' => esc_html__('Four', 'rogan-core')
                ],
                'default' => '6'
            ]
        );

        // Gradient Color
        $this->add_control(
            'bg_color', [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );

        $this->add_control(
            'bg_color2', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .skill-section-portfo:before' => 'background: -webkit-radial-gradient( 50% 50%, {{bg_color.VALUE}} 0%, {{VALUE}} 100%)',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        $title_append = !empty($settings['title_append']) ? '<span>'.$settings['title_append'].'</span>' : '';
        ?>
        <div class="section-portfo skill-section-portfo">
            <div class="section-num hide-pr">
                <?php if(isset($settings['number1'])) : ?>
                    <span><?php echo esc_html($settings['number1']) ?></span>
                <?php endif; ?>
                <?php if(isset($settings['number2'])) : ?>
                    <span><?php echo esc_html($settings['number2']) ?></span>
                <?php endif; ?>
            </div>
            <div class="container">
                <?php if(!empty($settings['title_text'])) : ?>
                    <h2 class="theme-title-two">
                        <?php echo wp_kses_post($settings['title_text']); echo wp_kses_post($title_append); ?>
                    </h2>
                <?php endif; ?>
                <div class="row">
                    <?php
                    if(!empty($settings['skills'])) {
                        foreach ($settings['skills'] as $skill) {
                            ?>
                            <div class="col-lg-<?php echo $settings['column'] ?>">
                                <div class="skill-bar">
                                    <div class="progress">
                                        <div class="progress-bar" data-percent="<?php echo esc_attr($skill['percent']['size']) ?>">
                                            <span class="percent-text"><span class="count"></span>%</span>
                                        </div>
                                    </div>
                                    <?php if(!empty($skill['title'])) : ?>
                                        <div class="skill-title"> <?php echo esc_html($skill['title']) ?> </div>
                                    <?php endif; ?>
                                    <?php if(!empty($skill['tools'])) : ?>
                                        <span class="category"> <?php echo esc_html($skill['tools']) ?> </span>
                                    <?php endif; ?>
                                </div> <!-- /.skill-bar -->
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div> <!-- /.row -->
            </div>
        </div>
        <?php
    }
}