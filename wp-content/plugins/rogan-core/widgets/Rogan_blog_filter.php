<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use WP_Query;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_blog_filter extends Widget_Base {

    public function get_name() {
        return 'rogan_blog_filter';
    }

    public function get_title() {
        return esc_html__( 'Blog Filter Section', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-posts-masonry';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return ['isotope'];
    }

    protected function _register_controls() {

        $this->start_controls_section(
            'blog_sec', [
                'label' => esc_html__( 'Blog Filter - Posts', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'upper_title', [
                'label' => esc_html__( 'Section Upper Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'OUR NEWS'
            ]
        );

        $this->add_control(
            'main_title', [
                'label' => esc_html__( 'Section Main Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Our inside stories for you'
            ]
        );

        $this->end_controls_section();


        // ---------------------------------- Filter Options ------------------------
        $this->start_controls_section(
            'filter', [
                'label' => esc_html__( 'Blog Filter', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'show_count', [
                'label' => esc_html__( 'Show Posts Count', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 9
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__( 'Order', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->end_controls_section();

        /**
         * Style Tab
         * SVG Shape Images
         */
        // *************************** Section Title Style ***********************************************
        $this->start_controls_section(
            'sec_title_style',
            [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_upper_title',
            [
                'label' => esc_html__( 'Upper Title Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .blog-filter-title .upper-title' => 'color: {{VALUE}};',
                ]
            ]
        );

        $this->add_control(
            'color_main_title',
            [
                'label' => esc_html__( 'Main Title Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .blog-filter-title .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_upper_title',
                'label' => esc_html__( 'Upper Title Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .blog-filter-title .upper-title',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_main_title',
                'label' => esc_html__( 'Main Title Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .blog-filter-title .main-title',
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {

        $settings = $this->get_settings();

        $blog_posts = new WP_Query(array(
            'post_type'     => 'post',
            'posts_per_page'=> $settings['show_count'],
            'order' => $settings['order'],
        ));
        ?>

            <div class="our-blog blog-filer mb-150">
                <div class="blog-filter-title">
                   <?php if (!empty($settings['upper_title'])) : ?>
                        <div class="upper-title"><?php echo esc_html($settings['upper_title']) ?></div>
                    <?php endif; ?>
                    <?php if (!empty($settings['main_title'])) : ?>
                        <h2 class="main-title"><?php echo $settings['main_title'] ?></h2>
                    <?php endif; ?>
                </div> <!-- /.blog-filter-title -->
                
                <ul class="isotop-menu-wrapper blog-filter-nav clearfix">
                    <li class="is-checked" data-filter="*"><span>All</span></li>
                    <?php 
                        $cats = get_terms('category'); 
                        foreach ($cats as $cat) :
                    ?>
                    <li data-filter=".<?php echo $cat->slug; ?>"><span><?php echo $cat->name; ?></span></li>
                    <?php endforeach; ?>
                </ul>

                <div class="masnory-blog-wrapper">
                    <div class="grid-sizer"></div>

                    <?php while ($blog_posts->have_posts()) : $blog_posts->the_post();    
                        $blog_cats = get_the_terms( get_the_ID(), 'category' );
                        if ( $blog_cats && ! is_wp_error( $blog_cats ) ) {
                            $blog_cat_list = array();
                            foreach ( $blog_cats as $cat ) {
                                $blog_cat_list[] = $cat->slug;
                            }
                            $blog_all_cats = join( " ", $blog_cat_list);
                        }
                        else {
                            $blog_all_cats = '';
                        }  
                    ?>
                    <div class="isotop-item <?php echo $blog_all_cats; ?>">
                        <div <?php post_class('single-blog-post'); ?>>
                            <?php if ( has_post_thumbnail() ) : ?>
                               <div class="img-holder"><?php the_post_thumbnail('rogan_415x360'); ?></div>
                            <?php endif; ?>
                            <div class="post-data">
                                <a href="<?php rogan_day_link(); ?>" class="date"><?php the_time( get_option( 'date_format' ) ); ?></a>
                                <h5 class="blog-title-one title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                <p><?php echo wp_trim_words(get_the_content(), 16, '') ?></p>
                                <a href="<?php the_permalink(); ?>" class="read-more"><i class="flaticon-next-1"></i></a>
                            </div> <!-- /.post-data -->
                        </div> <!-- /.single-blog-post -->
                    </div> <!-- /.isotop-item -->
                    <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>

                </div> <!-- /.masnory-blog-wrapper -->
                <div class="theme-pagination-one text-center pt-15">
                    <?php rogan_pagination(); ?>
                </div> <!-- /.theme-pagination-one -->
            </div> <!-- /.our-blog -->


        <script>
            (function ( $ ) {
                "use strict";
                $(document).ready( function() {

                // ----------------------------- isotop gallery
                if ($(".masnory-blog-wrapper").length) {
                    var $grid = $('.masnory-blog-wrapper').isotope({
                      // options
                      itemSelector: '.isotop-item',
                      percentPosition: true,
                      masonry: {
                        // use element for option
                        columnWidth: '.grid-sizer'
                      }

                    });

                    // filter items on button click
                    $('.isotop-menu-wrapper').on( 'click', 'li', function() {
                      var filterValue = $(this).attr('data-filter');
                      $grid.isotope({ filter: filterValue });
                    });

                     // change is-checked class on buttons
                      $('.isotop-menu-wrapper').each( function( i, buttonGroup ) {
                        var $buttonGroup = $( buttonGroup );
                        $buttonGroup.on( 'click', 'li', function() {
                          $buttonGroup.find('.is-checked').removeClass('is-checked');
                          $( this ).addClass('is-checked');
                        });
                      });
                }

                });
            }( jQuery ));
        </script>
        <?php
    }
}