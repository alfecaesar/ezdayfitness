<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use WP_Query;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_blog_full_grid extends Widget_Base {

    public function get_name() {
        return 'rogan_blog_full_grid';
    }

    public function get_title() {
        return esc_html__( 'Blog Full Grid Posts', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-image-box';
    }

    public function get_keywords() {
        return [ 'rogan', 'blog', 'grid' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

       


        // ---------------------------------- Filter Options ------------------------
        $this->start_controls_section(
            'filter', [
                'label' => esc_html__( 'Filter', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'show_count', [
                'label' => esc_html__( 'Show Posts Count', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 4
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__( 'Order', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->end_controls_section();

        /**
         * Style Tab
         * SVG Shape Images
         */
        // ***************************  Title Style ***********************************************
        $this->start_controls_section(
            'blog_post_style',
            [
                'label' => esc_html__( 'Blog Contnet Style', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_main_title',
            [
                'label' => esc_html__( 'Heading - Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .blog-default .single-blog-post .post-data .title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_main_title',
                'label' => esc_html__( 'Heading Title Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .blog-default .single-blog-post .post-data .title',
            ]
        );


        $this->add_control(
            'color_blog_text',
            [
                'label' => esc_html__( 'Blog Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .our-blog .post-data p' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_text_p',
                'label' => esc_html__( 'Blog Text Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .our-blog .post-data p',
            ]
        );

        $this->end_controls_section();
     
    }

    protected function render() {

        $settings = $this->get_settings();

        $blog_posts = new WP_Query(array(
            'post_type'     => 'post',
            'posts_per_page'=> $settings['show_count'],
            'order' => $settings['order'],
        ));
        ?>
        <div class="our-blog blog-default pt-150 mb-200">
            <div class="full-grid-container">

                <?php
                while ($blog_posts->have_posts()) : $blog_posts->the_post();
                if( has_post_format('video') ) {
                    wp_enqueue_style( 'fancybox' );
                    wp_enqueue_script( 'fancybox' );
                } 
                if ( has_post_format('quote') ) { ?>
                    <div class="single-blog-post bg-solid-post hide-pr show-pr">
                        <div class="post-data">
                            <h5 class="blog-title-two title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                            <div class="author"><?php the_author(); ?></div>
                        </div> <!-- /.post-data -->
                    </div> <!-- /.single-blog-post -->
                <?php 
                }
                else { ?>
                    <div <?php post_class('single-blog-post'); ?>>
                        <?php
                        if(is_sticky()) {
                            echo '<p class="sticky-label">'.esc_html__('Featured', 'rogan').'</p>';
                        }
                        ?>
                        <?php if ( has_post_thumbnail() ) : ?>
                           <div class="img-holder"><?php the_post_thumbnail('rogan_1000x500'); ?></div>
                        <?php endif; ?>
                        <div class="post-data">
                            <a href="<?php rogan_day_link(); ?>" class="date"><?php the_time( get_option( 'date_format' ) ); ?></a>
                            <h5 class="blog-title-two title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                            <p><?php echo wp_trim_words(get_the_content(), 40, '') ?></p>
                            <a href="<?php the_permalink(); ?>" class="read-more"><i class="flaticon-next-1"></i></a>
                        </div> <!-- /.post-data -->
                    </div> <!-- /.single-blog-post -->
                    <?php 
                }
                ?>

                <?php
                endwhile;
                wp_reset_postdata();
                ?>
                <div class="theme-pagination-one pt-15">
                    <?php rogan_pagination(); ?>
                </div>
            </div> <!-- /.full-grid-container -->
        </div> <!-- /.our-blog -->

        <?php
    }
}