<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_services_saas extends Widget_Base {

    public function get_name() {
        return 'rogan_services_saas';
    }

    public function get_title() {
        return esc_html__( 'Service Saas', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-home-heart';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return ['aos-next'];
    }

    protected function _register_controls() {

        // ***************************** Service Title **************************************//
        $this->start_controls_section(
            'title_sec', [
                'label' => esc_html__( 'Service Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'Provide awesome customer service <br>  with our tools.',
            ]
        );

        $this->end_controls_section();

        // ********************************* Icon Box ******************************************//
        $this->start_controls_section(
            'icon_box',
            [
                'label' => esc_html__( 'Icon Box', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'icon_img',
            [
                'label' => esc_html__( 'Icon Image', 'rogan-core' ),
                'description' => esc_html__( 'Recommended to upload a png image.', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/icon23.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'bg_shape',
            [
                'label' => esc_html__( 'Background Image', 'rogan-core' ),
                'description' => esc_html__( 'Recommended to use SVG to PNG image as the background shape.', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/bg-shape1.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();



        // *************************************** Service List *************************************//
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__( 'Service List', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'title', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' =>Controls_Manager::TEXTAREA,
                'default' => esc_html__( 'User Friendly dashboard & Cool Interface.' , 'rogan-core' ),
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'subtitle', [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
                'type' =>Controls_Manager::TEXTAREA,
            ]
        );

        $repeater->add_control(
            'icon', [
                'label' => esc_html__( 'Top Icon', 'rogan-core' ),
                'type' =>Controls_Manager::ICON,
                'default' => 'flaticon-web',
                'options' => rogan_flat_icons(),
                'include' => rogan_include_flaticon_icons()
            ]
        );

        $repeater->add_control(
            'icon_color', [
                'label' => esc_html__( 'Icon Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}} .service-block .icon-s' => 'color: {{VALUE}}',
                )
            ]
        );

        $repeater->add_control(
            'url', [
                'label' => esc_html__( 'URL', 'rogan-core' ),
                'type' =>Controls_Manager::URL,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'bottom_icon', [
                'label' => esc_html__( 'Bottom Icon', 'rogan-core' ),
                'type' =>Controls_Manager::ICON,
                'options' => rogan_flat_icons(),
                'include' => rogan_include_flaticon_icons()
            ]
        );

        $this->add_control(
            'service_list',
            [
                'label' => esc_html__( 'Services', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{title}}}'
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tab
         * @Title
         * @Hover Colors
         */
        /********************************* Title ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_prefix', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_prefix',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();


        /**************************** Hover Colors ********************************/
        $this->start_controls_section(
            'hover_colors_sec', [
                'label' => esc_html__( 'Item Hover Colors', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE
            ]
        );

        $this->add_control(
            'hover_bg_color1', [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-service-sass .service-block:before' => 'background: {{VALUE}};',
                )
            ]
        );

        $this->add_control(
            'hover_bg_color2', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-service-sass .service-block .hover-content:before' => 'background: {{VALUE}};',
                )
            ]
        );

        $this->add_control(
            'hover_bg_color3', [
                'label' => esc_html__( 'Color Three', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-service-sass .service-block .hover-content:after' => 'background: {{VALUE}};',
                )
            ]
        );

        $this->add_control(
            'hover_bg_color4', [
                'label' => esc_html__( 'Color Four', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .our-service-sass .service-block .hover-content' => 'background: {{VALUE}};',
                )
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        $services = $settings['service_list'];
        ?>

        <div class="our-service-sass hide-pr">
            <div class="container">

                <div class="theme-title-one text-center">
                    <div class="icon-box hide-pr">
                        <?php if (!empty($settings['bg_shape']['url'])) : ?>
                            <img src="<?php echo esc_url($settings['bg_shape']['url']) ?>" alt="<?php echo esc_attr($settings['title_text']) ?>" class="bg-shape">
                        <?php endif; ?>
                        <?php if (!empty($settings['icon_img']['url'])) : ?>
                            <img src="<?php echo esc_url($settings['icon_img']['url']) ?>" alt="<?php echo esc_attr($settings['title_text']) ?>" class="icon">
                        <?php endif; ?>
                    </div>
                    <?php if (!empty($settings['title_text'])) : ?>
                        <h2 class="main-title"> <?php echo wp_kses_post($settings['title_text']) ?> </h2>
                    <?php endif; ?>
                </div> <!-- /.theme-title-one -->

                <div class="inner-wrapper">
                    <div class="row">
                        <?php
                        if (!empty($services)) {
                            $i = 0;
                            foreach ($services as $service) {
                                ?>
                                <div class="col-lg-4 single-block elementor-repeater-item-<?php echo $service['_id'] ?>" data-aos="fade-up"
                                    <?php if($i != 0) : ?> data-aos-delay="<?php echo $i; ?>" <?php endif; ?>>
                                    <div class="service-block">
                                        <span class="snow-dot"></span>
                                        <span class="snow-dot"></span>
                                        <span class="snow-dot"></span>
                                        <span class="snow-dot"></span>
                                        <span class="snow-dot"></span>
                                        <span class="snow-dot"></span>
                                        <span class="snow-dot"></span>
                                        <div class="hover-content"></div>
                                        <?php if (!empty($service['icon'])) : ?>
                                            <i class="<?php echo esc_attr($service['icon']) ?> icon-s"></i>
                                        <?php endif; ?>
                                        <?php if (!empty($service['title'])) : ?>
                                            <h5 class="title">
                                                <a href="<?php echo esc_url($service['url']['url']) ?>" <?php rogan_is_external($service['url']); rogan_is_nofollow($service['url']) ?>>
                                                    <?php echo esc_html($service['title']) ?>
                                                </a>
                                            </h5>
                                        <?php endif; ?>
                                        <?php if (!empty($service['subtitle'])) : ?>
                                            <?php echo wp_kses_post(wpautop($service['subtitle'])); ?>
                                        <?php endif; ?>
                                        <a href="<?php echo esc_url($service['url']['url']) ?>" class="detail-button" <?php rogan_is_external($service['url']); rogan_is_nofollow($service['url']) ?>>
                                            <i class="<?php echo esc_attr($service['bottom_icon']) ?>"></i>
                                        </a>
                                    </div> <!-- /.service-block -->
                                </div>
                                <?php
                                $i = $i + 200;
                            }
                        }
                        ?>
                    </div> <!-- /.row -->
                </div> <!-- /.inner-wrapper -->
            </div> <!-- /.container -->
        </div>

        <?php
    }
}