<div class="our-service service-modren">
    <div class="container">

        <?php
        $i = 0;
        while ($service->have_posts()) : $service->the_post();
            switch ( $i ) {
                case 0:
                    $cat_color = 'one';
                    break;
                case 1:
                    $cat_color = 'two';
                    break;
                case 2:
                    $cat_color = 'three';
                    break;
                case 3:
                    $cat_color = 'four';
                    break;
                default:
                    $cat_color = 'one';
                    break;
            }
            ?>
            <div class="row service-block align-items-center modern_service_<?php echo $i; ?>">
                <div class="col-md-6 order-md-<?php echo ($i % 2 == 0) ? 'first' : 'last'; ?>" data-aos="fade-left">
                    <div class="service-info">
                        <?php if ( function_exists( 'rogan_first_category' ) ) : ?>
                            <div class="tag color-<?php echo esc_attr($cat_color) ?>" > <?php rogan_first_category('service_cat') ?> </div>
                        <?php endif; ?>
                        <h2 class="service-title">
                            <a href="<?php the_permalink() ?>"> <?php the_title() ?> </a>
                        </h2>
                        <a href="<?php the_permalink() ?>" class="read-more"><i class="flaticon-next-1"></i></a>
                    </div>
                </div>
                <div class="col-md-6 order-md-<?php echo ($i % 2 == 0) ? 'last' : 'first'; ?>" data-aos="fade-right">
                    <div class="img-box"> <?php the_post_thumbnail( 'rogan_510x550' ) ?> </div>
                </div>
            </div> <!-- /.service-block -->
            <?php
            ++$i;
        endwhile;
        ?>
    </div> <!-- /.container -->
</div>