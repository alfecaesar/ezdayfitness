<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_team extends Widget_Base {

    public function get_name() {
        return 'rogan_team_minimal';
    }

    public function get_title() {
        return esc_html__( 'Rogan Team', 'rogan-core' );
    }

    public function get_icon() {
        return ' eicon-person';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls()
    {

        // ----------------------------------------  Select Style ------------------------------
        $this->start_controls_section(
            'sec_style',
            [
                'label' => esc_html__( 'Select Style', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'team_style', [
                'label' => esc_html__( 'Select Style', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'style_1' => esc_html__('Style One (Minimal)', 'rogan-core'),
                    'style_2' => esc_html__('Style Two (Standard)', 'rogan-core'),
                    'style_3' => esc_html__('Style Three (Carousel)', 'rogan-core'),
                ],
                'default' => 'style_1'
            ]
        );

        $this->end_controls_section();


        ///--------------------------------------- Team Member ----------------------------------///
        $this->start_controls_section(
            'team_sec',
            [
                'label' => esc_html__('Team Member', 'rogan-core'),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'name', [
                'label' => esc_html__('Name', 'rogan-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Hunter Chapman'
            ]
        );

        $repeater->add_control(
            'designation', [
                'label' => esc_html__('Designation', 'rogan-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Co-Founder'
            ]
        );

        $repeater->add_control(
            'image', [
                'label' => esc_html__('Image', 'rogan-core'),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $repeater->add_control(
            'facebook', [
                'label' => esc_html__('Facebook URL', 'rogan-core'),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $repeater->add_control(
            'twitter', [
                'label' => esc_html__('Twitter URL', 'rogan-core'),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $repeater->add_control(
            'linkedin', [
                'label' => esc_html__('Linkedin URL', 'rogan-core'),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $repeater->add_control(
            'instagram', [
                'label' => esc_html__('Instagram URL', 'rogan-core'),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $repeater->add_control(
            'google_plus', [
                'label' => esc_html__('Google-plus URL', 'rogan-core'),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $repeater->add_control(
            'dribbble', [
                'label' => esc_html__('Dribbble URL', 'rogan-core'),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $this->add_control(
            'members', [
                'label' => esc_html__('Team Member', 'rogan-core'),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ name }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section(); // End Team Member

        /// ---------------------------------------   Buttons ----------------------------------///
        $this->start_controls_section(
            'button_sec',
            [
                'label' => esc_html__('Button', 'rogan-core'),
                'condition' => [
                    'team_style' => ['style_1', 'style_2']
                ]
            ]
        );

        $this->add_control(
            'btn_label', [
                'label' => esc_html__('Button Label', 'rogan-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Load More'
            ]
        );

        $this->add_control(
            'btn_url', [
                'label' => esc_html__('Button URL', 'rogan-core'),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        //---------------------------- Normal and Hover ---------------------------//
        $this->start_controls_tabs(
            'style_tabs'
        );


        // Normal Color
        $this->start_controls_tab(
            'normal_btn_style',
            [
                'label' => __( 'Normal', 'saasland-core' ),
            ]
        );

        $this->add_control(
            'normal_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-button-one' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'normal_bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-button-one' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'normal_border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-button-one' => 'border-color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();


        // Hover Color
        $this->start_controls_tab(
            'hover_btn_style',
            [
                'label' => __( 'Hover', 'saasland-core' ),
            ]
        );

        $this->add_control(
            'hover_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-button-one:hover' => 'color: {{VALUE}}',
                ]
            ]
        );

        $this->add_control(
            'hover_bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-button-one:hover' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'hover_border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-button-one:hover' => 'border-color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_section();

        /*
         * Style Tabs
         */

       /* //------------------------------ Section Style ------------------------------
        $this->start_controls_section(
            'col_sec',
            [
                'label' => esc_html__( 'Column Section', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE
            ]
        );

        $this->add_control(
            'column', [
                'label' => esc_html__( 'Column', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '6' => esc_html__('Two', 'rogan-core'),
                    '4' => esc_html__('Three', 'rogan-core'),
                    '3' => esc_html__('Four', 'rogan-core')
                ],
                'default' => '4'
            ]
        );

        $this->end_controls_section();*/


        //------------------------------ Style Team Hover Color ------------------------------
        $this->start_controls_section(
            'hover_sec', [
                'label' => esc_html__('Hover Background', 'rogan-core'),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'team_style' => 'style_1',
                ]
            ]
        );
        $this->add_control(
            'color_title', [
                'label' => esc_html__('Background Color', 'rogan-core'),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .team-minimal .single-team-member .hover-content' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

        /**
         * Section Style
         * Tab: Style
         */
        $this->start_controls_section(
            'style_sec',
            [
                'label' => esc_html__( 'Section Style', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding',
            [
                'label' => __( 'Padding', 'plugin-name' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'devices' => [ 'desktop', 'tablet', 'mobile' ],
                'selectors' => [
                    '{{WRAPPER}} .agn-counter-section' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'sec_margin',
            [
                'label' => __( 'Padding', 'plugin-name' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'devices' => [ 'desktop', 'tablet', 'mobile' ],
                'selectors' => [
                    '{{WRAPPER}} .agn-counter-section' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


    }

    protected function render() {

        $settings = $this->get_settings();
        $members = !empty($settings['members']) ? $settings['members'] : '';

        if ($settings['team_style'] == 'style_1') {
            include 'part-team-minimal.php';
        }

        if ($settings['team_style'] == 'style_2' ) {
            include 'part-team-standard.php';
        }

        if ( $settings['team_style'] == 'style_3' ) {
            include 'part-team-caruosel.php';
        }

    }
}