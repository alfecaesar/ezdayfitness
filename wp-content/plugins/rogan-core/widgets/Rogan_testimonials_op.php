<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_testimonials_op extends Widget_Base {

    public function get_name() {
        return 'rogan_testimonials_op';
    }

    public function get_title() {
        return esc_html__( 'Testimonials Masonry Grid', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-testimonial-carousel';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        //***********************************  Section Title ******************************************//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Whats our client <span>think…</span>'
            ]
        );

        $this->end_controls_section();


        //********************************* Testimonials List **********************************//
        $this->start_controls_section(
            'testimonials_sec',
            [
                'label' => esc_html__( 'Testimonials', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'name',
            [
                'label' => esc_html__( 'Author Name', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Eh Jewel'
            ]
        );

        $repeater->add_control(
            'designation',
            [
                'label' => esc_html__( 'Designation', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'testimonial_txt',
            [
                'label' => esc_html__( 'Testimonial Text', 'rogan-core' ),
                'type' =>Controls_Manager::TEXTAREA,
            ]
        );

        $repeater->add_control(
            'author_img',
            [
                'label' => esc_html__( 'Author Image', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
            ]
        );

        $this->add_control(
            'left_testimonials',
            [
                'label' => esc_html__( 'Left Testimonials', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ name }}}',
            ]
        );

        $right_testimonials = new \Elementor\Repeater();
        $right_testimonials->add_control(
            'name',
            [
                'label' => esc_html__( 'Author Name', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Eh Jewel'
            ]
        );

        $right_testimonials->add_control(
            'designation',
            [
                'label' => esc_html__( 'Designation', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $right_testimonials->add_control(
            'testimonial_txt',
            [
                'label' => esc_html__( 'Testimonial Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $right_testimonials->add_control(
            'author_img',
            [
                'label' => esc_html__( 'Author Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $this->add_control(
            'right_testimonials',
            [
                'label' => esc_html__( 'Right Testimonials', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $right_testimonials->get_controls(),
                'title_field' => '{{{ name }}}',
            ]
        );

        $this->end_controls_section();



        /**
         * Style Tabs
         */
        /********************************* Section Title ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'title_highlight_color',
            [
                'label' => esc_html__( 'Highlighted Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .op-banner-one .main-title span:before' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'heading_typography',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .op-banner-one .main-title',
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .op-testimonial-one' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->add_control(
            'background_color',
            [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .op-testimonial-one' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        $left_testimonials = $settings['left_testimonials'];
        $right_testimonials = $settings['right_testimonials'];
        ?>
        <div class="op-testimonial-one">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-md-6">
                        <?php if ( !empty($settings['title_text']) ) : ?>
                            <h2 class="font-k2d theme-title-four">
                                <?php echo wp_kses_post(nl2br($settings['title_text'])); ?>
                            </h2>
                        <?php endif; ?>
                        <?php
                        if( $left_testimonials ) {
                        foreach ( $left_testimonials as $testimonial ) {
                            if ( !empty($testimonial['author_img']['id']) ) {
                                $author_image = wp_get_attachment_image($testimonial['author_img']['id'], 'full');
                            } else {
                                $author_image = '';
                            }
                            ?>
                            <div class="client-text-box">
                                <?php echo $author_image; ?>
                                <?php if ( !empty( $testimonial['testimonial_txt']) ) : ?>
                                    <blockquote> <?php echo $testimonial['testimonial_txt'] ?> </blockquote>
                                <?php endif; ?>
                                <?php if ( !empty($testimonial['name']) ) : ?>
                                    <h6 class="font-k2d"> <?php echo esc_html($testimonial['name']) ?> </h6>
                                <?php endif; ?>
                                <?php if ( !empty($testimonial['designation']) ) : ?>
                                    <div class="desg"> <?php echo esc_html($testimonial['designation']) ?> </div>
                                <?php endif; ?>
                            </div> <!-- /.client-text-box -->
                            <?php
                        }}
                        ?>
                    </div>

                    <div class="col-md-6">
                        <?php
                        unset($testimonial);
                        if( $right_testimonials ) {
                            foreach ( $right_testimonials as $testimonial ) {
                                if ( isset($testimonial['author_img']['id']) ) {
                                    $author_image = $testimonial['author_img']['id'];
                                    $author_image = wp_get_attachment_image($author_image, 'full');
                                } else {
                                    $author_image = '';
                                }
                                ?>
                                <div class="client-text-box">
                                    <?php echo $author_image; ?>
                                    <blockquote> <?php echo $testimonial['testimonial_txt'] ?> </blockquote>
                                    <?php if ( !empty($testimonial['name']) ) : ?>
                                        <h6 class="font-k2d"> <?php echo esc_html($testimonial['name']) ?> </h6>
                                    <?php endif; ?>
                                    <?php if ( !empty($testimonial['designation']) ) : ?>
                                        <div class="desg"> <?php echo esc_html($testimonial['designation']) ?> </div>
                                    <?php endif; ?>
                                </div> <!-- /.client-text-box -->
                                <?php
                            }}
                        ?>
                    </div>
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div>
        <?php
    }
}