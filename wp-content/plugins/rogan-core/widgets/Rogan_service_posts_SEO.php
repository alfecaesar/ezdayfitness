<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use WP_Query;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_service_posts_SEO extends Widget_Base {

    public function get_name() {
        return 'rogan_service_posts_seo';
    }

    public function get_title() {
        return esc_html__( 'Service Posts (SEO)', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-gallery-grid';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }


    protected function _register_controls() {

        $this->start_controls_section(
            'title_sec', [
                'label' => esc_html__( 'Service Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'upper_title', [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'SERVICES'
            ]
        );

        $this->add_control(
            'main_title', [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->end_controls_section();


        //------------------------------------ Button ----------------------------------------//
        $this->start_controls_section(
            'buttons', [
                'label' => esc_html__( 'Service Button', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'btn_label', [
                'label' => esc_html__( 'Button', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'More Details'
            ]
        );

        $this->end_controls_section();

        /****************************** Background **************************/
        $this->start_controls_section(
            'round_one_sec', [
                'label' => esc_html__( 'Right Corner Latter', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'round_one', [
                'label' => esc_html__( 'Right Corner Letter', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'o'
            ]
        );

        $this->end_controls_section();


        // ---------------------------------- Filter Options ------------------------
        $this->start_controls_section(
            'filter', [
                'label' => esc_html__( 'Filter', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'cat', [
                'label' => esc_html__( 'Category', 'rogan-core' ),
                'description' => esc_html__( 'Input here the category slugs to display services by category. Use comma for multiple category slugs', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'show_count', [
                'label' => esc_html__( 'Show Posts Count', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 3
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__( 'Order', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tab
         * SVG Shape Images
         */
        // *************************** Section Title Style ***********************************************
        $this->start_controls_section(
            'sec_upper_title_style',
            [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_upper_title',
            [
                'label' => esc_html__( 'Upper Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-service-seo .theme-title-one .upper-title' => 'color: {{VALUE}};',
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_upper_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one .upper-title',
            ]
        );

        $this->end_controls_section(); // End Upper Title

        $this->start_controls_section(
            'sec_main_title_style',
            [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_main_title',
            [
                'label' => esc_html__( 'Main Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-service-seo .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_main_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();


        /****************************** Background Gradient Color **************************/
        $this->start_controls_section(
            'bg_gradient', [
                'label' => esc_html__( 'Background Gradient', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        // Style One Gradient color
        $this->add_control(
            'bg_color', [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
            ]
        );

        $this->add_control(
            'bg_color2', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .our-service-seo' => 'background: linear-gradient( -135deg, {{bg_color.VALUE}} 0%, {{VALUE}} 100%);',
                ],

            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'column', [
                'label' => esc_html__( 'Column', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '6' => esc_html__('Two', 'rogan-core'),
                    '4' => esc_html__('Three', 'rogan-core'),
                    '3' => esc_html__('Four', 'rogan-core')
                ],
                'default' => '6'
            ]
        );

        $this->add_control(
            'background_color',
            [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .our-service-seo:before' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .our-service-seo' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                    'isLinked' => false,
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {

        $settings = $this->get_settings();

        if(!empty($settings['cat'])) {
            $service = new WP_Query(array(
                'post_type' => 'service',
                'posts_per_page' => $settings['show_count'],
                'order' => $settings['order'],
                'tax_query' => array(
                    array(
                        'taxonomy' => 'service_cat',
                        'field'    => 'slug',
                        'terms'    => explode(',', $settings['cat']),
                    ),
                ),
            ));
        } else {
            $service = new WP_Query(array(
                'post_type' => 'service',
                'posts_per_page' => $settings['show_count'],
                'order' => $settings['order'],
            ));
        }
        ?>

        <div class="our-service-seo">
            <?php if (!empty($settings['round_one'])) : ?>
                <div class="round-shape-one"><?php echo esc_html($settings['round_one']) ?></div>
            <?php endif; ?>
            <div class="round-shape-two"></div>
            <div class="container">
                <div class="theme-title-one title-underline text-center">
                    <?php if (!empty($settings['upper_title'])) : ?>
                        <div class="upper-title"><?php echo esc_html($settings['upper_title']) ?></div>
                    <?php endif; ?>
                    <?php if (!empty($settings['main_title'])) : ?>
                        <h2 class="main-title"> <?php echo wp_kses_post(nl2br($settings['main_title'])) ?></h2>
                    <?php endif; ?>
                </div> <!-- /.theme-title-one -->
                <div class="row">
                    <?php
                    while ($service->have_posts()) : $service->the_post();
                        ?>
                        <div class="col-md-<?php echo $settings['column'] ?>">
                            <div <?php post_class('single-block') ?> data-aos="flip-right">
                                <?php the_post_thumbnail('full', array('class' => 'icon')) ?>
                                <h4 class="title">
                                    <a href="<?php the_permalink() ?>">
                                        <?php the_title() ?>
                                    </a>
                                </h4>
                                <?php the_excerpt(); ?>
                                <?php if (!empty($settings['btn_label'])) : ?>
                                    <a href="<?php the_permalink() ?>" class="more-button">
                                        <?php echo esc_html($settings['btn_label']) ?>
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </a>
                                <?php endif; ?>
                            </div> <!-- /.single-block -->
                        </div>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div>
        <?php
    }
}