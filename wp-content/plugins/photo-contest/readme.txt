=== WordPress Photo Gallery - Image Gallery ===
Contributors: origincode
Donate link: https://origincode.co/downloads/photo-gallery
Tags: image gallery, photo gallery, WordPress gallery, gallery, video gallery
Requires at least: 4.9
Tested up to: 5.3.2
Stable tag: 1.0.3
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Photo Gallery plugin by Origin code comes with insane image gallery options, that your website certainly needs. Download right away!

== Description ==

### Useful:

* [Home Page](https://origincode.co/downloads/photo-gallery/)
* [Demos](https://origincode.co/photo-gallery-demo-4/)
* [Support Forum](https://wordpress.org/support/plugin/photo-contest/) 
* [Contact US](https://origincode.co/contact/)

Origin Code is here to provide valuable photo gallery plugin made for each WordPress user. Our goal is to deliver the most wanted high quality not only in the plugin code, but also in its support.

We are excited about this photo gallery plugin functionality, because it has everything you need for creating the best image galleries. 

Each gallery image may include:

* Title
* Description 
* URL 

Origin code photo gallery is an ideal solution for creating site-portfolio for photographers. It can be used for demonstration of designer’s portfolio, showing products, services, etc.

The Photo Gallery comes with multiple effects which will help to attract new visitors and customers, by bringing your website freshness and style. It is a modern solution with a pure coding standards and is UI/UX friendly design.

You also get 8 modern photo gallery views to choose from. Just select the one you like the most. All of them have different but unique styles.
 
See detailed description of the views below:


### Demos:

* [Demo Mosaic](https://origincode.co/forms-demo-4/)
* [Demo Justified](https://origincode.co/photo-gallery-demo-3/)
* [Demo Demo Mosaic](https://origincode.co/photo-gallery-demo-6/)
* [Demo Content Popup](https://origincode.co/photo-gallery-demo-5/)
* [Demo Thumbnail View](https://origincode.co/photo-gallery-demo-2/)
* [Demo Lightbox Gallery](https://origincode.co/photo-gallery-demo-1/)


**Gallery/Content Popup view**

This photo gallery view has a photo and content. When you hover the picture you see a magnifying glass on it, and when you click on it opens a popup with a large version of the image, title and description text next to it, also a “View More” button with link , if needed - in the popup, you can scroll the image of other projects.


**Content Slider view**

Content slider view doesn’t slide only image it also slides the title and description of the image which is placed next to it. The content slider also may have a View More button with a special custom page link. Images of the content slider are displayed in the new lightbox, once clicked.


**Lightbox Gallery view**

For those who want to beautifully present photos in different sizes we offer lightbox view. It is a natural decomposition of photos on a page with its original dimensions. It displays the title with mouse hover and redirects to URL on title click. Click on the photo itself opens up a completely new popup with cool functionality.


**Slider Gallery**

Using a slider is always nice if you give preference to sliders, this view is specially for you. Add layers with text content straight on the slider and decorate the slideshow with many beautiful effects


**Thumbnails view**

Showing your masterpiece  images in small image gallery thumbnails is very compact and looks great. Visitor can press on thumbnail and increase the photo in the lightbox to list through them. Hovering the mouse on thumbnail brings a translucent overlay with title and additional small text.


**Justified**

Justified view of the Photo Gallery allows you to showcase your images and videos with your created gallery next to each other without padding between them, filling the container. The size of images may vary and be random in position.


**Masonry view**

Masonry is very popular layout. It place the elements in most optimal position based on free vertical space, sort of like a mason fitting stones in a wall. You could have seen the famous image gallery view all over the Internet.


**Mosaic view**

Mosaic view arranges photos within a flat rectangle layout, automatically picking the sizes. It usually takes the entire width of image gallery container, whereas with Masonry view filling the entire space is not necessarily.


Image gallery backend options are made in minimalistic design which is loved by many developers and users. The plugin provides rating buttons to help visitors leave their impressions by liking or disliking the image gallery photos. To save more time for loading you may set pagination or load more functionality on your gallery. As a lightbox tool you may find a new and modern designed lightbox which has several types and many configuration options. By picking up the view make sure it will satisfy all your needs and has the right functionality, as every view is different and unique

Summing up all the image gallery features:

* Unlimited Amount Of Galleries And Images

Photo Gallery allows adding unlimited galleries with photos/videos and showcase them on pages or/and posts. There is no limit in free version. The Photo Gallery plugin comes as very light plugin and you are free to add a lot of images to the gallery.

* Fully Responsive

Photo Gallery plugin is fully responsive and appears beautiful on mobile devices. You may enjoy scrolling through image gallery photos/videos and view them with a single click. 

The image gallery looks natural with every responsive theme and it is fully mobile friendly.


* Load More And Pagination

With Photo Gallery plugin also get a load more functionality for the gallery images and videos. The image gallery option allows you to either add pagination, so that you can click NEXT and PREVIOUS buttons when going through images or to use a load more button , so that the next collection of galleries would pop below.

* Title And Description

Photo Gallery allows users to add both titles and descriptions. Descriptions appear to be more detailed than the title. Besides you may also add a URL in each image gallery content.

* 8 Nicely Designed Image Gallery  Views

The most unique feature of this photo gallery plugin appears to be its collection of beautiful views. This plugin comes with 8 views , that meet all the modern website design requirements. The good news is that all these views are available in Lite version of the plugin.

* Hundreds Of Design Options

We constantly update the plugin and add new features and options in order to help users to expand the functionality of their photo gallery.

* YouTube and Vimeo Posts

Photo Gallery allows to showcase videos from YouTube and Vimeo too. This is a great idea to add some spice to your photo gallery with detailed video instructions, for example.

* Lightbox Popup With Many Options

This gallery plugin used has numerous lightbox options. This is really a nice and advanced tool for WordPress Photo Gallery users. Why? Because it has a lot of options and functionality to offer and some of them are available on this plugin only. The list is really big, starting from big collection of top-social sharing icons and buttons and finishing with watermark and advertising solutions.

* Friendly UI/UX Coding Standards

Our team helps users by bringing plugins that are being tested multiple times by different users. We test the plugins on different devices and programs. This image gallery plugin is meeting the most recent UX/UI standards for sure.

* Minimalistic backend design

Minimalist back-end and admin panel of photo gallery plugin appear to be very simple, in order to help users to find and navigate through options more easily.

So hurry up and give Origin Code gallery a try, as it’s made just for your needs.

== Screenshots ==

1. Demo Mosaic
2. Demo Masonry
3. Demo Justified
4. Demo Popup
5. Demo Thumbnails
6. Demo Lightbox Gallery
