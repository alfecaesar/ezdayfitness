<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

//todo: correct urls
class OriginCode_Photo_Gallery_WP_Admin_Assets
{

    /**
     * OriginCode_Photo_Gallery_WP_Admin_Assets constructor.
     */
    public function __construct()
    {
        add_action('admin_enqueue_scripts', array($this, 'admin_styles'));
        add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));
    }

    /**
     * @param $hook hook of current page
     */
    public function admin_styles($hook)
    {
        wp_enqueue_style("font-awesome", 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', true);
        if (in_array($hook, OriginCode_Photo_Gallery_WP()->admin->pages)) {
            wp_enqueue_style("gallery_admin_css", OriginCode_Photo_Gallery_WP()->plugin_url() . "/assets/style/admin.style.css", false);
            wp_enqueue_style("jquery_ui", OriginCode_Photo_Gallery_WP()->plugin_url() . '/assets/style/jquery-ui.1.10.4.smoothness.css', false);
            wp_enqueue_style("simple_slider_css", OriginCode_Photo_Gallery_WP()->plugin_url() . "/assets/style/simple-slider.css", false);
            wp_enqueue_style("free-banner", OriginCode_Photo_Gallery_WP()->plugin_url() . "/assets/style/head.css", false);

        }
        $edit_pages = array('post.php', 'post-new.php');
        if (in_array($hook, $edit_pages)) {
            wp_enqueue_style('shortcode-components', OriginCode_Photo_Gallery_WP()->plugin_url() . "/assets/style/shortcode-components.css");
        }
    }

    public function admin_scripts($hook)
    {

        if (in_array($hook, OriginCode_Photo_Gallery_WP()->admin->pages)) {
            wp_enqueue_media();
            wp_enqueue_script("gallery_admin_js", OriginCode_Photo_Gallery_WP()->plugin_url() . "/assets/js/admin.js", false);
            wp_enqueue_script("jquery-ui-core");
            wp_enqueue_script("simple_slider_js", OriginCode_Photo_Gallery_WP()->plugin_url() . '/assets/js/simple-slider.js', false);
            wp_enqueue_script('param_block2', OriginCode_Photo_Gallery_WP()->plugin_url() . "/assets/js/jscolor.js");
        }
        $edit_pages = array('post.php', 'post-new.php');
        if (in_array($hook, $edit_pages)) {
            wp_enqueue_script('shortcode-components', OriginCode_Photo_Gallery_WP()->plugin_url() . "/assets/js/shortcode-components.js", array('jquery'));
        }
    }


    public function localize_scripts()
    {

    }
}

new OriginCode_Photo_Gallery_WP_Admin_Assets();
