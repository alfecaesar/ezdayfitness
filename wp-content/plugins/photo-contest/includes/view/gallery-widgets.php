<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 * Class OriginCode_Photo_Gallery_WP_Widgets
 */
class OriginCode_Photo_Gallery_WP_Widgets {

	/**
	 * OriginCode_Photo_Gallery_WP_Widgets constructor.
	 */
	public function __construct() {
		add_action( 'widgets_init', array($this,'register_widget'));
	}

	/**
	 * Register Photo Gallery WP Widget
	 */
	public function register_widget(){
		register_widget( 'OriginCode_Photo_Gallery_WP_Widget' );
	}
}

new OriginCode_Photo_Gallery_WP_Widgets();
