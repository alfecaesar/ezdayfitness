<?php

/**
 * Class OriginCode_Photo_Gallery_WP_Lightbox_Settings
 */
class OriginCode_Photo_Gallery_WP_Lightbox_Settings extends WPDEV_Settings_API {

	public $plugin_id = 'origincode_photo_gallery_wp';

	public function __construct(){
		$config = array(
			'menu_slug' => 'origincode_photo_gallery_wp_lightbox_options',
			'parent_slug' => 'origincode_photo_gallery_wp_gallery',
			'page_title' => __( 'Lightbox Options', 'origincode-photo-gallery-wp' ),
			'title' => __('Photo Gallery WP Lightbox Options','origincode-photo-gallery-wp'),
			'menu_title'=> __( 'Lightbox Options', 'origincode-photo-gallery-wp' ),
		);
		$this->init();
		$this->init_sections();
		$this->init_controls();

		parent::__construct( $config);
		$this->add_css('free-banner',OriginCode_Photo_Gallery_WP()->plugin_url()."/assets/style/head.css");
	}

	/**
	 * Initialize user defined variables
	 */
	public function init(){
		$this->ph_lightbox_style_view = $this->get_option('ph_lightbox_style_view', 1);
		$this->ph_slide_animation_type = $this->get_option('ph_slide_animation_type', 1);
		$this->ph_lightbox_speed = $this->get_option('ph_lightbox_speed', 600);
		$this->ph_lightbox_slider_animation = $this->get_option('ph_lightbox_slider_animation', 'yes');
		$this->ph_lightbox_overlay_close = $this->get_option('ph_lightbox_overlay_close', 'yes');
		$this->ph_lightbox_loop = $this->get_option('ph_lightbox_loop', 'yes');
		$this->ph_lightbox_esc_key_close = $this->get_option('ph_lightbox_esc_key_close', 'yes');
		$this->ph_lightbox_keypress_navigation = $this->get_option('ph_lightbox_keypress_navigation', 'yes');
		$this->ph_lightbox_arrows = $this->get_option('ph_lightbox_arrows', 'yes');
		$this->ph_lightbox_download_image = $this->get_option('ph_lightbox_download_image', 'yes');
		$this->ph_lightbox_default_title = $this->get_option('ph_lightbox_default_title', 'Default Title');
		$this->ph_lightbox_slideshow_on = $this->get_option('ph_lightbox_slideshow_on', 'yes');
		$this->ph_lightbox_slideshow_auto = $this->get_option('ph_lightbox_slideshow_auto', 'no');
		$this->ph_lightbox_slideshow_speed = $this->get_option('ph_lightbox_slideshow_speed', 2500);
		$this->ph_lightbox_size_fix = $this->get_option('ph_lightbox_size_fix', 'yes');
		$this->ph_lightbox_social_on_off = $this->get_option('ph_lightbox_social_on_off', 'no');
		$this->ph_lightbox_social_facebook = $this->get_option('ph_lightbox_social_facebook', 'yes');
		$this->ph_lightbox_social_twitter = $this->get_option('ph_lightbox_social_twitter', 'yes');
		$this->ph_lightbox_social_google = $this->get_option('ph_lightbox_social_google', 'yes');
		$this->ph_lightbox_social_pinterest = $this->get_option('ph_lightbox_social_pinterest', 'yes');
		$this->ph_lightbox_social_linkedin = $this->get_option('ph_lightbox_social_linkedin', 'yes');
		$this->ph_lightbox_social_tumblr = $this->get_option('ph_lightbox_social_tumblr', 'yes');
		$this->ph_lightbox_social_reddit = $this->get_option('ph_lightbox_social_reddit', 'no');
		$this->ph_lightbox_social_buffer = $this->get_option('ph_lightbox_social_buffer', 'no');
		$this->ph_lightbox_social_digg = $this->get_option('ph_lightbox_social_digg', 'no');
		$this->ph_lightbox_social_vk = $this->get_option('ph_lightbox_social_vk', 'no');
		$this->ph_lightbox_social_yummly = $this->get_option('ph_lightbox_social_yummly', 'no');
	}

	public function init_sections(){
		$this->sections = array(
			'general' => array(
				'title' => __('Internationalization','origincode-photo-gallery-wp'),
				'disabled' => true,
				'disabled_link' => 'https://origincode.co/downloads/photo-gallery'
			),
			'slideshow' => array(
				'title' => __('Slideshow','origincode-photo-gallery-wp'),
				'disabled' => true,
				'disabled_link' => 'https://origincode.co/downloads/photo-gallery'
			),
			'dimensions' => array(
				'title' => __('Dimensions','origincode-photo-gallery-wp'),
				'disabled' => true,
				'disabled_link' => 'https://origincode.co/downloads/photo-gallery'
			),
			'social_buttons' => array(
				'title' => __('Social Buttons','origincode-photo-gallery-wp'),
				'disabled' => true,
				'disabled_link' => 'https://origincode.co/downloads/photo-gallery'
			)
		);
	}

    public function init_controls(){
        $this->controls = array(
            'ph_lightbox_social_on_off' => array(
                'section' => 'social_buttons',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_social_on_off,
                'label' => __('Social Buttons', 'origincode-photo-gallery-wp'),
                'help' => __('Check to activate social sharing buttons.', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_social_facebook' => array(
                'section' => 'social_buttons',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_social_facebook,
                'label' => __('Facebook', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_social_twitter' => array(
                'section' => 'social_buttons',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_social_twitter,
                'label' => __('Twitter', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_social_google' => array(
                'section' => 'social_buttons',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_social_google,
                'label' => __('Google Plus', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_social_pinterest' => array(
                'section' => 'social_buttons',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_social_pinterest,
                'label' => __('Pinterest', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_social_linkedin' => array(
                'section' => 'social_buttons',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_social_linkedin,
                'label' => __('Linkedin', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_social_tumblr' => array(
                'section' => 'social_buttons',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_social_tumblr,
                'label' => __('Tumblr', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_social_reddit' => array(
                'section' => 'social_buttons',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_social_reddit,
                'label' => __('Reddit', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_social_buffer' => array(
                'section' => 'social_buttons',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_social_buffer,
                'label' => __('Buffer', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_social_digg' => array(
                'section' => 'social_buttons',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_social_digg,
                'label' => __('Digg', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_social_vk' => array(
                'section' => 'social_buttons',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_social_vk,
                'label' => __('VK', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_social_yummly' => array(
                'section' => 'social_buttons',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_social_yummly,
                'label' => __('Yummly', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_style_view' => array(
                'section' => 'general',
                'type' => 'select',
                'default' => $this->ph_lightbox_style_view,
                'choices' => array (
                    4 => '4',
                    3 => '3',
                    2 => '2',
                    1 => '1',
                ),
                'label' => __('Lightbox style','origincode-photo-gallery-wp'),
                'help' => __('Choose the type of your popup', 'origincode-photo-gallery-wp')
            ),
            'ph_slide_animation_type' => array(
                'section' => 'general',
                'type' => 'select',
                'default' => $this->ph_slide_animation_type,
                'choices' => array (
                    8 => '9',
                    7 => '8',
                    6 => '7',
                    5 => '6',
                    4 => '5',
                    3 => '4',
                    2 => '3',
                    1 => '2',
                    0 => '1',
                ),
                'label' => __('Slide Animation Type','origincode-photo-gallery-wp'),
                'help' => __('Choose the slide animation type', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_speed' => array(
                'section' => 'general',
                'type' => 'number',
                'default' => $this->ph_lightbox_speed,
                'label' => __('Speed', 'origincode-photo-gallery-wp'),
                'help' => __('Set the speed of opening the popup in milliseconds..', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_slider_animation' => array(
                'section' => 'general',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_slider_animation,
                'label' => __('Slider Animation', 'origincode-photo-gallery-wp'),
                'help' => __('Choose whether to display the animation', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_overlay_close' => array(
                'section' => 'general',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_overlay_close,
                'label' => __('Overlay Close', 'origincode-photo-gallery-wp'),
                'help' => __('Choose to close the content by clicking on the overlay.', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_loop' => array(
                'section' => 'general',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_loop,
                'label' => __('Loop', 'origincode-photo-gallery-wp'),
                'help' => __('Loop content. If switched on give the ability to move from the last image to the first image while navigation..', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_esc_key_close' => array(
                'section' => 'general',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_esc_key_close,
                'label' => __('Esc Key close', 'origincode-photo-gallery-wp'),
                'help' => __('Choose to close the content by pressing esc.', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_keypress_navigation' => array(
                'section' => 'general',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_keypress_navigation,
                'label' => __('Keyboard navigation', 'origincode-photo-gallery-wp'),
                'help' => __('Set to change the images with keyboard buttons.', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_arrows' => array(
                'section' => 'general',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_arrows,
                'label' => __('Arrows', 'origincode-photo-gallery-wp'),
                'help' => __('Choose whether to display arrows', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_download_image' => array(
                'section' => 'general',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_download_image,
                'label' => __('Download', 'origincode-photo-gallery-wp'),
                'help' => __('Choose whether to let download image', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_default_title' => array(
                'section' => 'general',
                'type' => 'text',
                'default' => $this->ph_lightbox_default_title,
                'label' => __('Default Title', 'origincode-photo-gallery-wp'),
                'help' => __('Choose whether to display default title', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_slideshow_on' => array(
                'section' => 'slideshow',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_slideshow_on,
                'label' => __('Slideshow', 'origincode-photo-gallery-wp'),
                'help' => __('Select to enable slideshow.', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_slideshow_auto' => array(
                'section' => 'slideshow',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_slideshow_auto,
                'label' => __('Slideshow auto', 'origincode-photo-gallery-wp'),
                'help' => __('If true it works automatically', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_slideshow_speed' => array(
                'section' => 'slideshow',
                'type' => 'number',
                'default' => $this->ph_lightbox_slideshow_speed,
                'label' => __('Slideshow Speed', 'origincode-photo-gallery-wp'),
                'help' => __('Speed of slideshow in milliseconds', 'origincode-photo-gallery-wp')
            ),
            'ph_lightbox_size_fix' => array(
                'section' => 'dimensions',
                'type' => 'checkbox',
                'default' => $this->ph_lightbox_size_fix,
                'label' => __('Popup size fix', 'origincode-photo-gallery-wp'),
                'help' => __('Choose to fix the popup width and high.', 'origincode-photo-gallery-wp')
            ),
        );
    }
}
