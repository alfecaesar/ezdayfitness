<?php
$opt = get_option( 'rogan_opt' );

$navbar_layout = isset($opt['navbar_layout']) ? $opt['navbar_layout'] : 'boxed';
$menu_alignment = !empty($opt['menu_alignment']) ? $opt['menu_alignment'] : 'menu_right';

$is_menu_btn = !empty($opt['is_menu_btn']) ? $opt['is_menu_btn'] : '';
$eheader_for_shop = isset($opt['eheader_for_shop']) ? $opt['eheader_for_shop'] : '';
$is_shop_banner = isset($opt['is_shop_banner']) ? $opt['is_shop_banner'] : '';
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <!-- For IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- For Responsive Device -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php wp_head(); ?>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158748528-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-158748528-1');
        </script><!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158748528-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-158748528-1');
        </script>
    </head>
    <body <?php body_class(); ?>>
    <div class="main-page-wrapper">
        <?php
        $is_preloader = !empty($opt['is_preloader']) ? $opt['is_preloader'] : '';
        $preloader_style = !empty($opt['preloader_style']) ? $opt['preloader_style'] : 'text';
        if( $is_preloader == '1' ) {
            if ( defined( 'ELEMENTOR_VERSION' ) ) {
                if (\Elementor\Plugin::$instance->preview->is_preview_mode()) {
                    echo '';
                } else {
                    if( $preloader_style == 'text' ) {
                        get_template_part( 'template-parts/header_elements/pre', 'loader' );
                    } else {
                        ?>
                        <div id="preloader">
                            <div id="status"></div>
                        </div>
                        <?php
                    }
                }
            }
            else {
                get_template_part( 'template-parts/header_elements/pre', 'loader' );
            }
        }

        // Nav-bar part
        $page_header_style = function_exists( 'get_field' ) ? get_field( 'header_style' ) : '';
        if( $page_header_style && $page_header_style != 'default' ) {
            $header_style = $page_header_style;
        } else {
            $header_style = !empty($opt['header_style']) ? $opt['header_style'] : '4';
        }

        if ( $eheader_for_shop == '1' && class_exists( 'WooCommerce' ) ) {
            if ( is_shop() || is_singular( 'product' ) || is_checkout() || is_cart() || is_tax( 'product_cat' ) || is_tax( 'product_tag' ) ) {
                get_template_part( 'template-parts/header_elements/headers/header', '6' );
            } else {
                get_template_part( 'template-parts/header_elements/headers/header', $header_style);
            }
        } else {
            get_template_part( 'template-parts/header_elements/headers/header', $header_style);
        }

        $is_banner = '1';

        if( is_404() ) {
            $is_banner = '';
        }

        if( is_home() ) {
            $is_banner = '1';
        }

        if( is_singular( 'portfolio' ) || is_post_type_archive( 'portfolio' ) || is_singular( 'product' ) ) {
            $is_banner = '';
        }

        if ( is_page() || is_singular( 'job' ) ) {
            $is_banner = function_exists( 'get_field' ) ? get_field( 'is_banner' ) : '1';
            $is_banner = isset($is_banner) ? $is_banner : '1';
        }

        if ( class_exists( 'WooCommerce' ) && $is_shop_banner == '' ) {
            if ( is_shop() || is_singular( 'product' ) || is_checkout() || is_cart() || is_tax( 'product_cat' ) || is_tax( 'product_tag' ) ) {
                $is_banner = '';
            }
        }

        if( $is_banner == '1' ) {
            get_template_part( 'template-parts/header_elements/banner' );
        }