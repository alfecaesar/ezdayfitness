<?php
// Register Widget areas
add_action( 'widgets_init', function() {

    $opt = get_option( 'rogan_opt' );

    register_sidebar( array(
        'name'          => esc_html__( 'Primary Sidebar', 'rogan' ),
        'description'   => esc_html__( 'Place widgets in sidebar widgets area.', 'rogan' ),
        'id'            => 'sidebar_widgets',
        'before_widget' => '<div id="%1$s" class="single-block mb-80 main-menu-list %2$s">',
        'after_widget'  => '</div>',
        'before_title'  =>  '<h4 class="sidebar-title">',
        'after_title'   => '</h4> '
    ));

    if ( class_exists( 'WooCommerce' ) ) {
        register_sidebar( array (
            'name' => esc_html__( 'Shop Sidebar', 'rogan' ),
            'description' => esc_html__( 'Shop page widgets area.', 'rogan' ),
            'id' => 'shop_sidebar',
            'before_widget' => '<div id="%1$s" class="single-block mb-80 %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="sidebar-title">',
            'after_title' => '</h4> '
        ));
    }

    if ( class_exists( 'Rogan_core' ) ) {
        register_sidebar(array(
            'name' => esc_html__( 'Portfolio Sidebar', 'rogan' ),
            'description' => esc_html__( 'Portfolio archive page widgets area.', 'rogan' ),
            'id' => 'portfolio_widgets',
            'before_widget' => '<div id="%1$s" class="portfolio_widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3 class="sidebar-title">',
            'after_title' => '</h3> '
        ));
    }

    register_sidebar(array(
        'name'          => esc_html__( 'Footer widgets', 'rogan' ),
        'description'   => esc_html__( 'Add widgets here for Footer widgets area', 'rogan' ),
        'id'            => 'footer_widgets',
        'before_widget' => '<div id="%1$s" class="aos-init aos-animate widget col-lg-3 col-sm-6 col-12 %2$s" data-aos="fade-up">',
        'after_widget'  => '</div>',
        'before_title'  => '<h5 class="title">',
        'after_title'   => '</h5>'
    ));

});
