<?php

/**
 * Register Google fonts.
 *
 * @return string Google fonts URL for the theme.
 */
function rogan_fonts_url() {
    $fonts_url = '';
    $fonts     = array();
    $subsets   = '';
    /* Body font */
    if ( 'off' !== esc_html__( 'on', 'rogan' ) ) {
        $fonts[] = "Lato:100,300,900,700";
    }
    if ( 'off' !== esc_html__( 'on', 'rogan' ) ) {
        $fonts[] = "Playfair Display:700";
    }

    return $fonts_url;
}

function rogan_scripts() {
    $opt = get_option( 'rogan_opt' );
    $is_anim = isset($opt['is_anim']) ? $opt['is_anim'] : '1';
    wp_register_style( 'aos', esc_url(ROGAN_DIR_VEND . '/aos-next/dist/aos.css' ) );

    $dynamic_css = '';

    wp_enqueue_style( 'rogan-fonts', rogan_fonts_url(), array(), null);

    wp_enqueue_style( 'circular-std', esc_url(ROGAN_DIR_FONT.'/circular-std/css/circular-std.css' ) );

    wp_enqueue_style( 'bootstrap', esc_url(ROGAN_DIR_VEND.'/bootstrap/css/bootstrap.min.css' ) );

    wp_enqueue_style( 'rogan-menu', esc_url(ROGAN_DIR_VEND.'/mega-menu/assets/css/menu.css' ) );

    wp_enqueue_style( 'font-awesome', esc_url(ROGAN_DIR_FONT.'/font-awesome/css/font-awesome.min.css' ) );

    wp_enqueue_style( 'rogan-flaticon', esc_url(ROGAN_DIR_FONT.'/flaticon/flaticon.css' ) );

    if ( defined( 'ELEMENTOR_VERSION' ) ) {
        if (!\Elementor\Plugin::$instance->preview->is_preview_mode()) {
            if ( $is_anim == '1' ) {
                wp_enqueue_style( 'aos' );
            }
        }
    }

    wp_enqueue_style( 'rogan-wpd', esc_url(ROGAN_DIR_CSS.'/wpd-style.css' ) );

    wp_enqueue_style( 'rogan-main', esc_url(ROGAN_DIR_CSS . '/style.css' ) );

    wp_enqueue_style( 'rogan-default-widgets', esc_url(ROGAN_DIR_CSS . '/default-widgets.css' ) );

    if ( function_exists('has_block') ) {
        if (has_block('paragraph') || has_block('textColumns') || has_block('button') || has_block('image') || has_block('columns') || has_block('coverImage') || has_block('gallery') || has_block('pullquote') || has_block('quote')) {
            wp_enqueue_style('rogan-gutenberg', esc_url(ROGAN_DIR_CSS . '/rogan-gutenberg.css'));
        }
    }

    if ( class_exists( 'WooCommerce' ) ) {
        if ( is_shop() || is_singular( 'product' ) || is_checkout() || is_cart() || is_tax( 'product_cat' ) || is_tax( 'product_tag' ) ) {
            wp_enqueue_style( 'rogan-shop-global', esc_url(ROGAN_DIR_CSS.'/shop-global.css' ));
        }
    }

    wp_enqueue_style( 'rogan-root', get_stylesheet_uri());

    wp_enqueue_style( 'rogan-responsive', esc_url(ROGAN_DIR_CSS . '/responsive.css' ) );

    wp_enqueue_style( 'rogan-responsive2', esc_url(ROGAN_DIR_CSS . '/responsive-2.css' ) );

    if( function_exists( 'get_field' ) ) {
        /**
         * Banner Settings
         * @Background Colors
         */
        $banner_background_color = get_field( 'banner_background_color' );
        $banner_title_color = get_field( 'banner_title_color' );
        $banner_background_image = get_field( 'banner_background_image' );
        if( $banner_background_color ) {
            $dynamic_css .= "
            .solid-inner-banner {
                background: $banner_background_color;
            }";
        }
        if( $banner_title_color ) {
            $dynamic_css .= "
            .solid-inner-banner .page-title { color: $banner_title_color; } ";
        }
        if( $banner_background_image ) {
            $dynamic_css .= "
            .solid-inner-banner {
                background-image: url($banner_background_image);
                background-size: cover;
                background-repeat: no-repeat;
            }";
        }

        /**
         * Page Settings
         *
         * @Colors
         * @Padding
         */
        $colors = get_field( 'colors' );
        $page_bg_color = !empty($colors['background_color']) ? "background: {$colors['background_color']};" : '';
        $page_font_color = !empty($colors['font_color']) ? "color: {$colors['font_color']};" : '';
        if($page_bg_color || $page_font_color) {
            $dynamic_css .= "
                body.page-id-" . get_the_ID() . " {
                    $page_bg_color
                    $page_font_color
                }
            ";
        }

        $page_padding = get_field( 'page_content_padding' );
        // Page Padding top
        if (!empty($page_padding['padding_top']) || $page_padding['padding_top'] == '0' ) {
            $dynamic_css .= "
            .elementor-template-full-width .elementor.elementor-" . get_the_ID() . ",
            .sec_pad.page_wrapper {
                padding-top: {$page_padding['padding_top']}px;
            }";
        }
        // Page Padding bottom
        if (!empty($page_padding['padding_bottom']) || $page_padding['padding_bottom'] == '0' ) {
            $dynamic_css .= "
            .elementor-template-full-width .elementor.elementor-" . get_the_ID() . ",
            .sec_pad.page_wrapper {
                padding-bottom: {$page_padding['padding_bottom']}px;
            }";
        }

        /**
         * Header Settings
         * @menu_item_color
         * @menu_item_active_color
         */
        $menu_item_color = get_field( 'menu_item_color' );
        if ( $menu_item_color ) {
            $dynamic_css .= "
                .theme-menu-one .navbar-nav .nav-item .nav-link {color: $menu_item_color;}
            ";
        }

        $menu_item_active_color = get_field( 'menu_item_active_color' );
        if ( $menu_item_active_color ) {
            $dynamic_css .= "
            .navbar .dropdown-menu .nav-item.active .dropdown-item {
                color: $menu_item_active_color;
            };";
        }


        /**
         * Portfolio CSS
         *
         * @Portfolio Creative Featured Image
         */
        $layout = (get_field( 'layout' ) != 'default' ) ? get_field( 'layout' ) : '';
        if( $layout == 'creative' ) {
            if( has_post_thumbnail() ) {
                $dynamic_css .= "
                .project-details .pd-banner {
                    background-image: url(".get_the_post_thumbnail_url().");
                }";
            }
        }

    }

    if ( class_exists( 'ReduxFrameworkPlugin' ) ) {

        //404 background image - style 1
        $error_bg_image_src = isset($opt['error_page_bg_one']['url']) ? $opt['error_page_bg_one']['url'] : '';
        if (!empty($error_bg_image_src)) {
            $dynamic_css .= "
            .error-creative-content {
                background: url(". esc_url($error_bg_image_src) .") no-repeat center;
            }";
        }

        // 404 background image - style 2
        $error_page_bg_two = isset($opt['error_page_bg_two']['url']) ? $opt['error_page_bg_two']['url'] : '';
        if (!empty($error_page_bg_two)) {
            $dynamic_css .= "
            .error-content:before {
                background: url(". esc_url($error_page_bg_two) .") no-repeat;
            }";
        }

        // Menu Item Font Colors
        if ( !empty($opt['menu_font_color']['regular']) ) {
            $dynamic_css .= "
                .navbar-nav .nav-item .nav-link, .theme-main-menu .navbar-nav .nav-item .nav-link,
                .theme-menu-one.d-align-item.color-white .navbar-nav .nav-item .nav-link {color: {$opt['menu_font_color']['regular']}}
            ";
        }

        if ( !empty($opt['menu_font_color']['hover']) ) {
            $dynamic_css .= "
                .navbar-nav .nav-item:hover .nav-link,
                .theme-main-menu .navbar-nav .nav-item:hover .nav-link,
                .theme-menu-one.d-align-item.app-menu .navbar .dropdown-item:hover,
                .theme-menu-one.d-align-item .navbar .dropdown-submenu.dropdown:hover>.dropdown-item,
                .theme-menu-one.d-align-item .navbar .dropdown-item:hover, .theme-menu-one.d-align-item .navbar .dropdown-item:focus,
                .theme-menu-one.d-align-item .navbar-nav .nav-item:hover .nav-link {color: {$opt['menu_font_color']['hover']}}
            ";
        }

        if ( !empty($opt['menu_font_color']['active']) ) {
            $dynamic_css .= "
                .navbar-nav .nav-item.active .nav-link,
                .navbar-nav .nav-item.current-menu-ancestor .nav-link,
                .theme-menu-one.d-align-item.color-white .navbar-nav .nav-item.current-menu-ancestor .nav-link,
                .theme-menu-one .navbar-nav .nav-item.active .nav-link, .theme-menu-one.d-align-item .navbar-nav .nav-item.active .nav-link,
                .theme-menu-one.fixed .navbar-nav .nav-item:hover .nav-link, .navbar .dropdown-menu .nav-item.active .dropdown-item, 
                .navbar .dropdown-menu .menu-item.active > a, .theme-menu-one .navbar-nav .nav-item.current-menu-ancestor .nav-link {color: {$opt['menu_font_color']['active']}}
            ";
        }
        // Menu Sticky Color
        if ( !empty($opt['sticky_menu_font_color']['regular']) ) {
            $dynamic_css .= "
                .theme-menu-one.fixed .navbar-nav .nav-item.current-menu-ancestor .nav-link,
                .theme-menu-two.fixed .navbar-nav .nav-item .nav-link,
                .theme-menu-three .navbar-nav .nav-item .nav-link,
                .theme-menu-one.fixed .navbar-nav .nav-item .nav-link,
                .theme-main-menu.fixed .navbar-nav .nav-item .nav-link,
                .theme-menu-one.d-align-item.color-white .navbar-nav .nav-item .nav-link,
                .eCommerce-side-menu .main-menu-list>ul>li>a {
                    color: {$opt['sticky_menu_font_color']['regular']};
                }
            ";
        }

        if ( !empty($opt['sticky_menu_font_color']['hover']) ) {
            $dynamic_css .= "
                .theme-main-menu.fixed .navbar-nav .nav-item:hover .nav-link,
                .theme-menu-one.fixed .navbar-nav .nav-item:hover .nav-link,
                .eCommerce-side-menu .main-menu-list>ul>li>a:hover {
                    color: {$opt['sticky_menu_font_color']['hover']};
                };";
        }

        if ( !empty($opt['sticky_menu_font_color']['active']) ) {
            $dynamic_css .= "
                .theme-main-menu.fixed .navbar-nav .nav-item.active .nav-link,
                .theme-menu-one.fixed .navbar-nav .nav-item.active .nav-link {
                    color: {$opt['sticky_menu_font_color']['active']};
                }";
        }

    }
    wp_add_inline_style( 'rogan-root', $dynamic_css );


    /**
     * Scripts
     */
    wp_register_script( 'aos', esc_url(ROGAN_DIR_VEND . '/aos-next/dist/aos.js' ), array( 'jquery' ), '1.0', true );
    $dynamic_js = '';

    wp_enqueue_script( 'popper', esc_url(ROGAN_DIR_VEND.'/popper.min.js' ), array( 'jquery' ), '1.0', true );

    wp_enqueue_script( 'bootstrap', esc_url(ROGAN_DIR_VEND.'/bootstrap/js/bootstrap.min.js' ), array( 'jquery', 'popper' ), '4.1.2', true );

    if ( defined( 'ELEMENTOR_VERSION' ) ) {
        if (!\Elementor\Plugin::$instance->preview->is_preview_mode()) {
            if ( $is_anim == '1' ) {
                wp_enqueue_script( 'aos' );
            }
        }
    }

    if ( is_singular( 'product' ) ) {
        wp_enqueue_script( 'owl-carousel', ROGAN_DIR_VEND.'/owl-carousel/owl.carousel.min.js', 'jquery', '3.5', true);
    }

    wp_enqueue_script( 'rogan-mega-menu', esc_url(ROGAN_DIR_VEND.'/mega-menu/assets/js/custom.js' ), array( 'jquery' ), '1.0', true );

    if ( is_page_template( 'archive-portfolio-creative.php' )) {
        wp_enqueue_script( 'imagesloaded' );
        wp_enqueue_script( 'charming', esc_url(ROGAN_DIR_VEND . '/cr-project/charming.min.js' ), array( 'jquery' ), '1.0', true);
        wp_enqueue_script( 'TweenMax', esc_url(ROGAN_DIR_VEND . '/cr-project/TweenMax.min.js' ), array( 'jquery' ), '1.20.3', true);
        wp_enqueue_script( 'rogan-tweenmax-demo', esc_url(ROGAN_DIR_VEND . '/cr-project/demo.js' ), array( 'jquery' ), '1.0.0', true);
    }

    wp_enqueue_script( 'rogan-custom-wp', esc_url(ROGAN_DIR_JS.'/custom-wp.js' ), array( 'jquery' ), '1.0', true );

    wp_add_inline_script( 'rogan-custom-wp', $dynamic_js);

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'rogan_scripts', 99);

add_action( 'admin_enqueue_scripts', function() {
    wp_enqueue_style( 'rogan-admin', esc_url(ROGAN_DIR_CSS.'/rogan-admin.css' ) );
});


// Gutenberg editor assets
add_action( 'enqueue_block_assets', function() {
    wp_enqueue_style( 'rogan-editor-fonts', rogan_fonts_url(), array(), null );
    wp_enqueue_style( 'rogan-editor-circular-std', esc_url(ROGAN_DIR_FONT.'/circular-std/css/circular-std.css' ) );
});