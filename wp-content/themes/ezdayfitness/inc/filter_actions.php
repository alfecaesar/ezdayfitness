<?php
// add category nick names in body and post class
function rogan_post_class( $classes ) {
    global $post;
    if( !has_post_thumbnail() ) {
        $classes[] = 'blog-text-style';
    }

    return $classes;
}
add_filter( 'post_class', 'rogan_post_class' );


// Body classes
add_filter( 'body_class', function($classes) {
    $opt = get_option( 'rogan_opt' );
    if ( !is_user_logged_in() ) {
        $classes[] = 'not_logged_in';
    }
    if ( is_404() ) {
        $classes[] = 'page-404';
    }
    if ( is_single() && !comments_open() ) {
        $classes[] = 'comment_closed';
    }
    if ( !is_active_sidebar( 'footer_widgets' ) ) {
        $classes[] = 'no_footer_widgets';
    }
    if ( empty($opt['main_logo']['url']) ) {
        $classes[] = 'text_logo';
    }

    $classes[] = 'no-scroll-y';

    return $classes;
});


// filter to replace class on reply link
add_filter( 'comment_reply_link', function($class){
    $class = str_replace("class='comment-reply-link", "class='comment_reply", $class);
    return $class;
});


/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function rogan_pingback_header() {
    if ( is_singular() && pings_open() ) {
        echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
    }
}
add_action( 'wp_head', 'rogan_pingback_header' );


// Move the comment field to bottom
add_filter( 'comment_form_fields', function ( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
});


// Remove WordPress admin bar default CSS
add_action( 'get_header', function() {
    remove_action( 'wp_head', '_admin_bar_bump_cb' );
});


// Add class to Next posts navigation
add_filter( 'next_posts_link_attributes', function() {
    return 'class="theme-pager next"';
});


// Add class to Previous posts navigation
add_filter( 'previous_posts_link_attributes', function() {
    return 'class="theme-pager prev"';
});


// filter to replace class on reply link
add_filter( 'comment_reply_link', function($class){
    $class = str_replace("class='comment_reply", "class='reply", $class);
    return $class;
});


// Comment form submit button
add_filter( 'comment_form_submit_button', function( $submit_button, $args ) {
    // Override the submit button HTML:
    $button = '<button name="%1$s" type="submit" id="%2$s" class="%3$s"> %4$s </button>';
    return sprintf(
        $button,
        esc_attr( $args['name_submit'] ),
        esc_attr( $args['id_submit'] ),
        esc_attr( $args['class_submit'] ),
        esc_attr( $args['label_submit'] )
    );
}, 10, 2 );


// Tag Cloud List
add_filter( 'widget_tag_cloud_args', 'rogan_tag_cloud_widget_parameters' );
function rogan_tag_cloud_widget_parameters() {
    $args = array(
        'smallest' => 15,
        'largest' => 15,
        'unit' => 'px',
        'number' => 10,
        'format' => 'list',
        'separator' => "\n",
        'orderby' => 'name',
        'order' => 'ASC',
        'exclude' => '',
        'include' => '',
        'link' => 'view',
        'post_type' => '',
        'echo' => false
    );
    return $args;
}


// Wrap up the category count in span tag
add_filter( 'wp_list_categories', function($links) {
    $links = str_replace( '</a> ( ', '<span>( ', $links);
    $links = str_replace( ' )', ' )</span> </a>', $links);
    return $links;
});


// Wrap up the category count in span tag
add_filter( 'get_archives_link', function($links) {
    $links = str_replace( '</a>&nbsp;( ', '<span>( ', $links);
    $links = str_replace( ' )', ' )</span> </a>', $links);
    return $links;
});


// Remove auto p from Contact Form 7 shortcode output
add_filter( 'wpcf7_autop_or_not', function() {
    return false;
});