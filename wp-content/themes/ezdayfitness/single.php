<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package rogan
 */

get_header();
$blog_column = is_active_sidebar( 'sidebar_widgets' ) ? '8' : '12';
?>
    <div class="our-blog blog-details pt-150 mb-200">
        <div class="container">
            <div class="row">
                <div class="col-lg-<?php echo esc_attr($blog_column) ?>">
                    <div class="single-blog-post">
                        <?php
                        while ( have_posts() ) : the_post();
                            get_template_part( 'template-parts/contents/content', 'single' );
                        endwhile;
                        ?>

                        <?php
                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;
                        ?>

                    </div> <!-- /.single-blog-post -->
                </div> <!-- /.col- -->

                <?php get_sidebar(); ?>

            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>

<?php
get_footer();