<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rogan
 */

if ( ! is_active_sidebar( 'portfolio_widgets' ) ) {
    return;
}
?>

<div class="col-lg-3 col-md-6 col-sm-8 order-lg-first">
    <div class="gallery-sidebar">
        <?php dynamic_sidebar( 'portfolio_widgets' ); ?>
    </div>
</div>