<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package rogan
 */

get_header();
$blog_column = is_active_sidebar( 'sidebar_widgets' ) ? '8' : '12';
?>
    <div class="our-blog blog-default pt-150 mb-200">
        <div class="container">
            <div class="row">
                <div class="col-lg-<?php echo esc_attr($blog_column) ?>">
                    <?php
                    if ( have_posts() ) {
                        while (have_posts()) : the_post();
                            if (has_post_format( 'video' )) {
                                wp_enqueue_style( 'fancybox' );
                                wp_enqueue_script( 'fancybox' );
                            }
                            get_template_part( 'template-parts/contents/content', get_post_format());
                        endwhile;
                    } else {
                        get_template_part( 'template-parts/contents/content', 'none' );
                    }
                    ?>

                    <?php if ( is_paged() ) : ?>
                        <div class="pd-footer d-flex justify-content-between align-items-center pt-50">
                            <?php
                            previous_posts_link( '<span class="flaticon-back"></span> &nbsp;&nbsp; '.esc_html__( 'Previous', 'rogan' ) );
                            next_posts_link( esc_html__( 'Next', 'rogan' ). '&nbsp;&nbsp;<span class="flaticon-next"></span>' );
                            ?>
                        </div>
                    <?php endif; ?>

                </div> <!-- /.col- -->

                <?php get_sidebar(); ?>

            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>

<?php
get_footer();