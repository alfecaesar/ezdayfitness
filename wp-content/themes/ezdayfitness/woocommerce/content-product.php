<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
    return;
}
?>

<div <?php post_class( 'col-xl-4 col-lg-6 col-md-4 col-sm-6' ); ?>>
    <div class="single-product-case">
        <div class="img-holder">
            <a href="<?php the_permalink() ?>">
                <?php woocommerce_template_loop_product_thumbnail(); ?>
            </a>
        </div>
        <div class="info">
            <a href="<?php the_permalink() ?>" class="name"> <?php the_title() ?> </a>
            <div class="price"> <?php woocommerce_template_loop_price(); ?> </div>
            <?php woocommerce_template_loop_add_to_cart() ?>
        </div>
    </div> <!-- /.single-product-case -->
</div> <!-- /.col- -->