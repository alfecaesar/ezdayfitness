<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $related_products ) : ?>


    <div class="realated-product product-showcase">

        <h2 class="title"><?php esc_html_e( 'Related products', 'rogan' ); ?></h2>
        <div class="related-product-slider">

            <?php foreach ( $related_products as $related_product ) : ?>
                <?php
                $post_object = get_post( $related_product->get_id() );
                setup_postdata( $GLOBALS['post'] =& $post_object ); ?>
                <div class="item">
                    <div class="single-product-case">
                        <div class="img-holder">
                            <a href="<?php the_permalink() ?>">
                                <?php woocommerce_template_loop_product_thumbnail(); ?>
                            </a>
                        </div>
                        <div class="info">
                            <a href="<?php the_permalink() ?>" class="name"> <?php the_title() ?> </a>
                            <div class="price"> <?php woocommerce_template_loop_price(); ?> </div>
                            <?php woocommerce_template_loop_add_to_cart() ?>
                        </div>
                    </div> <!-- /.single-product-case -->
                </div>
            <?php endforeach; ?>
        </div>
    </div>

<?php endif;

wp_reset_postdata();
