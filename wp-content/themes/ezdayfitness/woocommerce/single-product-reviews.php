<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product;

if ( ! comments_open() ) {
	return;
}

?>
<div id="reviews" class="woocommerce-Reviews">
	<div id="comments">
		<h2 class="woocommerce-Reviews-title">
			<?php
			$count = $product->get_review_count();
			if ( $count && wc_review_ratings_enabled() ) {
				/* translators: 1: reviews count 2: product name */
				$reviews_title = sprintf( esc_html( _n( '%1$s review for %2$s', '%1$s reviews for %2$s', $count, 'rogan' ) ), esc_html( $count ), '<span>' . get_the_title() . '</span>' );
				echo apply_filters( 'woocommerce_reviews_title', $reviews_title, $count, $product ); // WPCS: XSS ok.
			} else {
				esc_html_e( 'Reviews', 'rogan' );
			}
			?>
		</h2>

		<?php if ( have_comments() ) : ?>
			<div class="user-comment-area">
				<?php
                wp_list_comments(
                    apply_filters(
                        'woocommerce_product_review_list_args',
                        array( 'callback' => 'woocommerce_comments', 'style' => 'div' )
                    )
                );
                ?>
			</div>

			<?php
			if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
				echo '<nav class="woocommerce-pagination">';
				paginate_comments_links(
					apply_filters(
						'woocommerce_comment_pagination_args',
						array(
							'prev_text' => '&larr;',
							'next_text' => '&rarr;',
							'type'      => 'list',
						)
					)
				);
				echo '</nav>';
			endif;
			?>
		<?php else : ?>
			<p class="woocommerce-noreviews"><?php esc_html_e( 'There are no reviews yet.', 'rogan' ); ?></p>
		<?php endif; ?>
	</div>

	<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) : ?>
		<div class="comment-form-area" id="review_form_wrapper">
				<?php
				$commenter = wp_get_current_commenter();
                $aria_req       = ( $req ? " aria-required='true'" : '' );
                $fields =  array(
                    'author' => '<div class="row"> <div class="col-md-6"> <input type="text" name="author" id="name" value="'.esc_attr($commenter['comment_author']).'" placeholder="'.esc_attr__( 'Name *', 'rogan' ).'" '.$aria_req.'> </div>',
                    'email'	=> '<div class="col-md-6"> <input type="email" name="email" id="email" value="'.esc_attr($commenter['comment_author_email']).'" placeholder="'.esc_attr__( 'Email *', 'rogan' ).'" '.$aria_req.'> </div> </div>',
                );
				$comment_form = array(
					/* translators: %s is product title */
					'title_reply'         => have_comments() ? __( 'Add a review', 'rogan' ) : sprintf( __( 'Be the first to review &ldquo;%s&rdquo;', 'rogan' ), get_the_title() ),
					/* translators: %s is product title */
					'title_reply_to'      => __( 'Leave a Reply to %s', 'rogan' ),
					'title_reply_before'  => '<span id="reply-title" class="comment-reply-title">',
					'title_reply_after'   => '</span>',
					'comment_notes_after' => '',
                    'fields'                => apply_filters( 'comment_form_default_fields', $fields ),
					'label_submit'        => __( 'Submit', 'rogan' ),
					'logged_in_as'        => '',
					'comment_field'       => '',
                    'class_submit'          => 'theme-button-two',
				);

				$account_page_url = wc_get_page_permalink( 'myaccount' );
				if ( $account_page_url ) {
					/* translators: %s opening and closing link tags respectively */
					$comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( esc_html__( 'You must be %1$slogged in%2$s to post a review.', 'rogan' ), '<a href="' . esc_url( $account_page_url ) . '">', '</a>' ) . '</p>';
				}

				if ( wc_review_ratings_enabled() ) {
					$comment_form['comment_field'] = '<div class="comment-form-rating"><label for="rating">' . esc_html__( 'Your rating', 'rogan' ) . '</label><select name="rating" id="rating" required>
						<option value="">' . esc_html__( 'Rate&hellip;', 'rogan' ) . '</option>
						<option value="5">' . esc_html__( 'Perfect', 'rogan' ) . '</option>
						<option value="4">' . esc_html__( 'Good', 'rogan' ) . '</option>
						<option value="3">' . esc_html__( 'Average', 'rogan' ) . '</option>
						<option value="2">' . esc_html__( 'Not that bad', 'rogan' ) . '</option>
						<option value="1">' . esc_html__( 'Very poor', 'rogan' ) . '</option>
					</select></div>';
				}

				$comment_form['comment_field'] .= '<p class="comment-form-comment"><label for="comment">' . esc_html__( 'Your review', 'rogan' ) . '&nbsp;<span class="required">*</span></label><textarea id="comment" name="comment" cols="45" rows="8" required></textarea></p>';

				comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				?>
		</div>
	<?php else : ?>
		<p class="woocommerce-verification-required"><?php esc_html_e( 'Only logged in customers who have purchased this product may leave a review.', 'rogan' ); ?></p>
	<?php endif; ?>

	<div class="clear"></div>
</div>
