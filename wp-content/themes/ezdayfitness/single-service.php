<?php
/**
 * The template for displaying all services.
 *
 * @package rogan
 */

get_header();

while ( have_posts() ) : the_post();
    ?>
    <div class="blog-details pt-140 pb-140">
        <div class="container">
            <?php
            the_content();
            wp_link_pages( array (
                'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'rogan' ) . '</span>',
                'after'       => '</div>',
                'link_before' => '<span>',
                'link_after'  => '</span>',
                'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'rogan' ) . ' </span>%',
                'separator'   => '<span class="screen-reader-text">, </span>',
            ));
            ?>
        </div>
    </div>
<?php
endwhile; // End of the loop.


get_footer();
