<?php
if ( ! is_active_sidebar( 'shop_sidebar' ) ) {
    return;
}
?>

<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8 col-12 order-lg-first">
    <div class="theme-sidebar-widget shop-sidebar">
        <?php dynamic_sidebar( 'shop_sidebar' ) ?>
    </div>
</div>