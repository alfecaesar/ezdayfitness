<?php
$opt = get_option( 'rogan_opt' );
$share_options = !empty($opt['share_options']) ? $opt['share_options'] : '';
$portfolio_images = function_exists( 'get_field' ) ? get_field( 'portfolio_images' ) : '';
?>

<?php get_template_part( 'template-parts/project-layouts/banner', 'business' ) ?>

<div class="project-details project-details-business pt-150 pb-200">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-7">
                <div class="pd-img-box">
                    <?php the_post_thumbnail( 'full' ) ?>
                </div>
                <?php
                if( $portfolio_images ) {
                    foreach ($portfolio_images as $portfolio_image) {
                        echo '<div class="pd-img-box">';
                        echo wp_get_attachment_image($portfolio_image['ID'], 'full' );
                        echo '</div>';
                    }
                }
                ?>
            </div> <!-- /.col- -->
            <div class="col-lg-4 col-md-5">
                <div class="pd-body">
                    <?php the_content() ?>
                    <?php
                    // check if the repeater field has rows of data
                    if ( have_rows( 'portfolio_attributes' ) ):
                    ?>
                    <div class="project-info">
                        <h3 class="title"> <?php esc_html_e( 'Project Details', 'rogan' ) ?> </h3>
                        <ul>
                            <?php while (have_rows( 'portfolio_attributes' )) : the_row(); ?>
                            <li>
                                <h6 class="list-title"> <?php echo esc_html(get_sub_field( 'attribute_title' )); ?> </h6>
                                <span class="date"> <?php echo esc_html(get_sub_field( 'attribute_value' )); ?> </span>
                            </li>
                            <?php endwhile; ?>
                        </ul>
                    </div> <!-- /.project-info -->
                    <?php endif; ?>
                    <ul class="share-icon">
                        <?php rogan_portfolio_social_share() ?>
                    </ul>
                </div> <!-- /.pd-body -->
            </div> <!-- /.col- -->
        </div> <!-- /.row -->
        <div class="pd-footer d-flex justify-content-between align-items-center pt-90">
            <?php
            $prev_post = get_previous_post();
            if ($prev_post) : ?>
                <a href="<?php echo get_permalink($prev_post->ID) ?>" class="theme-pager prev">
                    <span class="flaticon-back"></span>
                    <?php esc_html_e( 'Prev Project', 'rogan' ) ?>
                </a>
            <?php endif; ?>
            <?php
            $next_post = get_next_post();
            if ($next_post) : ?>
                <a href="<?php echo get_permalink($next_post->ID) ?>" class="theme-pager next">
                    <?php esc_html_e( 'Next Project', 'rogan' ) ?>
                    <span class="flaticon-next"></span>
                </a>
            <?php endif; ?>
        </div> <!-- /.pd-footer -->
    </div> <!-- /.container -->
</div>