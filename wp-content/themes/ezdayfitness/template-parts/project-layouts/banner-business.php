<?php
$opt = get_option( 'rogan_opt' );
$banner_title = function_exists( 'get_field' ) ? get_field( 'banner_title' ) : '';

    ?>
    <div class="solid-inner-banner">
        <h2 class="page-title"> <?php the_title() ?> </h2>
        <ul class="page-breadcrumbs">
            <li><a href="<?php echo esc_url(home_url( '/' )) ?>"> <?php esc_html_e( 'Home', 'rogan' ) ?> </a></li>
            <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
            <li> <?php the_title() ?> </li>
        </ul>
    </div>
    <?php
