<div class="project-details project-details-creative pb-200">

    <div class="pd-banner">
        <div class="container">
            <div class="container-inner">
                <h2 class="project-title-one"> <?php the_title() ?> </h2>
            </div>
        </div>
    </div> <!-- /.pd-banner -->
    <div class="container">
        <div class="container-wrapper">
            <div class="pd-body">
                <div class="row">
                    <div class="col-lg-8 order-lg-last">
                        <?php the_content() ?>
                    </div>
                    <?php
                    // check if the repeater field has rows of data
                    if (have_rows( 'portfolio_attributes' )):
                    ?>
                    <div class="col-lg-4 order-lg-first">
                        <div class="project-info">
                            <ul>
                                <?php
                                while (have_rows( 'portfolio_attributes' )) : the_row();
                                    ?>
                                    <li>
                                        <h6 class="list-title"> <?php echo esc_html(get_sub_field( 'attribute_title' )); ?> </h6>
                                        <span class="date"> <?php echo esc_html(get_sub_field( 'attribute_value' )); ?> </span>
                                    </li>
                                    <?php
                                endwhile;
                                ?>
                            </ul>
                        </div> <!-- /.project-info -->
                    </div>
                    <?php endif; ?>
                </div> <!-- /.row -->
            </div> <!-- /.pd-body -->
            <div class="pd-footer d-md-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-between">
                    <?php
                    $prev_post = get_previous_post();
                    if ($prev_post) : ?>
                        <a href="<?php echo get_permalink($prev_post->ID) ?>" class="theme-pager prev">
                            <span class="flaticon-back"></span>
                            <?php esc_html_e( 'Previous', 'rogan' ) ?>
                        </a>
                    <?php endif; ?>
                    <?php
                    $next_post = get_next_post();
                    if ($next_post) : ?>
                        <a href="<?php echo get_permalink($next_post->ID) ?>" class="theme-pager next">
                            <?php esc_html_e( 'Next', 'rogan' ) ?>
                            <span class="flaticon-next"></span>
                        </a>
                    <?php endif; ?>
                </div>
                <ul class="share-icon">
                    <?php rogan_portfolio_social_share() ?>
                </ul>
            </div> <!-- /.pd-footer -->
        </div> <!-- /.container-wrapper -->
    </div> <!-- /.container -->
</div>