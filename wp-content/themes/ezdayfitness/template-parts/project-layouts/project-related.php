<?php
$opt = get_option( 'rogan_opt' );
$cats = get_the_terms(get_the_ID(), 'portfolio_cat' );
$cat_ids = wp_list_pluck($cats,'term_id' );
$related_post_count = !empty($opt['portfolio_related_posts_count']) ? $opt['portfolio_related_posts_count'] : 3;
$posts = new WP_Query( array(
    'post_type' => 'portfolio',
    'tax_query' => array(
        array(
            'taxonomy' => 'portfolio_cat',
            'field' => 'id',
            'terms' => $cat_ids,
            'operator'=> 'IN' //Or 'AND' or 'NOT IN'
        )),
    'posts_per_page' => $related_post_count,
    'ignore_sticky_posts' => 1,
    'orderby' => 'rand',
    'post__not_in' => array($post->ID)
));
wp_enqueue_script( 'owl-carousel' );
wp_enqueue_style( 'owl-carousel' );
wp_enqueue_style( 'owl-theme' );
wp_enqueue_script( 'fancybox' );
wp_enqueue_style( 'fancybox' );
?>

<div class="related-project our-project">
    <div class="container">
        <?php if( !empty($opt['related_portfolio_title']) ) : ?>
            <h3 class="sec-title"> <?php echo esc_html( $opt['related_portfolio_title'] ); ?> </h3>
        <?php endif; ?>
        <div class="related-product-slider">
            <?php
            $i = 0;
            while ( $posts->have_posts() ) : $posts->the_post();
                $portfolio_title_word_limit = !empty($opt['portfolio_title_word_limit']) ? $opt['portfolio_title_word_limit'] : 2;
                $title = wp_trim_words(get_the_title(), $portfolio_title_word_limit, '' );
                $title_first_part = preg_replace( '/\W\w+\s*(\W*)$/', '$1', $title);
                $title_pieces = explode( ' ', $title);
                $title_last_word = array_pop($title_pieces);
                $main_title = $title_first_part . " <span>$title_last_word</span>";

                ?>
                <div <?php post_class( 'item' ) ?>>
                    <div class="project-item">
                        <div class="img-box">
                            <?php the_post_thumbnail( 'rogan_370x510' ) ?>
                        </div>
                        <div class="hover-valina">
                            <div>
                                <h4 class="title" title="<?php the_title_attribute(); ?>">
                                    <a href="<?php the_permalink() ?>"> <?php echo wp_kses_post($main_title) ?> </a>
                                </h4>
                                <?php
                                $i = 0;
                                while ( have_rows( 'key_features' ) ) : the_row();
                                    if ( $i == 1 ) {
                                        break;
                                    }
                                    echo wpautop( get_sub_field( 'feature_title' ) );
                                    ++$i;
                                endwhile;
                                ?>
                                <a href="<?php the_post_thumbnail_url() ?>" class="zoom fancybox" data-fancybox="gallery">
                                    <img src="<?php echo esc_url(ROGAN_DIR_IMG) ?>/icon/zoom-in.svg" alt="<?php esc_attr_e( 'Zoom Icon', 'rogan' ) ?>" class="svg">
                                </a>
                            </div>
                        </div> <!-- /.hover-valina -->
                    </div> <!-- /.project-item -->
                </div> <!-- /.item -->
                <?php
            endwhile;
            wp_reset_postdata();
            ?>
        </div> <!-- /.related-product-slider -->
    </div> <!-- /.container -->
</div>