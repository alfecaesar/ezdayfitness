<?php
$opt = get_option( 'rogan_opt' );
$share_options = !empty($opt['share_options']) ? $opt['share_options'] : '';
?>
<?php get_template_part( 'template-parts/project-layouts/banner' ) ?>
<div class="project-details project-details-agency pt-150 pb-200">
    <div class="container">
        <div class="pd-header d-md-flex justify-content-between align-items-end">
            <h2 class="project-title-one"> <?php the_title(); ?> </h2>
            <?php if( $share_options == '1' ) : ?>
                <ul class="share-icon">
                    <?php rogan_portfolio_social_share() ?>
                </ul>
            <?php endif; ?>
        </div> <!-- /.pd-header -->
        <div class="pd-img-box">
            <?php the_post_thumbnail( 'full' ) ?>
        </div>
        <div class="pd-body">
            <div class="row">
                <div class="col-md-<?php echo ( have_rows( 'portfolio_attributes' ) ) ? '8' : '12'; ?>">
                    <?php the_content() ?>
                </div> <!-- /.col- -->

                <?php
                // check if the repeater field has rows of data
                if ( have_rows( 'portfolio_attributes' ) ):
                    ?>
                    <div class="col-md-4">
                        <div class="project-info">
                            <ul>
                                <?php while (have_rows( 'portfolio_attributes' )) : the_row(); ?>
                                    <li>
                                        <h6 class="list-title"> <?php echo esc_html(get_sub_field( 'attribute_title' )); ?> </h6>
                                        <span class="date"> <?php echo esc_html(get_sub_field( 'attribute_value' )); ?> </span>
                                    </li>
                                <?php endwhile; ?>
                            </ul>
                        </div> <!-- /.project-info -->
                    </div> <!-- /.col- -->
                    <?php
                endif;
                ?>
            </div> <!-- /.row -->
        </div> <!-- /.pd-body -->
        <div class="pd-footer d-flex justify-content-between align-items-center">
            <?php
            $prev_post = get_previous_post();
            if ($prev_post) : ?>
                <a href="<?php echo get_permalink($prev_post->ID) ?>" class="theme-pager prev">
                    <span class="flaticon-back"></span>
                    <?php esc_html_e( 'Prev Project', 'rogan' ) ?>
                </a>
            <?php endif; ?>
            <?php
            $next_post = get_next_post();
            if ($next_post) : ?>
                <a href="<?php echo get_permalink($next_post->ID) ?>" class="theme-pager next">
                    <?php esc_html_e( 'Next Project', 'rogan' ) ?>
                    <span class="flaticon-next"></span>
                </a>
            <?php endif; ?>
        </div> <!-- /.pd-footer -->
    </div> <!-- /.container -->
</div>