<?php
$opt = get_option( 'rogan_opt' );
$main_logo = isset($opt['main_logo'] ['url']) ? $opt['main_logo'] ['url'] : '';
$menu6_close_icon = !empty($opt['menu6_close_icon']['url']) ? $opt['menu6_close_icon']['url'] : ROGAN_DIR_IMG.'/icon/icon43.svg';
$menu6_open_icon = !empty($opt['menu6_open_icon']['url']) ? $opt['menu6_open_icon']['url'] : ROGAN_DIR_IMG.'/logo/menu.svg';
?>
<div id="sidebar-menu" class="eCommerce-side-menu">
    <div class="inner-wrapper">
        <div class="logo-wrapper">
            <button class="close-button">
                <img src="<?php echo esc_url($menu6_close_icon) ?>" alt="<?php bloginfo( 'name' ); esc_attr_e( 'Menu close icon', 'rogan' ) ?>">
            </button>
            <?php
            if ( !empty($opt['menu6_logo']['url']) ) {
                ?>
                <a href="<?php echo esc_url(home_url( '/' )) ?>">
                    <img src="<?php echo esc_url($opt['menu6_logo']['url']) ?>" alt="<?php bloginfo( 'name' ); ?>">
                </a>
                <?php
            } else {
                rogan_logo();
            }
            ?>
        </div>

        <?php
        if( has_nav_menu( 'main_menu' ) ) {
            wp_nav_menu( array (
                'menu' => 'main_menu',
                'theme_location' => 'main_menu',
                'container_class' => 'main-menu-list',
                'walker' => new Rogan_navwalker_side_menu(),
                'depth' => 2
            ));
        }
        ?>

        <?php if ( !empty($opt['header6_content']) ) : ?>
            <p class="copy-right"> <?php echo wp_kses_post($opt['header6_content']) ?> </p>
        <?php endif; ?>
    </div> <!-- /.inner-wrapper -->
</div>

<div class="theme-Ecommerce-menu">
    <div class="d-flex justify-content-between align-items-center">
        <div class="left-content">
            <ul>
                <li>
                    <button class="menu-button sidebar-menu-open">
                        <img src="<?php echo esc_url($menu6_open_icon) ?>" alt="<?php bloginfo( 'name' ); esc_attr_e( 'Menu open icon', 'rogan' ) ?>">
                    </button>
                </li>
                <li class="logo">
                    <?php
                    if ( !empty($opt['menu6_logo']['url']) ) {
                        ?>
                        <a href="<?php echo esc_url(home_url( '/' )) ?>">
                            <img src="<?php echo esc_url($opt['menu6_logo']['url']) ?>" alt="<?php bloginfo( 'name' ); ?>">
                        </a>
                        <?php
                    } else {
                        rogan_logo();
                    }
                    ?>
                </li>
            </ul>
        </div> <!-- /.left-content -->

        <div class="right-content">
            <ul>
                <?php
                $header6_search = isset($opt['header6_search']) ? $opt['header6_search'] : '1';
                if ( $header6_search == '1' ) : ?>
                    <li class="search-form">
                        <form action="<?php echo esc_url(home_url( '/' )) ?>" class="eCommerce-search" role="search" method="get">
                            <input type="text" name="s" placeholder="<?php esc_attr_e( 'Search here', 'rogan' ) ?>">
                            <i class="fa fa-search icon" aria-hidden="true"></i>
                        </form>
                    </li>
                <?php endif; ?>

                <?php
                $header6_minicart = isset($opt['header6_minicart']) ? $opt['header6_minicart'] : '1';

                if ( class_exists( 'WooCommerce' ) && $header6_minicart == '1' ) :
                    global $woocommerce;
                    $total_items = count(WC()->cart->get_cart());
                    ?>
                    <li class="action-list-item cart-action-wrapper">
                        <div class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo ROGAN_DIR_IMG ?>/icon/icon37.svg" alt="<?php bloginfo( 'name' ) ?> ">
                                <span class="item-count"> <?php echo esc_html($woocommerce->cart->cart_contents_count); ?> </span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="cart-product-list">
                                    <?php
                                    $i = 0;
                                    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                                        $is_single_item = ($total_items == 1) ? 'only_single_item' : '';
                                        $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                                        $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                                        if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                                            $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
                                            ?>
                                            <li class="clearfix selected-item">

                                                <?php
                                                $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image( 'rogan_70x80' ), $cart_item, $cart_item_key);
                                                if (!$product_permalink) {
                                                    echo wp_kses_post($thumbnail);
                                                } else {
                                                    printf( '<a href="%s" class="item-img">%s</a>', esc_url($product_permalink), wp_kses_post($thumbnail));
                                                }
                                                ?>

                                                <div class="item-info">
                                                    <?php
                                                    if (!$product_permalink) {
                                                        echo wp_kses_post(apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;' );
                                                    } else {
                                                        echo wp_kses_post(apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s" class="name">%s</a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key));
                                                    }

                                                    do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

                                                    // Meta data.
                                                    echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.

                                                    // Backorder notification.
                                                    if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                                        echo wp_kses_post(apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'rogan' ) . '</p>' ));
                                                    }
                                                    ?>
                                                    <div class="price">
                                                        <?php echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); ?>
                                                    </div>

                                                    <?php
                                                    // @codingStandardsIgnoreLine
                                                    echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                                        '<a href="%s" class="remove action close" aria-label="%s" data-product_id="%s" data-product_sku="%s"><i class="fa fa-window-close"></i></a>',
                                                        esc_url(wc_get_cart_remove_url($cart_item_key)),
                                                        esc_html__( 'Remove this item', 'rogan' ),
                                                        esc_attr($product_id),
                                                        esc_attr($_product->get_sku())
                                                    ), $cart_item_key);
                                                    ?>

                                                </div> <!-- /.item-info -->
                                            </li> <!-- /.selected-item -->
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul> <!-- /.cart-product-list -->
                                <div class="subtotal d-flex justify-content-between align-items-center">
                                    <div class="title"> <?php esc_html_e( 'Subtotal', 'rogan' ) ?> </div>
                                    <div class="total-price"> <?php wc_cart_totals_order_total_html(); ?> </div>
                                </div>
                                <ul class="button-group">
                                    <li><a href="<?php echo wc_get_cart_url() ?>" class="view-cart"> <?php esc_html_e( 'View Cart', 'rogan' ) ?> </a></li>
                                    <li><a href="<?php echo wc_get_checkout_url() ?>" class="checkout"> <?php esc_htmL_e( 'Checkout', 'rogan' ) ?> </a></li>
                                </ul>
                            </div> <!-- /.dropdown-menu -->
                        </div>
                    </li> <!-- /.cart-action-wrapper -->
                <?php endif; ?>

                <?php if ( !empty($opt['ac_menu_title']) ) : ?>
                <li class="action-list-item user-profile-action">
                    <div class="dropdown">
                        <button class="dropdown-toggle" data-toggle="dropdown">
                            <?php if ( !empty($opt['ac_menu_icon']['url']) ) : ?>
                                <img src="<?php echo esc_url($opt['ac_menu_icon']['url']) ?>" alt="<?php echo esc_attr($opt['ac_menu_title']) ?>">
                            <?php endif; ?>
                            <span> <?php echo esc_html($opt['ac_menu_title']) ?> </span>
                        </button>
                        <?php if ( !empty($opt['ac_items']) ) : ?>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul>
                                    <?php
                                    foreach ( $opt['ac_items'] as $ac_item ) {
                                        ?>
                                        <li>
                                            <a href="<?php echo esc_url($ac_item['url']) ?>">
                                                <img src="<?php echo esc_url($ac_item['image']) ?>" alt="<?php echo esc_attr($ac_item['title']) ?>" class="icon svg">
                                                <?php echo esc_html($ac_item['title']) ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div> <!-- /.dropdown-menu -->
                        <?php endif; ?>
                    </div>
                </li> <!-- /.user-profile-action -->
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>