<?php
$opt = get_option( 'rogan_opt' );
$titlebar_align = !empty($opt['titlebar_align']) ? $opt['titlebar_align'] : 'center';

$is_breadcrumb = function_exists( 'get_field' ) ? get_field( 'is_breadcrumb' ) : '1';

if ( is_home() ) {
    $is_breadcrumb = '1';
}
if ( is_search() ) {
    $is_breadcrumb = '';
}

?>
<div class="solid-inner-banner">
    <h2 class="page-title"> <?php rogan_banner_title(); ?> </h2>
    <?php if( $is_breadcrumb == '1' ) : ?>
        <ul class="page-breadcrumbs">
            <li><a href="<?php echo esc_url(home_url( '/' )) ?>"> <?php esc_html_e( 'Home', 'rogan' ) ?> </a></li>
            <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
            <li> <?php rogan_banner_title(); ?> </li>
        </ul>
    <?php endif; ?>
</div>