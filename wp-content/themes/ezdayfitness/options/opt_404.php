<?php

Redux::setSection( 'rogan_opt', array(
    'title'     => esc_html__( '404 Error Settings', 'rogan' ),
    'id'        => '404_0pt',
    'icon'      => 'dashicons dashicons-info',
    'fields'    => array(
        array(
            'title'    => esc_html__( '404 Page Style', 'rogan' ),
            'id'       => 'error_img_select',
            'type'     => 'image_select',
            'default'   => '1',
            'options'  => array(
                '1' => array(
                    'alt' => esc_html__( 'Style 01', 'rogan' ),
                    'img' => ROGAN_DIR_IMG.'/404_style/404_style_1.png'
                ),
                '2' => array(
                    'alt' => esc_html__( 'Style 02', 'rogan' ),
                    'img' => ROGAN_DIR_IMG.'/404_style/404_style_2.png'
                ),
            ),
        ),

        array(
            'title'     => esc_html__( 'Heading Text', 'rogan' ),
            'id'        => 'error_heading',
            'type'      => 'text',
            'required'    => array( 'error_img_select', '=', '2' ),
            'default'   => esc_html__("404", 'rogan' ),
        ),

        array(
            'title'     => esc_html__( 'Title Text', 'rogan' ),
            'id'        => 'error_title',
            'type'      => 'text',
            'required'    => array( 'error_img_select', '=', '1' ),
            'default'   => esc_html__("Page Not Found", 'rogan' ),
        ),

        array(
            'title'     => esc_html__( 'Subtitle', 'rogan' ),
            'id'        => 'error_subtitle',
            'type'      => 'textarea',
            'default'   => esc_html__( ' We’re sorry, the page you have looked for does not exist in our database! Maybe go to our home page.', 'rogan' ),
        ),

        array(
            'title'     => esc_html__( 'Home button label', 'rogan' ),
            'id'        => 'error_home_btn_label',
            'type'      => 'text',
            'default'   => esc_html__( 'Go Back', 'rogan' ),
        ),

        array(
            'id'          => 'btn_font_color',
            'type'        => 'color',
            'title'       => esc_html__( 'Button Text Color', 'rogan' ),
            'output'      => array(
                'color' => '.error-creative-content .inner-wrapper .solid-button-one, .error-content .back-button.line-button-one',
            ),
        ),

        array(
            'id'          => 'btn_bg_color',
            'type'        => 'color',
            'title'       => esc_html__( 'Button Background Color', 'rogan' ),
            'output'      => array(
                'background' => '.error-creative-content .inner-wrapper .solid-button-one, .error-content .back-button.line-button-one',
            ),
        ),

        array(
            'id'          => 'btn_border_color',
            'type'        => 'color',
            'title'       => esc_html__( 'Button Border Color', 'rogan' ),
            'output'      => array(
                'border-color' => '.error-creative-content .inner-wrapper .solid-button-one, .error-content .back-button.line-button-one',
            ),
        ),

        array(
            'id'          => 'btn_font_color_hover',
            'type'        => 'color',
            'title'       => esc_html__( 'Button Text Hover Color', 'rogan' ),
            'output'      => array(
                'color' => '.error-creative-content .inner-wrapper .solid-button-one:hover, .error-content .back-button.line-button-one:hover',
            ),
        ),

        array(
            'id'          => 'btn_bg_hover',
            'type'        => 'color',
            'title'       => esc_html__( 'Button Background Hover Color', 'rogan' ),
            'output'      => array(
                'background' => '.error-creative-content .inner-wrapper .solid-button-one:hover, .error-content .back-button.line-button-one:hover',
            ),
        ),

        array(
            'id'          => 'btn_border_hover',
            'type'        => 'color',
            'title'       => esc_html__( 'Button Border Hover Color', 'rogan' ),
            'output'      => array(
                'border-color' => '.error-creative-content .inner-wrapper .solid-button-one:hover, .error-content .back-button.line-button-one:hover',
            ),
        ),

        array(
            'id'          => 'error_page_bg_one',
            'type'        => 'media',
            'url'         => true,
            'title'       => esc_html__( 'Error Style 01 - Background Image', 'rogan' ),
            'required'    => array( 'error_img_select', '=', '1' ),
            'default'     => array(
                        'url'=> ROGAN_DIR_IMG.'/home/error-bg.svg'
                    ),
        ),

        array(
            'id'          => 'error_page_bg_two',
            'type'        => 'media',
            'url'         => true,
            'title'       => esc_html__( 'Error Style 02 - Background Image', 'rogan' ),
            'required'    => array( 'error_img_select', '=', '2' ),
            'default'     => array(
                        'url'=> ROGAN_DIR_IMG.'/home/404.png'
                    ),
        ),
    )
));
