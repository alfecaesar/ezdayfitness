<?php
Redux::setSection( 'rogan_opt', array(
    'title'     => esc_html__( 'Social links', 'rogan' ),
    'id'        => 'opt_social_links',
    'icon'      => 'dashicons dashicons-share',
    'fields'    => array(
        array(
            'id'    => 'facebook',
            'type'  => 'text',
            'title' => esc_html__( 'Facebook', 'rogan' ),
            'default'	 => '#'
        ),
        array(
            'id'    => 'twitter',
            'type'  => 'text',
            'title' => esc_html__( 'Twitter', 'rogan' ),
            'default'	  => '#'
        ),
        array(
            'id'    => 'googleplus',
            'type'  => 'text',
            'title' => esc_html__( 'Google Plus', 'rogan' ),
            'default'	  => '#'
        ),
        array(
            'id'    => 'linkedin',
            'type'  => 'text',
            'title' => esc_html__( 'LinkedIn', 'rogan' ),
            'default'	  => '#'
        ),
        array(
            'id'    => 'dribbble',
            'type'  => 'text',
            'title' => esc_html__( 'Dribbble', 'rogan' ),
            'default'	  => '#'
        ),
        array(
            'id'    => 'youtube',
            'type'  => 'text',
            'title' => esc_html__( 'Youtube', 'rogan' ),
        ),
        array(
            'id'    => 'instagram',
            'type'  => 'text',
            'title' => esc_html__( 'Instagram', 'rogan' ),
        ),
    ),
));