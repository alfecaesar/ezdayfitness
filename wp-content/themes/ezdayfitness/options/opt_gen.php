<?php
Redux::setSection( 'rogan_opt', array(
    'title'      => esc_html__( 'General Settings', 'rogan' ),
    'id'         => 'gen_settings',
    'icon'       => 'dashicons dashicons-admin-settings',
    'fields'     => array(
        array(
            'title'     => esc_html__( 'Website Animation', 'rogan' ),
            'subtitle'  => esc_html__( 'This will totally turn off all animations of this website.', 'rogan' ),
            'id'        => 'is_anim',
            'type'      => 'switch',
            'default'   => '1',
        ),
    )
));