<?php

// Footer settings
Redux::setSection( 'rogan_opt', array(
	'title'     => esc_html__( 'Footer Settings', 'rogan' ),
	'id'        => 'rogan_footer',
	'icon'      => 'dashicons dashicons-arrow-down-alt2',
));


// Footer settings
Redux::setSection( 'rogan_opt', array(
	'title'     => esc_html__( 'Font colors', 'rogan' ),
	'id'        => 'rogan_footer_font_colors',
	'icon'      => '',
	'subsection'=> true,
	'fields'    => array(
        array(
            'title'     => esc_html__( 'Font color', 'rogan' ),
            'id'        => 'footer_top_font_color',
            'type'      => 'color_rgba',
            'output'    => array( '.footer-widget p, .footer-widget ul li a' )
        ),
        array(
            'title'     => esc_html__( 'Widget Title Color', 'rogan' ),
            'id'        => 'widget_title_color',
            'type'      => 'color',
            'output'    => array( '.footer-widget .widget_title' )
        ),
	)
));

// Footer background
Redux::setSection( 'rogan_opt', array(
	'title'     => esc_html__( 'Background', 'rogan' ),
	'id'        => 'rogan_footer_background',
	'icon'      => '',
	'subsection'=> true,
	'fields'    => array(
        array(
            'title'     => esc_html__( 'Footer Background image', 'rogan' ),
            'desc'      => esc_html__( 'The main footer background image', 'rogan' ),
            'id'        => 'footer_bg_image',
            'type'      => 'media',
            'default'   => array(
                'url' => esc_url(ROGAN_DIR_IMG.'/seo/footer_bg.png' )
            )
        ),
        array(
            'title'     => esc_html__( 'Footer top background color', 'rogan' ),
            'id'        => 'footer_top_bg_color',
            'type'      => 'color',
            'output'    => array( '.footer-top' ),
            'mode'      => 'background'
        ),
        array(
            'title'     => esc_html__( 'Footer bottom background color', 'rogan' ),
            'id'        => 'footer_btm_bg_color',
            'type'      => 'color',
            'output'    => array( '.footer_bottom' ),
            'mode'      => 'background'
        ),
	)
));


// Footer settings
Redux::setSection( 'rogan_opt', array(
    'title'     => esc_html__( 'Footer Bottom', 'rogan' ),
    'id'        => 'rogan_footer_btm',
    'icon'      => '',
    'subsection'=> true,
    'fields'    => array(
        array(
            'title'     => esc_html__( 'Left Content', 'rogan' ),
            'id'        => 'copyright_txt',
            'type'      => 'editor',
            'args'    => array(
                'wpautop'       => true,
                'media_buttons' => false,
                'textarea_rows' => 10,
                //'tabindex' => 1,
                //'editor_css' => '',
                'teeny'         => false,
                //'tinymce' => array(),
                'quicktags'     => false,
            )
        ),
        array(
            'title'     => esc_html__( 'Right Content', 'rogan' ),
            'id'        => 'right_content',
            'type'      => 'editor',
            'args'    => array(
                'wpautop'       => true,
                'media_buttons' => false,
                'textarea_rows' => 10,
                //'tabindex' => 1,
                //'editor_css' => '',
                'teeny'         => false,
                //'tinymce' => array(),
                'quicktags'     => false,
            )
        ),
    )
));