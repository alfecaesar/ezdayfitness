<?php
get_header();
$opt = get_option( 'rogan_opt' );
$page_title = !empty( $opt['portfolio_archive_title'] ) ? $opt['portfolio_archive_title'] : esc_html__( 'Project Gallery', 'rogan' );
$content_column = is_active_sidebar( 'portfolio_widgets' ) ? '9' : '';
wp_enqueue_style( 'fancybox' );
wp_enqueue_script( 'fancybox' );
?>

<div class="our-project project-with-sidebar pt-150 pb-100">
    <div class="full-width-container">
        <div class="row">
            <div class="col-lg-<?php echo esc_attr($content_column) ?> order-lg-last">
                <h2 class="page-title"> <?php echo esc_html($page_title) ?> </h2>
                <div class="row">
                    <?php
                    while( have_posts() ) : the_post();
                        ?>
                        <div class="col-xl-4 col-lg-6 col-md-4 col-sm-6">
                            <div class="project-item">
                                <div class="img-box">
                                    <?php the_post_thumbnail( 'rogan_430x575' ) ?>
                                    <a href="<?php the_post_thumbnail_url() ?>" class="zoom fancybox" data-fancybox="gallery">
                                        <img src="<?php echo esc_url(ROGAN_DIR_IMG.'/icon/zoom-in.svg' ); ?>" alt="<?php the_title_attribute() ?>" class="svg">
                                    </a>
                                </div>
                                <div class="hover-coco">
                                    <div>
                                        <h4 class="title">
                                            <a href="<?php the_permalink() ?>">
                                                <?php the_title() ?>
                                            </a>
                                        </h4>
                                        <p> <?php rogan_first_category( 'portfolio_cat' ) ?> </p>
                                    </div>
                                </div> <!-- /.hover-coco -->
                            </div> <!-- /.project-item -->
                        </div> <!-- /.col- -->
                        <?php
                    endwhile;
                    ?>
                </div> <!-- /.row -->
                <div class="theme-pagination-one pt-15">
                    <?php rogan_pagination(); ?>
                </div>
            </div> <!-- /.col- -->

            <?php get_sidebar( 'portfolio' ) ?>

        </div> <!-- /.row -->
    </div> <!-- /.full-width-container -->
</div>

<?php
get_footer();